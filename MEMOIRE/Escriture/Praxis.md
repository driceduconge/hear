# Déplacer

## Le déplacement comme transfiguration paysagère
multiplication des points de vue et d'écoute, fondu l'un dans l'autre...

## Le déplacement comme potentialité narrative
enchaînement des points de perception, séquencage du paysage, fragmentation et réassemblage...

# Media praxiques 
*(aucune idée si c'est français de dire ça)*

## La praxis comme influence larsénique au paysage
### réel
J'expérimente le paysage en agissant sur lui donc je transforme mes points de percepts tout en transformant le paysage, et en transformant le paysage je transforme mes points de percepts, mes régimes de regards et d'écoute etc etc etc.

### représenté
en me déplaçant et influant autour et avec le paysage (embarqué, posé, exposé, streamé...) je transforme mon point de vue sur ce dernier, je transforme l'espace d'expression de ce dernier et ainsi de suite. Je ne change pas spécialement la matérialité du paysage représenté mais plutôt sa situation de diffusion/exposition.

## Le jeu vidéo comme expérience paysagère

*gros morceau*

*ça pourrait être un mémoire en soi*

Narration
Narration par l'environnement
- visuel
- sonore

influence dua joueurse sur le jeu vidéo et donc sur le paysage proposé...

différentes manières d'aborder le paysage au sein du même jeu... enfin bref c vaste.