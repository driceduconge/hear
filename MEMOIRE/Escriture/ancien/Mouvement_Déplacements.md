:: ça aucune idée non plus c bof ::

Au coeur de la dérive il y a du balottement, du "laisser porter". On se perd dans les vagues. On se perd dans la sensation, inlassablement trimballés par les flux et reflux jusqu'à perdre le sens de l'orientation. Et pourtant, quelque chose se dessine, une sorte de route, un itinéraire commence à s'immiscer, une logique apparaît dans le semblant d'aléatoire et l'on se rend compte, une fois relâchés sur la grève que l'on a fait le tour d'une île. On aura entendu les flots, le fracas des lames contre la roche, avec de la chance les mouettes. On aura vu les étoiles puis sous l'eau, puis le soleil, et sous l'eau et tout ça se retrouvera dans un brouhaha. Puis il y aura le temps, le déplacement. Là j'aurais cru être proche des pierres, là j'étais au milieu de l'océan. Ici je ne sais plus, je me noyais. Au final ce que je remettrai là ce sera le brouillard, et la sensation de percevoir des choses pendant les rares moments où je flottais.
:::::




## Séquencage visuel, séquencage sonore
je pense que là c'est une séquence uniquement son et image. bien séparés mais qui se coupent (et non se recoupent) on est vraiment sur du 1/2/3/4/5.


## !Les! point de percept (Mouvant)

[Justine Balibar et la recomposition permanente du paysage par ex.]
[par la démultiplication des angles de captation, de déplacement intra paysager et extra-paysager, comment on recompose un paysage]
>> ici on va + être dans la description perceptuelle d'installation|s etc

## le déplacement sonore

## le déplacement visuel

## Frottement avec le paysage réel

dynamique d'arpentage, de performances.
le journal de bord peut être intéressant à évoquer ici.

## Frictions entre son & image

ASSEZ GROSSE PARTIE JE CROIS
[ici on est plutôt dans comment le mouvement souligne la différence d'expérience visuelle et sonore séparées et à quel point rajouter du son au visuel permet déjà *d'induire* le mouvement. Que les deux se répondent]
on mettra aussi pas mal d'image et de son au long de ça, afin de compléter le propos écrit.


## (La marche comme expérience paysagère)
peut être