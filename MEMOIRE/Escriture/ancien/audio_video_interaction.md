# Audio Vidéo

## Transformation|s
où le régime de regard se JUXTAPOSE au régime d'écoute
[de par les régimes d'écoute et de regard sur un paysage produit]
(ex. j'entends puis je vois l'oiseau, je rattache l'audio à sa source visuelle, je double spatialise...)

## Sound shaped vision, visually shaped sound
où l'on creuse l'aller retour, la cohabitation entre son et visuel dans le paysage
où les régimes de regards et d'écoutes se CO-INFLUENT
(j'entends donc je vois l'oiseau, je vois donc j'entends...)

# Le numérique comme espace multi-sensoriel

## "computer grass is real grass"

de la représentation numérique comme paysage idéal car mathématique puis re-glissement vers le thème avec l'ordinateur comme expérience non-linéaire et liée à la praxis


## Le jeu vidéo multi-sensoriel, multi-interactif 

on parlerait de comment la fusion permanente du son et de l'image au travers de la donnée pourrait aboutir à un "toucher" paysager.


## (?)L'interaction comme acte démiurgique

[où interagir avec le paysage c'est le reconstruire en même temps qu'on en fait l'expérience]