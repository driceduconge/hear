# Capter

## Lo-Fi & Hi-Fi
 
 [à propos tout d'abord des notions Lo et Hi de Murray Schaffer, des problèmes que cela soulève car c'est une approche fondamentalement phénoménologiste de la perception, et pas tant du percept interne]
   " citation de R.M. Schaffer "
   
## !Le! point de Percept (Fixe)
[on insinue ici aussi une fixitude de la méthode de perception, pas d'aller retour son image etc]
[le point de vue fixe, poncif paysager mais qui permet d'extraire un aspect très particulier de l'espace abordé]
[en se focalisant sur un point de perception, le son et l'image pousse à se projeter face à une entité.]
[ici on crée une réelle dichotomie entre visuel et sonore, car là on parle vraiment non pas d'une pauvreté de l'information mais de la rareté et de la précision extrême de cette dernière] en effet un seul point de perception permet un ancrage plus précis dans l'espace, et une manière plus frontale de l'aborder.
###cadrage|s

jpense à -> Toshiya Tsunoda
### conséquences narratives

## Filtres techniques (à la captation), le grain

[ici on aborde comment la technique (la technique = objet technique) crée un "focus" sur certaines formes, certaines bandes] 

[par ex. la peinture, la vidéo, la photo, les trucs temporels qui y sont liés]

[par ex. les filtres passe haut, passe bas, les qualités de micros -> ce qu'on perd (de la précision dans la restitution exacte naturaliste) mais ce qu'on gagne (une certaine précision dans une certaine plage fréquentielle]



# (Re)Transcrire

## Le haut parleur comme déformation

## L'ordinateur comme déformation
[ici d'un aspect purement technique comment l'électronique, par la compression et la transformation physique de percepts, en restitue d'autres et les déforme]