# Introduction & other defs

##Contacts 
? => à contacter
P => peu probable (personne connues/dont je n'ai pas le contact)
! => contacté.e / sûr de croiser
O => ok plus qu'à se voir
X => non

P Elise Guillaume
! Yannick Dauby (attente réponse mail)
O Matthieu Saladin (quand je serais à Paris)
O Elsa de Smet (quand je serais à Paris)
! Elena Biserna (quand je serais à Paris)
? Thomas Tilly
? Marc Namblard
? Francisco Lopez
! Sebastian Dicenaire (attente réponse mail)
P Sarah Sze
? Minh Xuan Truong (Expériences sensorielles de nature - Nature virtuelle)

## Définitions du Paysage + Paysage Sonore

La première impasse, lors de ce travail du paysage, est de se retrouver face à la myriade d'incertitudes, de définitions contradictoires et complémentaires que l'on trouve depuis quelques années dans des champs de la recherche aussi variés que l'urbanisme, l'écologie, l'histoire de l'art ainsi que l'esthétique. La notion du paysage, avec l'avancement des technologies de captation, de reproduction des environnements, mais aussi de déplacement s'est vue s'étendre, se transformer et devenir confuse. Si bien que l'on ne sait même plus de quoi l'on parle,  si l'on parle d'une perception, d'un site remarquable, d'une composante d'un territoire, d'une peinture ou encore d'un espace mental reliant différentes parties d'un même champ[^1]. Pourtant la notion de paysage ne saurait être contenue tout ou partie, par ces mots. En effet il n'est ni "mesurable ou identifiable [...] ni le territoire, ni le pays, ni le site"[^2], il reste éthéré et insaisissable, flottant. Le mot paysage, comme souvent répété par la plupart des philosophes contemporains traitant cette question[^3] est polysémique. Au sens qu'il signifie à la fois un paysage *réel*, dont le sujet fait partie intégrante et le paysage *représenté*, ou le sujet faisant l'expérience du paysage est dans un espace autre que celui du paysage. L'exemple le plus commun est la différence entre le paysage que l'on admire du haut d'un Pic Alpin et le paysage que l'on retrouve sur une peinture, un film qui nous est distant et in-touchable.

  On retrouvera alors encore une autre polysémie dans le paysage représenté :  la plupart des philosophes cantonnent leurs études du paysage représenté comme la représentation de "l'aspect d'un pays", donc à l'expérience visuelle de ce dernier, avec toutes les notions de cadrages, de profondeur de champ etc. qui sont inhérentes à l'image en deux dimensions. L'expérience du paysage réel serait donc multi-sensorielle, mais un paysage artialisé[^4] ne pourrait qu'être visuel. Tout au long de ce mémoire, nous nous efforcerons de mettre en parallèle la vue et l'ouïe, afin de faire glisser dans la polysémie du paysage le paysage sonore. Car en effet le paysage peut et est depuis longtemps représenté sous son aspect sonore. Certes, chronologiquement parlant, c'est le paysage visuel qui a le primat sur le terme, ayant émergé à la Renaissance[^5] à travers les peintures italiennes et flamandes, mais le paysage sonore, depuis l'avènement des techniques de captation et de reproduction sonore n'a cessé de s'imposer comme une étude et pratique essentielle du paysage. En effet depuis R. Murray Schaffer et son ouvrage *Le Paysage Sonore : le monde comme musique*[^6] on a vu se développer de multiples approches de ce dernier, autant utiles aux scientifiques[^7] qu'aux architectes acousticiens et aux artistes sonores.
  Il sera donc question non pas d'aborder le paysage représenté visuel et  sonore comme unique retranscription possible de ce dernier, mais plutôt de comprendre les liens qui se tissent entre ces deux grands axes sensoriels du paysage. On passera donc naturellement de l'un à l'autre, quant ils s'éloignent se mêlent, sans oublier de parler de la dualité réel/virtuel qui est composante totale du paysage.

[^1]: Par exemple on parle souvent de paysages sociaux, économiques etc. Au final ces notions relèvent plutôt de la cartographie, en convoquant des interconnections entre divers acteur|ice|s concernant un certain espace mental ou géographique.

[^2]: *Le Paysage*, Michael Jakob, 2008, Infolio éditions

[^3]: Anne Cauquelin, Justine Balibar, Augustin Berque, Alain Roger etc...

[^4]: citer ici Alain Roger

[^5]: citer J. Balibar par exemple... ou un.e autre... iels se répètent

[^6]: ref biblio R.M.S.

[^7] : Soundscape Ecology: The Science of Sound in the Landscape in *BioScience vol. 61 No. 3*, Bryan C. Pijanowsky et. al.

## Régimes d'écoute et de regards

[ici on parlera beaucoup de Anne Cauquelin et M. Jakob, donc de la notion de "cliché" paysager, d'attente face à un percept futur.]
[en vrai le truc ça va être de surtout de pas faire trop long ici mais de permettre de poser des notions d'image-paysage & de permettre de faire un pas vers l'intériorité, de ce qui se produit intérieurement]

## 

:: ça pas sûr ::
  Pourtant, souvent la première chose qui viendrait à l'esprit en pensant paysage serait une vue d'un espace extérieur, non-urbain, depuis un promontoire, avec un horizon distinguable et une profondeur de champ palpable. On pourrait, en se concentrant un peu plus longtemps sur cette scène, entendre le vent, la mer ou le craquement d'une montagne, ressentir la température ambiante, la texture du sol sous nos pieds... En somme toute, on se *projetterai* dans un espace. 
:: bof ::
