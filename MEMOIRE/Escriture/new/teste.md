es lacunes inattendues dans la logique de l’organisation spatiale_.[^56]

Opérer une coupure dans le temps pour archiver un morceau d’espace, c’est approcher le paysage de la perspective du médecin légiste : on incise, découpe, retourne une peau en voie de putréfaction pour se rendre compte de ce qui l’a amenée là. Le sensible se glisse parfois dans l’arrêt du temps, dans les images de cadavres.

[^56]:  Peter Galassi, *Before Photography: Painting and the Invention of Photography*, New York, MoMa, 1981, cité dans Desportes Marc, *Paysages en mouvement*, Gallimard, 2005

---

>Là où la présence visible ou tactile se tient dans un « en même temps » _immobile_, la présence sonore est un « en même temps » _essentiellement mobile_, vibrant de l’_aller-retour entre la source et l’oreille_, à travers l’espace ouvert.[^2]

[^2]: Nancy Jean-Luc, *À l’écoute*, Galilée, 2002, p. 36

>chaque son, chaque bloc devient image. De son, *il vire en bruit*. [^38]

[^38]: Charles Daniel, *Gloses sur J. Cage*, op. cit., p. 207

 <figure><img width="350" src="../../whsop_g/4.png"><figcaption>Hamadicharef Brahim, *Choir waveform sound analysis*, dans *Objective Prediction of Sound Synthesis Quality*, consulté en novembre 2022 [https://www.researchgate.net/publication/228972415_Objective_Prediction_of_Sound_Synthesis_Quality](https://www.researchgate.net/publication/228972415_Objective_Prediction_of_Sound_Synthesis_Quality)</figcaption></figure>


>Penser le Temps - c’est penser l’identité de l’aller et du retour. […] dans le même trait *revenir*.[^89]

[^89]: Quignard Pascal, *La Haine de la musique*, op. cit., p. 230

---

Pour peu qu’un son se joue on percevra le temps, le marqueur direct de ce dernier serait le battement de l’horloge, métronome cadencé au soleil. Dans le paysage, si la dimension temporelle est omniprésente, elle se cadence différemment des notations normées. En habitant l’espace, on ne divise pas le temps en heures et secondes, mais plutôt en rythme avec les autres sons présents, chaque espace revêtant dans le cycle des saisons et dans les rythmes du soleil et de la lune, des aspects sonores différents. J’entends la nuit les grillons ainsi que le bruissement d’une chouette, le jour il y aurait plutôt le vrombissement des automobiles parcourant la départementale, mêlée au chant d’un merle.

>La seule introduction possible du langage dans la musique est celle des conjonctions, [mais, où, est, donc, or, ni, car][^92]

[^92]: Nancy Jean-Luc, *À l’écoute*, op. cit., p. 43

Si le son est conjonction, le visuel est les noms et le sujet est lui-même, performant les verbes. Il y a donc bien une sorte de _dynamique_ son et image, composer un paysage en omettant le son reviendrait à lui enlever des _connecteurs _, ce qui ne va pas forcément ruiner la phrase « La mer, belle bleue », mais lui enlever une composante fine de cette dernière « La mer, belle et bleue », « La mer, belle car bleue ». Je m’avancerai et dirait que le sonore est de l’ordre de la _ponctuation et de la diacritique _, le son souligne, sur-signifie, transforme, retarde, rapproche, _respire_.

--- 

[TRACK 06 – FieldReorder 01, Martina Lussi & Tim Shaw]

>Toute vibration qui approche le battement du cœur et le rythme du souffle entraine une même contraction [^76]

[^76]: Charles Daniel, *Gloses sur John Cage*, op. cit., p. 53

L’air comprimé et relâché, par les variations de pressions, température et souffle, traverse et reforme un espace-son, l’air, l’invisible dans le vide formel, charge des poumons vers le cœur, le paysage exhale en continu ; nous inspirons son haleine, mélangeant notre cadence à la sienne, au rythme scandé.

>*Les mots forment chaîne dans le souffle. Les images forment rêve dans la nuit. Les sons aussi forment chaîne le long des jours.* [^88]

[^88]: Quignard Pascal, *La Haine de la musique*, op.cit., p. 55

>Nous sommes de l’eau, de la terre, de la lumière et de l’air contractés, non seulement avant de les reconnaître ou de les représenter, mais avant de les sentir. [^11]

[^11]: Deleuze Gilles, *Différence et répétition*, Paris, PUF, 1968, p. 99

---

Les vagues c’est un peu la montée en tension du tranquille. Le va vient incessant, le clapotis que l’on voit grimper en vague et qui alors sous l’eau, fait roulement de tambour sourd. On voit alors la vague qui monte, monte, gronde et puis s’enroule sèchement sur elle-même dans un éclatement sonore et visuel (l’écume qui s’éparpille en diacritiques) et tout cela retombe en bruissant vers vous jusqu’à mouiller les pieds. Le reste de vague, insignifiant milli- puis micro- puis pico -mètre griffe un peu le sable et couine fshhh, pauvre et s’écrase dans un bruit blanc, laisse un silence bref, et de l’écume parfois.

La vague s’écrase dans le monde pour y revenir plus tard, elle s’échoue sur le sable pour mieux le ronger, pour mieux sculpter le bord de mer le *shore*, l’attaque de l’écume se situe dans son point de rupture avec la roche où elle claque contre elle. L’attaque sonore de la vague en haute mer se meut en un bruit sourd que seule la terreur permet d’entendre. Le tangage énorme qu’elle crée participe à bouger l’horizon, à rendre le paysage flou en nous supprimant toute possibilité d’agir dans le mouvement.

---

Se mou-voir dans l’espace, c’est fondre l’une dans l’autre des images et des intensités sonores, et avec la vitesse les moduler. En courant, je changerais la hauteur du passereau, je verrais en roulant les garde-fous autoroutiers perdre leurs vis pour s’effilocher en une bande grise.

>Il semble même qu’une part du spectacle réside dans sa _dynamique_. C’est au cours d’un mouvement que peut-être apprécié le jeu des ondulations et des perspectives changeantes.[^71]

[^71]: Desportes Marc, *Paysages en Mouvement*, Paris, Gallimard, 2005, op. cit., p. 73

<figure>
<img width="350" src="../../BiblioIconoSono/videos/v.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_2.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_3.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_4.jpg">
<figcaption>Charrière Julian, An invitation to disappear, 2018, captures d’écran de vidéo, consulté  en novembre, https://www.youtube.com/watch?v=0Tvb4E8CAmU</figcaption>
</figure>

Le paysage dans le déplacement, se déforme, en des fils, des chemins, un élément glisse vers l’autre. La vallée est un chemin du ciel aux rias, le son de l’eau de la ria se répercute dans la vallée et le sujet écoutant perçoit l’eau et la vallée en lui-même. Ingold parle d’une *danse de l’agentivité*[^70] On ne perçoit qu’une partie de la nasse et ses enchevêtrements complexes de fils se compénétrant inlassablement parviennent à tisser la globalité paysage dans laquelle chaque élément peut ressurgir indépendamment. La maille, à la différence du réseau, s’étire en puissance dans le temps, un nœud étant toujours possible, tant qu’on aura a minima deux éléments flottants.

[^70]: Ingold Tim, *Dance of Agency*, 2013, cité dans Brown Steven D., Kanyeredzi Ava, McGrath Laura, Reavey Paula & Tucker Ian, « Affect theory and the concept of atmosphere », *Distinktion, Journal of Social Theory*, Volume 20, pp. 5-24

---

>Les vents courent, volent, s’abattent, finissent, recommencent, planent, siffle, mugissent, rient ; frénétiques, lascifs, effrénés, prenant leurs aises sur la vague irascible. Ces hurleurs ont une harmonie. Ils font tout le ciel sonore. Ils soufflent dans la nuée comme dans un cuivre, ils embouchent l’espace ; et ils chantent dans l’infini, avec toutes les voix amalgamées des clairons, des buccins, des olifants, des bugles et des trompettes, une sorte de fanfare prométhéenne.[^21]

[^21]: Hugo Victor, *Les Travailleurs de la Mer*, II, cité dans Murray-Schaffer R., *Le Paysage sonore, le monde comme musique* (1977), Wildproject, « Domaine sauvage », trad. Sylvette Gleize, 2010




