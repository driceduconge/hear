*trame = le point de vue fixe ou lent (l'arpentage ) ->le train et la voiture -> le walkman -> les jv*

Le paysage a été représenté longtemps sous la forme du point de vue, de la vue à travers la fenêtre, mais dernièrement, les attractions touristiques présentent non plus seulement des paysages mais des "sites". Ces derniers se posant plutôt comme un enchevêtrement de paysages pointant vers le même lieu significatif, donc une masse de perceptions et de vues idéales projetant donc non pas une vue acquise (le Mont Blanc, paysage), mais plutôt comme un ensemble de vue à *parcourir*, à examiner sous tous les angles.

>Du lieu, en revanche, le site garde le trait principal qui est de mémorisation, d'enveloppement, d'environnement, qu'il s'agisse du milieu physique ou de milieu contextuel, comportemental et transmissible par les usages, ou bien l'archivage. Ce site-là contient le temps, sous forme de mémoires accumulées, et il est contenu dans et par la temporalité, dont il donne une image expressive.[^1]

[^1]: A. Cauquelin, *Le site et le paysage*, p. 85à vérifier

On peut alors y réaliser différents parcours, différentes manières d'arpenter ce "gros paysage" (à la manières des différents chemin de randonnées, parfois placés par rapport à différents points cardinaux d'une montagne). Mais en parcourant cet espace, en l'écoutant et le regardant, on se meut *dans* le paysage. On se déplace dans cet espace sensible, on en goûte et repousse les frontières de l'horizon. C'est ici que s'opère la distinction entre le paysage-image et le paysage(-réel), on le pratique (praxis).

>[...] si le sujet s'avance, c'est que le lieu présente une profondeur; mais si ce même lieu semble profond c'est aussi que le sujet [...] perçoit en s'avançant que ce lieu est prêt à accueillir le mouvement de son corps.[^2]

[^2]: desportes, paysages en mouvement, p174

>[...] d'un côté l'expérience *contemplative* de la perception d'un espace séparé du nôtre, [...] de l'autre l'expérience *immersive ou intégrative* de la perception d'un espace en continuité avec le nôtre[^3]

[^3]: J. Balibar, Qu'est ce qu'un paysage, p. 11

Le mouvement, cognitivement, se passe comme un assemblage d'images ainsi qu'un déplacement du point d'écoute. Le cerveau traite un certain nombre d'images à la seconde, donc si l'on considère un paysage-image comme une certaine vision fixe, ici à travers le mouvement il y a plusieurs micros-paysages qui s'enchaînent en nous, translation après translation et forment un continuum de paysages fixes. Une démultiplication de la sensation paysagère s'opère aussi par l'oreille, qui en s'éloignant et se rapprochant de diverses sources, se reconfigure une audioposition[^4] à chaque pas. 

[^4]: terme attribué à michel Chion 

>Tout se fait par résonance des disparates, point de vue sur le point de vue, déplacement de la perspective, différenciation de la différence, etnon par identité des contraires. [^6]

>Nous songeons à une propagation plus ondulatoire et moins corpusculaire. [^5]**(p. 66)**

En ondulant à travers le paysage, on le transgresse, on l'éprouve autrement et l'on se construit un récit, en *découpant* les sons et la lumière pour retailler, au fur et à mesure de l'arpentage, un autre paysage, proche de la sensation fixe mais légèrement enrichie, en arpentant on effectue un travelling.

> J'ai le pouvoir d'enjamber, de m'attarder sur les points forts, de neutraliser certaines zones sous la forme de blancs ou de marges inintéressantes. 

[^5]: P. Sansot, *Variations paysagères*

![](../../BiblioIconoSono/videos/v.jpg)![](../../BiblioIconoSono/videos/vimeo_2.jpg)
![](../../BiblioIconoSono/videos/vimeo_3.jpg)
![](../../BiblioIconoSono/videos/vimeo_4.jpg)

Julian Charrière, *an invitation to disappear*

[^6]: Deleuze, *Logique du sens*, 1969, p. 203

