# Avant propos

https://github.com/markdown-it/markdown-it

Si, tout au long de ce mémoire, j'aborde la question de paysage, ce n'est pas afin de répéter ce que beaucoup (cités au fil de ce texte) ont déjà pu écrire au sujet du paysage. Je ne tergiverserai alors pas sur les conditions d'apparitions du concept de paysage, de sa place dans l'art au fil du temps ainsi que toutes les implications qu'il a pu prendre dans des domaines variés tel que l'architecture, l'écologie ou encore la géographie. Les théoricien·nes du paysage n'étant en accord ni sur une définition du terme, ni sur ses usages probables, on ne s'imposera pas de définition trop précise (arrêtée) du concept.

> Une recherche sur le paysage devrait se montrer fidèle à une sorte de principe de visibilité, c'est-à-dire s'attacher d'une façon constante ou du moins prioritaire à ce qui s'offre aux sens, à ce qui se perçoit, s'entend, se subodore. [^1]

[^1]:*Variations Paysagères*, Pierre Sansot, Paris, ed. Klincksieck, 1983

On assumera alors le terme paysage comme un concept volontairement flou,  de l'ordre du ressenti, en s'appuyant principalement sur les inter-relations perceptives et sensibles du paysage , soit-il réel ou (re)présenté. Ainsi, je ne me dédouane pas des diverses implications de son usage ici, mais plutôt *j'étends*, à la manière du paysage, l'horizon. Etalant ainsi le champ d'appréciation on se permettra d'employer le terme pour et dans tous les contextes où il serait pertinent (à tort et à travers), mais aussi on lui préférera parfois d'autres termes (l'atmosphère, l'ambiance). Les angles de réflexions seront abordés, par des méthodes aussi variées que celle de l'emprunt, de la citation, à la manière du rêveur dans son hamac, qui s'imagine tissant des liens, s'arnachant au bancal, fixé sur des fils à rien de rompre. Tout du long on liera bribes de ressentis, citations , sons et images qui peu à peu, fil à fil, déploieront un *paysage des paysages* sur l'horizon sensible.


*ici insérer mode d'emploi*


## interlude

Le paysage pour certain.es théoricien.nes c'est comme le cochon, on aura beau en distinguer tous les morceaux quand c'est vivant (par là les jambes, les pieds la queue en tire bouchon et son groin gros comme ça), on arrive à l'apprécier qu'une fois mort et disséqué dans l'assiette mâché par la bouche puis recraché vers l'estomac. On digère bien le tout et c'est là qu'on a bien mangé du bon porc que l'on comprend qu'on a bouffé un bon lard. Que une fois que les peintres  et autres enregistreurs nous auraient bien mâché l'espace, le ciel et la mer, qu'ils l'aient bien recomposé comme il faut qu'on le ressente dans leurs toiles et partitions, une fois qu'ils nous l'auraient bien fait passer, tout pressé, par les yeux et les oreilles, là seulement on saurait que c'est un beau paysage car on nous l'a dit. Ah ça oui le Mont Blanc il est beau ! Et c'est grâce aux frères Besson que j'y pense au mont blanc, qu'il est beau quand je le vois là depuis la voiture. C'est car c'est dans mon subconscient ! Car on me l'a donné à boire à la naissance, que ça s'est mis dans mon génome ! Sans eux, sans les boîtes d'allumettes où il est, jamais je me serais trouvé touché par ce caillou enneigé ! C'est beau tout de même !

Certes, le cochon mort est mieux compris en bouche que celui vivant, mais le paysage, une fois tué, mâché, ne se mange pas, car ce n'est pas de la viande. Alors on supposera peut être que le paysage, loin de toute métaphore carnassière, n'est pas seulement le produit d'une culture mais plutôt un *quelque chose* qui s'impose aux humains. Que ce quelque chose se situe peut être justement dans la manière dont nous perdons parfois la notion de délimitation du spectre lumineux, auditif, là où nos organes sensibles passent en liaison directe avec le coeur. Le moment où l'estomac parle un peu plus fort et nous laisse, caisse de résonance, sentir l'espace au fond des tripes. Le mont blanc qu'on engloutiw. Le bord de rond point qui vrille les veines et l'écho dans l'ancien four à chaux qui fait frémir le bas du dos.

#PLAN

- I : Paysage, oeil oreille, regard et écoute
- II : Atmosphères et paysages
- II.5 : Data-driven & touch-starved
- III : Reproductions de sensations
