
L’air comprimé et relâché, par les variations de pressions, température et souffle, traverse et reforme un espace-son, l’air, l’invisible dans le vide formel, charge des poumons vers le cœur, le paysage exhale en continu ; nous inspirons son haleine, mélangeant notre cadence à la sienne, au rythme scandé.

>*Les mots forment chaîne dans le souffle. Les images forment rêve dans la nuit. Les sons aussi forment chaîne le long des jours.* [^88]

[^88]: Quignard Pascal, *La Haine de la musique*, op.cit., p. 55

>Nous sommes de l’eau, de la terre, de la lumière et de l’air contractés, non seulement avant de les reconnaître ou de les représenter, mais avant de les sentir. [^11]

[^11]: Deleuze Gilles, *Différence et répétition*, Paris, PUF, 1968, p. 99

---

Les vagues c’est un peu la montée en tension du tranquille. Le va vient incessant, le clapotis que l’on voit grimper en vague et qui alors sous l’eau, fait roulement de tambour sourd. On voit alors la vague qui monte, monte, gronde et puis s’enroule sèchement sur elle-même dans un éclatement sonore et visuel (l’écume qui s’éparpille en diacritiques) et tout cela retombe en bruissant vers vous jusqu’à mouiller les pieds. Le reste de vague, insignifiant milli- puis micro- puis pico -mètre griffe un peu le sable et couine fshhh, pauvre et s’écrase dans un bruit blanc, laisse un silence bref, et de l’écume parfois.

La vague s’écrase dans le monde pour y revenir plus tard, elle s’échoue sur le sable pour mieux le ronger, pour mieux sculpter le bord de mer le *shore*, l’attaque de l’écume se situe dans son point de rupture avec la roche où elle claque contre elle. L’attaque sonore de la vague en haute mer se meut en un bruit sourd que seule la terreur permet d’entendre. Le tangage énorme qu’elle crée participe à bouger l’horizon, à rendre le paysage flou en nous supprimant toute possibilité d’agir dans le mouvement.

---

Se mou-voir dans l’espace, c’est fondre l’une dans l’autre des images et des intensités sonores, et avec la vitesse les moduler. En courant, je changerais la hauteur du passereau, je verrais en roulant les garde-fous autoroutiers perdre leurs vis pour s’effilocher en une bande grise.

>Il semble même qu’une part du spectacle réside dans sa _dynamique_. C’est au cours d’un mouvement que peut-être apprécié le jeu des ondulations et des perspectives changeantes.[^71]

[^71]: Desportes Marc, *Paysages en Mouvement*, Paris, Gallimard, 2005, op. cit., p. 73

<figure>
<img width="350" src="../../BiblioIconoSono/videos/v.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_2.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_3.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_4.jpg">
<figcaption>Charrière Julian, An invitation to disappear, 2018, captures d’écran de vidéo, consulté  en novembre, https://www.youtube.com/watch?v=0Tvb4E8CAmU</figcaption>
</figure>

Le paysage dans le déplacement, se déforme, en des fils, des chemins, un élément glisse vers l’autre. La vallée est un chemin du ciel aux rias, le son de l’eau de la ria se répercute dans la vallée et le sujet écoutant perçoit l’eau et la vallée en lui-même. Ingold parle d’une *danse de l’agentivité*[^70] On ne perçoit qu’une partie de la nasse et ses enchevêtrements complexes de fils se compénétrant inlassablement parviennent à tisser la globalité paysage dans laquelle chaque élément peut ressurgir indépendamment. La maille, à la différence du réseau, s’étire en puissance dans le temps, un nœud étant toujours possible, tant qu’on aura a minima deux éléments flottants.

[^70]: Ingold Tim, *Dance of Agency*, 2013, cité dans Brown Steven D., Kanyeredzi Ava, McGrath Laura, Reavey Paula & Tucker Ian, « Affect theory and the concept of atmosphere », *Distinktion, Journal of Social Theory*, Volume 20, pp. 5-24

---

>Les vents courent, volent, s’abattent, finissent, recommencent, planent, siffle, mugissent, rient ; frénétiques, lascifs, effrénés, prenant leurs aises sur la vague irascible. Ces hurleurs ont une harmonie. Ils font tout le ciel sonore. Ils soufflent dans la nuée comme dans un cuivre, ils embouchent l’espace ; et ils chantent dans l’infini, avec toutes les voix amalgamées des clairons, des buccins, des olifants, des bugles et des trompettes, une sorte de fanfare prométhéenne.[^21]

[^21]: Hugo Victor, *Les Travailleurs de la Mer*, II, cité dans Murray-Schaffer R., *Le Paysage sonore, le monde comme musique* (1977), Wildproject, « Domaine sauvage », trad. Sylvette Gleize, 2010




