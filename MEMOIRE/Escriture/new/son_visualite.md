[TRACK 01 – Extrait d’entretien avec Sebastian Dicenaire, créateur de fictions radiophoniques]

> ##### Noun: landscape (countable and uncountable, plural landscapes)
> + A portion of land or territory which the eye can comprehend in a single view, including all the objects it contains. 
>+ A sociological aspect of a physical area.
>+ A picture representing a real or imaginary scene by land or sea, the main subject being the general aspect of nature, as fields, hills, forests, water, etc.
>+ The pictorial aspect of a country.
>+ (computing, printing, uncountable) a mode of printing where the horizontal sides are longer than the vertical sides
>+ A space, indoor or outdoor and natural or man-made (as in "designed landscape")
(figuratively) a situation that is presented, a scenario

> ##### Noun: soundscape (plural soundscapes)
>+ An acoustic environment, a virtual/emotional environment created using sound. 
>+ A soundscape composition, an electroacoustic musical composition creating a sound portrait of a sound environment.

On verra qu’outre la notion d’environnement et l’étymologie en -scape qui semble les relier, les deux termes paraissent assez éloignés. Pourtant nous les regrouperons ici sous l’appellation de paysage. Non pas par praticité, mais plutôt car le soundscape n’est qu’une conceptualisation grossière du paysage en tant que son, ou comme préfère Murray Schaffer le « paysage sonore ». On se placera donc en léger désaccord, car tout au long de ce mémoire, nous partons du postulat que le paysage est une sensation, une expérience sensorielle parcellaire et totale, que l’on n’expérimente pas un paysage réel par morceaux et si l’on assiste à la reproduction d’un enregistrement sonore d’un paysage alors on a affaire qu’à cela, l’écoute d’une trace.

>L’environnement dont nous faisons l’expérience, celui que nous connaissons et dans lequel nous évoluons, n’est pas découpé selon différents chemins sensoriels par lesquels nous y accédons. […] La puissance du concept prototypique de *landscape* réside précisément dans le fait qu’il n’est lié à aucun registre sensoriel spécifique.[^14] 

[^14]: Ingold Tim, « Against soundscape », in *Autumn Leaves*, ed. Angus Calyle, Londres-Paris, CRiSAP/Double Entendre, 2007 cité dans Bonnet François J., *Les mots et les sons, un archipel sonore*, L’Éclat, 2012

Où l’on perçoit une section d’un espace réel ou imaginaire, où on se le représente à partir des percepts offerts. À travers principalement la vue et l’ouïe.

> Perception as a representing act is distinguished from immersion in the sensible ; it involves an anterior sorting out, presumes a directive surge of consciousness and an intended object.[^3]

Picture, en anglais, c’est donc la transmission d’un souvenir (à priori) non narratif, ne faisant pas anecdote en soi. Si la description d’un lieu forme un récit, on n’y intègre pas forcément les actions s’y déroulant. On dépeint un espace arrêté, une portion délimitée de celui-ci, par apposition mentale d’un cache (on pose une fenêtre)[^30] mettant en avant un espace perceptif. Tout l’enjeu de cette pratique du *picturing*, soit-elle en puissance ou non, est non pas de projeter le paysage et ses potentialités sensorielles, mais plutôt de (se) projeter dans les ressentis d’un·e rapporteurse de paysage.

>"[...] car un tableau ne représente pas une image vue à travers un œil : l’objet réel, tout comme l’objet peint, sont tous deux objets du regard. Un tableau n’est pas image de la vision. […] Si la vue est toujours vue de quelque chose, elle n’est pas vue d’elle-même.[^12]

[^12]: Maillet Arnaud, *Le miroir noir, enquête sur le côté obscur du reflet*, L’Éclat, « Kargo », 2005, p. 80
 
[^30]: Anne Cauquelin, *L’invention du paysage*, Paris, PUF, 2000, 180 p.

La captation confine le paysage en un extrait choisi, mais le paysage réel, au sens où il est en continuité avec notre situation sensorielle, s’offre d’une autre manière aux sens.

> [le paysage est] *un espace à la fois fini et infini, limité et illimité*[^111]

[^111]: Balibar Justine, *Qu’est-ce qu’un paysage ?*, Vrin,  « Chemins Philosophiques », 2021, p. 117

---

[TRACK 02 -  Baute Pascal, De 20 Hz à 20 kHz le spectre d’audition humain](https://www.youtube.com/watch?v=wbPclkhoXdE)

 <figure><img src="../../BiblioIconoSono/Images/wikimedia_spectrevisible.png"><figcaption>Gringer Ronan Phillip, *Situation du visible dans le spectre électromagnétique*,  Wikimedia Commons</figcaption></figure>

--- 

>Le paysage n’existe qu’en relation avec un sujet esthétique, il est l’objet d’une *perception* et d’une *expérimentation*, lesquelles sont médiatisées par toute une série de filtres culturels variés[^1]

[^1]: Ibid., p. 8

Un paysage retranscrit (car l’on est bien scribe face à un paysage) se trouve inévitablement  transformé par le prisme sensoriel dea scribe, un accord de couleur, un réglage d’amplitude, un angle de captation n’étant jamais objectif pour qui que ce soit. Quiconque retranscrit soi-disant objectivement un lieu se retrouve confronté à sa propre sensibilité, ses propres filtres. La posture du scribe ne va pas sans celle de l’enlumineur, de l’artiste. Personne, même l’audio-naturaliste lea plus chevronée ne pourra restituer une expérience réelle d’un paysage. C’est la fameuse fenêtre du peintre, c’est le *picture* anglais. L’objectivisme des Becher est tout autant un échec que celui d’Ayn Rand, au sens que la réalité existe bel et bien au-delà de la conscience, mais qu’il est impossible, et non-souhaitable de vouloir chercher à standardiser cette dernière, car l’expérience sensorielle étant fondamentalement basée sur les points de perception individuels, la collectivisation des ressentis n’appelle en aucun cas à l’édition d’un standard affectif.

Un ciel sans nuages, en lumière « neutre » n’est pas souhaitable et se forme en antithèse à la notion même de paysage.

>le point de vue est ouvert sur une divergence qu’il affirme : c’est une autre ville qui correspond à chaque point de vue, chaque point de vue est une autre ville, les villes n’étant unies que par leur distance et ne résonant que par la divergence de leurs séries, de leurs maisons et de leurs rues. Et toujours une autre ville dans la ville. Chaque terme devient un moyen d’aller jusqu’au bout de l’autre, en suivant toute la distance.[^6]

[^6]: Deleuze Gilles, *Logique du Sens*, Paris, Les Éditions de Minuit, « Critique », 1969, p. 203

[TRACK 03 – Extrait d’entretien avec Elsa de Smet, historienne de l’art]

La sonnance collective n’est pas et n’a pas à vocation à être harmonieuse, elle résulte d’un chaos de timbres et d’amplitudes, de couleurs et de masse. Le paysage consensuel n’est que celui fabriqué, répété ad nauseam à travers différents mediums. L’identité se crée alors dans la démultiplication aphone d’un élément standard. La différence infime entre chaque reproduction contribue à machiner un objet-paysage. Le Mont-Blanc en est un parfait exemple, car seul l’alpiniste chevronné ou les habitantes des alentours pourront témoigner d’une vision particulière de ce site. Certains entendront les craquements du tain tandis que d’autres en verront la silhouette par tout temps. Si l’on va voir le Mont-Blanc dans l’attente de ce que l’on a vu, on sera satisfait car on aura re-connu l’endroit, mais toute la surprise et la sensation paysagère résidera non pas dans la correspondance à l’image-paysage, mais bien dans l’agencement réel des composantes attendues, où la combinatoire liée au temps est forcément inattendue.

>Authentique, ce serait un paysage donné à un individu […], un bout de nature découverte et non reconnue. […] Il impliquerait en outre la _présence synesthésique de tous les sens au lieu de la soumission béate au diktat de l’œil et à la sémantique de la vision _.[^16]

[^16]: Jakob Michael, *Le Paysage*, Infolio éditions, 2008, p. 14

---

L’avènement du paysage étant le produit d’une perception, il est aussi et surtout le produit de *régimes d’attention* de l’ouïe et de la vue, qui deviennent alors, regard et écoute. Pour exemple, on voit un arbre, mais on regarde les interstices et les jeux de lumière entre les feuillages, ses nœuds. Ou encore, on entend une voiture s’approchant, mais on écoute la cadence du moteur, la vitesse de rotation des roues, la position de la source par rapport à nous. 

On fait donc ici la différence dans chaque sens entre son état passif et actif, J.-L. Nancy parle de « nature simple » et d’état « tendu, attentif ou anxieux ». Cet état d’attention crée immédiatement la différence entre un espace et un paysage, l’espace est présent, l’individu en extirpe une nature paysagère par l’attention. Une fois le processus cognitif-perceptif lancé le sujet mesure esthétiquement tous les virons (ciel-terre, terre-mer, soleil-yeux, vent-clapotis, cigales-grillons…). Ainsi le sujet non seulement est dans l’espace, mais fait exister (en son intérieur) les signaux de ce dernier.
Ainsi on ne voit pas et n’on entend pas le paysage, mais plutôt on *le génère par l’écoute et le regard*. Ces états attentifs font surgir le non-vu, le non-commun. La subjectivité est bien au cœur de l’expérience paysagère.

Le paysage se présente donc comme une relation écologique, au sens où l’on entretient une relation sensorielle avec l’espace et cette attention sensorielle permet l’avènement pour nous du paysage. On peut donc considérer la sensation paysagère comme une fascination vis-à-vis d’une expérience dans un espace circonscrit par des seuils perceptifs à travers une (in) certaine temporalité. Cette fascination n’est pas sans rappeler celle convoquée par la sensation de sublime qui se trouve plus être une sensation de vertige, de peur instinctive face à une immensité nous surplombant, qui tétanise. Les cascades et les montagnes dans ce répertoire en tant que site font « impression », mais ce qui les rend *paysage*, c’est leur ancrage dans une réalité géographique et perceptive.

> A model that understands the world, and cinema, to be made up not primarily of objects, substances, structures or representations, but rather of relational processes, encounters, or events[^51]

[^51]: Ivakhiv A., *Ecologies of the moving image: Cinema Affect Nature*, Waterloo: Wilfrid Laurier University Press, 2013 cité dans, Leth Meilvang Emil, « Cinema, meteorology, and the erotics of weather », dans *NECSUS. European Journal of Media Studies*, Printemps 2018, p. 70

---

Dans une démarche que l’on serait tenté de définir comme contraire à l’objectivisme, Schaeffer et Murray-Schaffer, chacun à leur manière, ont tenté de classifier les modalités d’écoute. Schaeffer par exemple au travers d’une nomenclature : d’une part l’écoute quotidienne qui serait purement fonctionnelle, à visée informative (on devine le son), de l’autre l’écoute réduite qui serait une appréciation du son « tel quel », sans tenir compte des informations qui en viennent. M.-S. lui aussi, entrainait ses étudiant·es à écouter les sons pour leurs qualités acoustiques. Cette seconde manière d’écouter, très utile dans un contexte audio-naturaliste, se révèle plus pernicieuse car elle cherche à sortir le son de son contexte (ne serait-ce que temporairement) paysager. En se concentrant sur un son particulier, on oublie toute la chorégraphie dont il fait partie. Les écologistes actuels, utilisant le son dans leurs recherches, ont compris à quel point étaient importantes les correspondances entre sons. Il a été noté que les oiseaux en ville chantaient en effet plus fort que ceux présents dans les milieux ruraux, ou encore que chaque espèce intercalait ses appels par rapport à ceux d’autres espèces dans le même milieu. La durée d’un son n’est alors pas indépendante de son environnement sonore.[^32]

[^32]: Pijanowski Bryan C., Villanueva-Rivera Luis J. et al., « Soundscape Ecology: The Science of Sound in the Landscape », *BioScience*, Volume 61, Mars 2011, pp. 203–216

>Comme ce n’est pas par un acte distinct de l’esprit que nous composons l’idée du timbre, il n’est peut-être pas juste de l’appeler perception ; en percevant le timbre, nous ne mesurons rien. […] L’intensité et le timbre sont des sensations immédiates, dans lesquelles nous ne pouvons remarquer de complexité qu’en nous servant de l’analyse extérieure.[^4]

[^4]: Lagneau, _Célèbres leçons et fragments _, Paris, PUF, 1964, p. 200 cité dans Nancy Jean-Luc, *À l’écoute*, Gallilée, 2002

Cette question du son-objet rejoint d’anciennes problématiques ayant déjà marqués le monde de l’art sculptural et pictural. Joanna Demers l’aborde dans son article en mettant en perspective les travaux de Francisco Lopez et Toshiya Tsunoda avec les sculptures de Donald Judd et Morris entre autres. Lopez notamment affirme que tout travail de paysage sonore (*soundscaping*) se doit de s’émanciper du couple cause-objet sonore. Il faut chercher à supprimer par l’écoute (réduite), toute information localisatrice, il écrase le lieu pour suivre le vent. Arrache le son au paysage pour en faire un objet à part. 

>Reduced listening also focuses so much attention on minute details of sound that it can foster perceptual distortions. More fundamentally, reduced listening *perpetuates the falllacy that there is one universal listening experience untouched by culture*.[^15]

[^15]: Demers Joanna,« Field Recording, Sound Art and Objecthood », *Organised Sound*, Volume 14 , Avril 2009 , p. 42 

[TRACK 04  - Francisco López, Wind[Patagonia], extrait ]

<figure><img src="../../BiblioIconoSono/Images/uploads3wikiartorg00336images_donald-judd_untitled-1994.jpg"><figcaption>Donald Judd, *Sans Titre*, 1994, WikiArt.org</figcaption>

L’instabilité des sources et des timbres sonores dans le paysage, perpétuellement modulés par les aléas alentour en rend la notation et l’extirpation de ces derniers de leur situation un arrachage, qui ne parvient qu’à les amener à un état étrange, celui d’une certaine durée dans l’air.

---

La perception et l’importance que chacun·e accorde au timbre d’un environnement, crée des qualités de paysages différentes, la culture propre à chaque communauté infuse dans le sujet et lui ouvre l’attention vers *quelque chose de particulier*, d’avoir arpenté le paysage inconsciemment l’on se retrouve partie intégrante de ce dernier. La subtilité des timbres présents dans un paysage sera perçue très différemment entre deux individus ayant appartenu à des contextes culturels différents, créant des processus de perception et d’appréciation sensoriels. Ainsi chacun·e des deux perçoit un autre paysage, lui appliquant une grille de lecture propre. Le sensoriel s’allie au culturel pour développer un narratif.

Une fois créé,  le paysage particulier reste en nous, inscrit. Accroché à nos oreilles et nos yeux, rémanence du souvenir. Taches noires et acouphènes. Marqué·es au fer rouge on le reverra dans les cartes postales, le ré-entendra dans le chant d’une moissonneuse.

---

> Un jour viendra où nous saurons percevoir à la mesure de nos souvenirs, où nous nous *ressouviendrons pour mieux ressentir*[^8]

[^8]: Sansot Pierre, *Variations Paysagères*, Paris, Klincksieck, 1983, p. 30

Le paysage prenant pour origine une perception sensible et attentive fusionne avec le sujet, iel se retrouve non pas pénétré, mais résonnant avec le paysage. Le cœur qui suit le rythme de la rivière et la peau qui s’harmonise avec le soleil du Midi.

Lors des balades d’été dans les vignes, le soleil frappait ma peau et pour souffler un peu, j’allais m’asseoir à l’ombre de l’ancien four à chaux. Une structure en pierre en ruine, laissant apparaître 8 murs, pas de plafonds, les fours ainsi que l’arbre qui venait m’abriter. Les cigales et la brise tendaient tout l’espace ensemble, le bourdon le remplissant et le vent faisait revivre quelques instants la terre, bruissait dans l’arbre et quelques jours, soufflait le calcaire des pierres. La pendularité de cette balade de quelques kilomètres, réalisée le plus souvent seul, à pied à vélo tissait les liens entre l’ancienne bergerie, le bêlement des moutons parfois, le four à chaux comme « sites » et les différentes pierres, le chemin, mes pas, la légère pente qui menait à la vigne, les ronces, les cigales, les voitures qui passaient plus loin sur la D30… Chaque élément se retrouvait à cet endroit afin de reformuler d’une autre manière le paysage de ce lieu. 

> D’une part il [le paysage] se structure, il s’organise, il se refuse à moi dans son originelle altérité, il se singularise par des signes qui nécessitent la plus grande attention. D’autre part, il *s’atmosphérise, il devient une vibration, une odeur, une émotion unique, il se mêle à moi comme s’il n’était pas distinct de moi*.[^812]

[^812]: Idem.

---

>[…] no matter how proximate, objects are always given across an intervening space. Seeing acknowledges distance even while compensating for it by bringing what is far off into visibility.[^78]

[^3]: Wyschogrod Edith, « Doing before hearing : on the primacy of touch », dans _Textes pour Emmanuel Levinas _, Paris, Jean-Michel Place, 1980, p. 181

[^78]: Wyschogrod Edith, « Doing before hearing : on the primacy of touch », op. cit., p. 188


Le paysage visuel relèverait alors de l’acte de regard, de mesure esthétique des choses. Pas au sens du beau, mais plutôt dans le sens d’une *appréciation sensible* de l’espace, de la distribution du vide, des distances. Les différentes composantes visuelles permettront, par leur frontalité et leurs limitations, au sujet de sentir si telle « vue » fait paysage. C’est « L’expérience d’un morceau d’espace perçu d’un coup »[^18].

[^18]: Jakob Michael, *Le paysage*, op. cit., p.38

>Dans les instants les plus rares, on pourrait définir la musique : *quelque chose de moins sonore que le sonore*. Quelque chose qui *lie le bruyant* (Pour le dire autrement : un bout de sonore ligoté (p.24)

Le paysage sonore, a longtemps été considéré comme une façon musicale d’écouter les sons de l’environnement, tout particulièrement car ces bruits se lient, se répondent et s’entrechoquent. Si l’on a considéré le paysage (le monde) comme musique, c’est car on a cru entendre un orchestre alors. Pourtant plus simplement, chronologiquement l’orchestre a écouté les paysages.

Si le bruyant est lié dans le paysage, il s’entrelace avec la lumière, avec l’air ambiant, car dans leur complémentarité ils tissent la mesure spatiale, couvrent et découvrent l’espace, distribuent le sol et le ciel. Le sol où s’arrêtent et se répercutent les sons, où la lumière choit et le ciel d’où elle vient et d’où ils semblent descendre. Le point de percept paysager est ancré par la gravité et se tend vers l’infini horizontal, s’échappe en un point de fuite, au seuil acoustique et visuel.

 L’infini relève de l’arpège, de la gradation des sons allant de l’étouffé au clair, du murmure à l’assourdissement, puis peut être de nappes plus calmes. Tout cela advenant séparément, se recomposant  dans l’oreille, l’œil et le cœur, nous laissant le loisir d’une autre mesure paysagère. On perçoit alors une autre limite à l’infini, un autre horizon, plutôt un tissu tendu de nous jusqu’au seuil du sensible.


>Le paysage, donc […], n’est pas une représentation de l’infini, mais une mise en présence de l’infini.[^34]

[^34]: Balibar Justine, *Qu’est ce qu’un paysage*, op. cit., p.120

>Le ciel limpide parmi les entrelacs de la vigne et des roses[^60] 

[^60]: Rolland Romain, *Jean-Christophe Tome 9 : Le buisson ardent*, 1911, cité dans Charles Daniel, *Gloses sur John Cage*, UGE, « 1018 », 1978

Le paysage, entrelac du fini et de l’infini.
 
---

>Sound converges on a potential listening point from every direction […]. The result is and *auditory array*, analogous to the optical array. […] Reverberant sound is informative about the size and layout of the surrounding environment.[^7]
>
> sound provides information about _an interaction of materials at a location in an environment_.[^7]

[^7]: Gaver William M., « What in the world do we hear? An ecological approach to auditory event perception », *Ecological Psychology*, Mars 1993, p. 4  
 
À noter que l’on ne peut simplifier l’espace que suggère le sonore à une simple identité entre la hauteur d’une note et sa distance perçue avec le sujet écoutant. Par exemple, un ultrason très haut pourrait se situer tout aussi loin qu’un tremblement sourd. L’information sensible d’une source et de sa position nous renvoie à notre propre situation dans l’espace, les opérations cognitives qui se succèdent nous transportent dans l’espace. Un paysage se constitue de subtils déphasages des hauteurs et des volumes, permettant de s’identifier, la multiplicité des timbres des différents sons traversant l’espace  créent le timbre propre au paysage en cet instant.

 <figure><img width="350" src="../../wshop_g/7.jpg"></img><figcaption>Zach Liebermann, Horizons, 2022, post Instagram</figcaption></figure> – >Light makes the thing appear by dissipating shadow so that objects emerge against the backdrop of empty space, of a void from which shadow has been disbursed ; the object appears to originate from nothingness.[^333]

[^333]: Wyschogrod Edith, « Doing before hearing : on the primacy of touch », op. cit., p. 182


La vue sort alors des ténèbres les objets silencieux, le regard leur distribue la distance, les mesure, les comprends. Si la vue atteste de l’existence, le regard estime le rapport à soi. La proprioception prend sa forme. Le paysage se matérialise dans la mesure d’objets, de leur distance par rapport à soi ainsi qu’avec un seuil (*threshold*) variant. Dehors, seuls le ciel et la mer en sont les limites.

>[…] car le bruit ne provient pas de la région sereine du ciel.[^19]

[^19]: Lucrèce, *De rerum natura*, livre IV


<figure><img src="../../BiblioIconoSono/Images/Yannick Dauby - Penghu July 2020.png"><figcaption>Dauby Yannick, Penghu July 2020</figcaption></figure>

[TRACK 05 – Dauby Yannick, Dit lip hue hng 直入花園,  extrait]

---

L'oeil lui contracte l'espace, comprime les distances, déplaçant ( map() ) ces dernières sur le spectre coloré, renvoie un *fisheye*. Le regard, lui, les réinterprète, visualise les virons, crée les contours aux formes et les retourne, les écrase et recrache un diaporama.

Le *fisheye* grossit les limites, fait fuir l’horizon et allonge les distances. L’écoute, elle, écrase et redéploie continuellement l’espace. Le son est traité pour nous recentrer (audiopositionner) dans l’espace, raffermir les distances avec les sources, affirmer leur présence, leur réalité. L’écoute décuple le regard, lui soutire une mesure et la confirme, dans l’immobilité rajoute le déplacement, dans le point de fuite rajoute la compression.

<figure><img width="350" src="../../BiblioIconoSono/Images/Fallen Sky (2021), Sarah Sze, at Storm King Art Center, Mountainville, New York. Photos by Nick Knight.jpg"></img><figcaption>Sze Sarah, *Fallen Sky*, Sarah Sze, Storm King Art Center, Mountainville, New York, 2021</figcaption></figure>

> Il n’y a rien dans le sonore qui nous renvoie de nous-mêmes une image localisable, *symétrique*, inversée, comme le fait le miroir. En latin le reflet se dit *repercussio*. L’image est une poupée localisable.[^22]

[^22]: Quignard 	Pascal, *La Haine de la musique* (1996), Gallimard, « Folio », 2002, p.112

Il faudrait prendre l’oreille comme un miroir des sons, une sorte de dispositif sonifiant, qui renvoie les sons à eux-mêmes, qui les tord de leur source dans la cave de l’oreille, les noues dans la cage de la perception, les nerfs optiques surchargés. Le tympan déplace le son en le faisant résonner et le cerveau le réinterprète. Il se dé-charge sur le cerveau, lui laisse trouver l’où et le quoi alors que les yeux, les cônes se sur-chargent, ne sous-traitent pas la vue. Tout est rendu identifiable dès que découvert.

Lorsque l’on découvre un paysage, on prend le bout de terre et on le dissèque, enregistrant physiquement toutes ses caractéristiques. Le regard, pour peu que la lumière soit là, laisse alors de place à l’imagination. Le faisceau visuel incide sur la forme et tend à la circonscrire, à forcer les couleurs à se fixer à l’œil, l’onde est concentrée (coincée) dans la rétine. Le son (bruissement), caché au-dessous des formes, vient parer, recouvrir le paysage mis à nu.

Le sonore, a contrario du lumineux, rebondit dans le temps. Le mur du son.

> Si nous consentions à établir à nouveau une opposition un peu forcée entre le visuel et le sonore, nous dirions que le premier nous permet de mieux distribuer et de mieux articuler le paysage tandis que le second nous assure le plus souvent de son existence[^9]

[^9]: Sansot Pierre, *Variations Paysagères*, op. cit., pp. 82-83


L’œil consent au chaos, y déniche des régularités, des lois, l’oreille, est contrainte de perdre la trace de ce qu’elle a retranscrit dès que le signal a été émis, le son n’existe qu’au présent.

---

Le son crée le temps, rend le visuel _tactile_[^3], car la vue prend alors vie, on s’éloigne du paysage silencieux - un silence de mort - pour insuffler le mouvement depuis un point de perception fixe. Si l’espace visuel immobile est considéré comme _zone morte_ alors la simple ouverture à l’écoute induit des échelles, des _déplacements_, des _variations_, fade-in & out d’une voiture, bourdon de la rivière, un bang quelques rues plus loin… Au bref, la vie. Le son-paysage fait en nous *sonner* l’espace, le recompose en lui incorporant du temps, du mouvement. Même à l’arrêt, les sons et leurs sources semblent dessiner une toute autre architecture, un tout autre rapport sensoriel.

>S’absenter de l’environ n’est pas possible pour l’ouïe. Il n’y a pas de paysage sonore parce que le paysage suppose l’écart devant le visible. *Il n’y a pas d’écart devant le sonore.*[^37]

[^37]: Quignard	Pascal, *La Haine de la musique*, op. cit., p. 110


Le noise se glisse « au-dessus et au-dessous »[^35] de l’entente, il prend l’interstice comme tangente. Ombre sonore. Il se fond dans ce qu’on appelle le silence, il constitue la matrice même, la matière noire que l’on cherche inlassablement renferme la même potentialité que le bruissement de fond.

Le fond sonore est l’espace possible dans le temps.

>The background noise never ceases; it is *limitless, continuous, unending, unchanging*. It has itself no background, no contradictory. [...][^36]

[^35]: Charles Daniel, *Gloses sur John Cage*, op. cit., p.93
[^36]: Cox Christoph, « Sound Art and the Sonic Unconscious », *Organised Sound*, Volume 14, p. 19

---

Le paysage est vu comme arrêté, figé par l’huile et l’encre, mais il se trouve qu’il est inexorablement teinté par le temps, rien qu’une photo, de par son diaphragme et sa vitesse d’obturation fera entrer *un certain temps* dans l’appareil. La photo est une boucle musicale.

> La photographie n’enregistre pas la réalité matérielle qui se trouve devant l’objectif, mais son aspect visible, déterminé par le point de vue et le champ visuel à un moment précis et dans un éclairage donné. [...]  [le photographe] s’expose à créer des _hiatus_, des télescopages, des juxtapositions fortuites, des condensations et _des lacunes inattendues dans la logique de l’organisation spatiale_.[^56]

Opérer une coupure dans le temps pour archiver un morceau d’espace, c’est approcher le paysage de la perspective du médecin légiste : on incise, découpe, retourne une peau en voie de putréfaction pour se rendre compte de ce qui l’a amenée là. Le sensible se glisse parfois dans l’arrêt du temps, dans les images de cadavres.

[^56]:  Peter Galassi, *Before Photography: Painting and the Invention of Photography*, New York, MoMa, 1981, cité dans Desportes Marc, *Paysages en mouvement*, Gallimard, 2005

---

>Là où la présence visible ou tactile se tient dans un « en même temps » _immobile_, la présence sonore est un « en même temps » _essentiellement mobile_, vibrant de l’_aller-retour entre la source et l’oreille_, à travers l’espace ouvert.[^2]

[^2]: Nancy Jean-Luc, *À l’écoute*, Galilée, 2002, p. 36

>chaque son, chaque bloc devient image. De son, *il vire en bruit*. [^38]

[^38]: Charles Daniel, *Gloses sur J. Cage*, op. cit., p. 207

 <figure><img width="350" src="../../whsop_g/4.png"><figcaption>Hamadicharef Brahim, *Choir waveform sound analysis*, dans *Objective Prediction of Sound Synthesis Quality*, consulté en novembre 2022 [https://www.researchgate.net/publication/228972415_Objective_Prediction_of_Sound_Synthesis_Quality](https://www.researchgate.net/publication/228972415_Objective_Prediction_of_Sound_Synthesis_Quality)</figcaption></figure>


>Penser le Temps - c’est penser l’identité de l’aller et du retour. […] dans le même trait *revenir*.[^89]

[^89]: Quignard Pascal, *La Haine de la musique*, op. cit., p. 230

---

Pour peu qu’un son se joue on percevra le temps, le marqueur direct de ce dernier serait le battement de l’horloge, métronome cadencé au soleil. Dans le paysage, si la dimension temporelle est omniprésente, elle se cadence différemment des notations normées. En habitant l’espace, on ne divise pas le temps en heures et secondes, mais plutôt en rythme avec les autres sons présents, chaque espace revêtant dans le cycle des saisons et dans les rythmes du soleil et de la lune, des aspects sonores différents. J’entends la nuit les grillons ainsi que le bruissement d’une chouette, le jour il y aurait plutôt le vrombissement des automobiles parcourant la départementale, mêlée au chant d’un merle.

>La seule introduction possible du langage dans la musique est celle des conjonctions, [mais, où, est, donc, or, ni, car][^92]

[^92]: Nancy Jean-Luc, *À l’écoute*, op. cit., p. 43

Si le son est conjonction, le visuel est les noms et le sujet est lui-même, performant les verbes. Il y a donc bien une sorte de _dynamique_ son et image, composer un paysage en omettant le son reviendrait à lui enlever des _connecteurs _, ce qui ne va pas forcément ruiner la phrase « La mer, belle bleue », mais lui enlever une composante fine de cette dernière « La mer, belle et bleue », « La mer, belle car bleue ». Je m’avancerai et dirait que le sonore est de l’ordre de la _ponctuation et de la diacritique _, le son souligne, sur-signifie, transforme, retarde, rapproche, _respire_.

--- 

[TRACK 06 – FieldReorder 01, Martina Lussi & Tim Shaw]

>Toute vibration qui approche le battement du cœur et le rythme du souffle entraine une même contraction [^76]

[^76]: Charles Daniel, *Gloses sur John Cage*, op. cit., p. 53

L’air comprimé et relâché, par les variations de pressions, température et souffle, traverse et reforme un espace-son, l’air, l’invisible dans le vide formel, charge des poumons vers le cœur, le paysage exhale en continu ; nous inspirons son haleine, mélangeant notre cadence à la sienne, au rythme scandé.

>*Les mots forment chaîne dans le souffle. Les images forment rêve dans la nuit. Les sons aussi forment chaîne le long des jours.* [^88]

[^88]: Quignard Pascal, *La Haine de la musique*, op.cit., p. 55

>Nous sommes de l’eau, de la terre, de la lumière et de l’air contractés, non seulement avant de les reconnaître ou de les représenter, mais avant de les sentir. [^11]

[^11]: Deleuze Gilles, *Différence et répétition*, Paris, PUF, 1968, p. 99

---

Les vagues c’est un peu la montée en tension du tranquille. Le va vient incessant, le clapotis que l’on voit grimper en vague et qui alors sous l’eau, fait roulement de tambour sourd. On voit alors la vague qui monte, monte, gronde et puis s’enroule sèchement sur elle-même dans un éclatement sonore et visuel (l’écume qui s’éparpille en diacritiques) et tout cela retombe en bruissant vers vous jusqu’à mouiller les pieds. Le reste de vague, insignifiant milli- puis micro- puis pico -mètre griffe un peu le sable et couine fshhh, pauvre et s’écrase dans un bruit blanc, laisse un silence bref, et de l’écume parfois.

La vague s’écrase dans le monde pour y revenir plus tard, elle s’échoue sur le sable pour mieux le ronger, pour mieux sculpter le bord de mer le *shore*, l’attaque de l’écume se situe dans son point de rupture avec la roche où elle claque contre elle. L’attaque sonore de la vague en haute mer se meut en un bruit sourd que seule la terreur permet d’entendre. Le tangage énorme qu’elle crée participe à bouger l’horizon, à rendre le paysage flou en nous supprimant toute possibilité d’agir dans le mouvement.

---

Se mou-voir dans l’espace, c’est fondre l’une dans l’autre des images et des intensités sonores, et avec la vitesse les moduler. En courant, je changerais la hauteur du passereau, je verrais en roulant les garde-fous autoroutiers perdre leurs vis pour s’effilocher en une bande grise.

>Il semble même qu’une part du spectacle réside dans sa _dynamique_. C’est au cours d’un mouvement que peut-être apprécié le jeu des ondulations et des perspectives changeantes.[^71]

[^71]: Desportes Marc, *Paysages en Mouvement*, Paris, Gallimard, 2005, op. cit., p. 73

<figure>
<img width="350" src="../../BiblioIconoSono/videos/v.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_2.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_3.jpg">
<img width="350" src="../..//BiblioIconoSono/videos/vimeo_4.jpg">
<figcaption>Charrière Julian, An invitation to disappear, 2018, captures d’écran de vidéo, consulté  en novembre, https://www.youtube.com/watch?v=0Tvb4E8CAmU</figcaption>
</figure>

Le paysage dans le déplacement, se déforme, en des fils, des chemins, un élément glisse vers l’autre. La vallée est un chemin du ciel aux rias, le son de l’eau de la ria se répercute dans la vallée et le sujet écoutant perçoit l’eau et la vallée en lui-même. Ingold parle d’une *danse de l’agentivité*[^70] On ne perçoit qu’une partie de la nasse et ses enchevêtrements complexes de fils se compénétrant inlassablement parviennent à tisser la globalité paysage dans laquelle chaque élément peut ressurgir indépendamment. La maille, à la différence du réseau, s’étire en puissance dans le temps, un nœud étant toujours possible, tant qu’on aura a minima deux éléments flottants.

[^70]: Ingold Tim, *Dance of Agency*, 2013, cité dans Brown Steven D., Kanyeredzi Ava, McGrath Laura, Reavey Paula & Tucker Ian, « Affect theory and the concept of atmosphere », *Distinktion, Journal of Social Theory*, Volume 20, pp. 5-24

---

>Les vents courent, volent, s’abattent, finissent, recommencent, planent, siffle, mugissent, rient ; frénétiques, lascifs, effrénés, prenant leurs aises sur la vague irascible. Ces hurleurs ont une harmonie. Ils font tout le ciel sonore. Ils soufflent dans la nuée comme dans un cuivre, ils embouchent l’espace ; et ils chantent dans l’infini, avec toutes les voix amalgamées des clairons, des buccins, des olifants, des bugles et des trompettes, une sorte de fanfare prométhéenne.[^21]

[^21]: Hugo Victor, *Les Travailleurs de la Mer*, II, cité dans Murray-Schaffer R., *Le Paysage sonore, le monde comme musique* (1977), Wildproject, « Domaine sauvage », trad. Sylvette Gleize, 2010




