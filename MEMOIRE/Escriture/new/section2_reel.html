<h1 id="section-ii-atmosphères-paysages">Section II : Atmosphères &amp;
Paysages</h1>
<hr />
<p>[TRACK 07 - Chihei Hatakeyama - Starlight and Black Echo,
extrait]</p>
<figure>
<img src="../../BiblioIconoSono/Images/Quayola_Landscape_dateUnknown_videoscreencap.jpg"
style="transform:rotate(90deg);">
<figcaption>
Quayola, <i>Landscape_Painting</i>, capture d’écran d’une vidéo,
consulté en novembre 2022
https://quayola.com/work/series/landscape-paintings.php
</figcaption>
</figure>
<blockquote>
<p>Atmospheres, to be sure, are not things<span
class="sidenote-wrapper"><label for="sn-0" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-0" class="margin-toggle"/><span
class="sidenote">Böhme Gernot, « The art of the stage set as a paradigm
for an aesthetics of atmospheres », <em>Ambiances</em>, 2013, connection
on 22 September 2020 <a
href="http://journals.openedition.org/ambiances/315">http://journals.openedition.org/ambiances/315</a><br />
<br />
</span></span></p>
</blockquote>
<hr />
<p>L’atmosphère est un « quasi objet » selon Böhme, elle est un
objet-processus, à la manière du paysage elle <em>semble se former</em>
dans le nœud d’interaction entre un instant, un espace et un sujet
sensible. Toute la tension de l’atmosphère repose, elle aussi sur
l’aisthesis et sur l’attention portée par un sujet spectateurice. C’est
une relation entre le subjectif et l’objectif qui fait le lien entre
qualités dans l’espace et sensations en puissance dans la personne<span
class="sidenote-wrapper"><label for="sn-1" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-1" class="margin-toggle"/><span
class="sidenote">Böhme Gernot, « The theory of atmospheres and its
applications », <em>Interstices: Journal of Architecture and Related
Arts</em>, pp. 93-100<br />
<br />
</span></span>.</p>
<p>Indicible, l’atmosphère rentre dans cette catégorie de concepts où
l’on peut discourir longuement sans jamais mettre vraiment le doigt
dessus, filant sous la précision d’une explication universelle et
rationnelle. Elle s’apparente à la transe où à l’extase religieuse, une
sensation loin des mots, présente dans l’espace par les sens.</p>
<blockquote>
<p>The reason is primarily that atmospheres are totalities: atmospheres
imbue everything, they tinge the whole of the world or a view, they
bathe everything in a certain light, unify a diversity of impressions in
a single emotive state<span
class="sidenote-wrapper"><label for="sn-2" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-2" class="margin-toggle"/><span
class="sidenote">Böhme Gernot, « The art of the stage set as a paradigm
for an aesthetics of atmospheres », <em>Ambiances</em>, 2013, connection
on 22 September 2020 <a
href="http://journals.openedition.org/ambiances/315">http://journals.openedition.org/ambiances/315</a><br />
<br />
</span></span></p>
</blockquote>
<figure>
<img src="../../BiblioIconoSono/Images/monet_nymphéas.jpg">
<figcaption>
Monet Claude, <i>Nymphéas</i>, 1917, Wikimedia Commons
</figcaption>
</figure>
<p>Le son et l’image ne sont pas atmosphère, tout comme le son et
l’image ne sont pas le paysage. Les deux sont autopoïétiques et les
circonstances environnantes, son et image en seraient plutôt des
composantes que des causes ou conséquences. Ce sont des expériences qui
se situent à la frontière des sens, et penser que tel son ou image
provoque ou découle de l’atmosphère/du paysage relève de l’apophénie, au
sens où le paysage et l’atmosphère ne sont pas des objets perçus, mais
plutôt l’infiligement d’un discours à une sensation étrange, l’arbre qui
tombe dans la forêt et l’orage.</p>
<hr />
<blockquote>
<p>Écouter, c’est se mettre soi-même en tension, et être écoutant
confère à la fois le sentiment d’être dans l’espace et de faire résonner
l’espace. […] En faisant résonner le son, en ressentant l’atmosphère,
l’écoutant fait résonner l’espace et, ce faisant, il crée l’espace.<span
class="sidenote-wrapper"><label for="sn-3" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-3" class="margin-toggle"/><span
class="sidenote">Böhme Gernot, « Théorie des atmosphères. L’écoute, le
silence et l’attention au théâtre », dans <em>Le son du théâtre (XIXe –
XXIe siècle)</em>, dir. Marie-Madeleine Mervant-Roux &amp; Larrue
Jean-Marc, CNRS éditions, 2016, p. 209<br />
<br />
</span></span></p>
</blockquote>
<p>L’atmosphère n’implique nulle mesure des distances ou perspective. Il
y a plutôt la sensation de quelque chose de différent, d’une présence ou
d’une sensation. Un <em>je ne sais quoi</em> dans l’air, la chaleur qui
fait trembler la vue.</p>
<p>Elle se retrouve énormément dans l’importance du climat dans les
scènes cinématographiques, selon Emil Leth Meilvang, dans son article
<em>Erotics of Weather</em>, où il analyse des livres de la collection «
Côté Cinéma / Motifs », notamment ceux sur la pluie, le vent et la
neige. L’intuition est juste, car si l’atmosphère réside dans l’air,
l’on peut penser que le climat extérieur et les variations
atmosphériques qui en résultent jouent sur notre ressenti: il est
indéniable que la lourdeur pèse « comme un couvercle » sur l’humeur.</p>
<figure>
<img src="../../BiblioIconoSono/Images/Fig3.png">
<figcaption>
capture d’écran d’un film inconnu, depuis Leth Meilvang Emil, « Cinema,
meteorology, and the erotics of weather », dans <i>NECSUS. European
Journal of Media Studies</i>
</figcaption>
</figure>
<hr />
<p>L’atmosphère par rapport au paysage, c’est le verbe et la marque
temporelle. L’atmosphère arrive, elle fait et transgresse les limites du
fini et de l’infini. C’est ce flottement hagard avant la gelure, la
paupière lourde sous le zénith. La force des choses présentes dans le
paysage, une puissance tranquille existant dans l’entre deux.</p>
<blockquote>
<p>The spaces generated by light and sound are no longer something
perceived at a distance, but something within which one is
enclosed.<span
class="sidenote-wrapper"><label for="sn-4" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-4" class="margin-toggle"/><span
class="sidenote">Böhme Gernot, « The art of the stage set as a paradigm
for an aesthetics of atmospheres », op. cit.<br />
<br />
</span></span></p>
</blockquote>
<p>[chihei end]</p>
<figure>
<img src="../../whsop_g/PSM_V13_D184_Sound_wave_experiment.jpg">
<figcaption>
</figcaption>
</figure>
<hr />
<p>L’atmosphère est à l’image de la teinte chez Rothko, elle semble
indépendante de la structure spatiale et pourtant elle crée l’accord,
définit une voie(x) parmi les possibles flottants.</p>
<figure>
<img src="../../BiblioIconoSono/Images/Untitled, Black on Gray, 1969, Rothko.jpg">
<figcaption>
Rothko Mark, <i>Black on Gray</i>, 1969, Wikimedia Commons
</figcaption>
</figure>
<blockquote>
<p>Rouge condensé par noir rouge rendu dense par noir<br />
progression de rouge dense comme des marches de lave<br />
rouge dense comme des marches de lave pour nous réunir là<br />
nous réunir avant d’avoir à effectuer les passages<br />
marches rouges nous mènent à trois chambres rouges<br />
chambres de lumière rouge dense rouge condensé par noir<span
class="sidenote-wrapper"><label for="sn-5" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-5" class="margin-toggle"/><span
class="sidenote">Taggart John, <em>Le poème de la Chapelle Rothko</em>,
trad. Alféri Pierre et Hocquard Emmanuel, Éditions Royaumont, « Un
bureau sur l’Atlantique », 1990<br />
<br />
</span></span></p>
</blockquote>
<p>L’espace s’arrête au fini, les sens tendent vers l’illimité et entre
les sens, sous la tension attentive de l’écoute et du regard, se niche
la sensation que quelque chose se passe. On perd peu à peu pied et du
ressenti découle une forme de vertige, un évènement non-situable, éthéré
qui baigne l’espace à la façon d’une ondée. Le signal romantique
déclenché fait découler de l’au-dessous de la forme paysagère.</p>
<hr />
<p>L’atmosphère semble faire lien entre la résonnance du sujet et de
l’espace, c’est son, point de départ, l’attaque de l’archet sur la corde
asème au travers l’écho. Les deux s’accordent, vibrant soudain sur un
certain accord. L’atmosphère alors, s’étendrait, soufflant sur l’espace,
respirant les murs et contractant les vides. Nos poumons se remplissent
de toutes les sensations que nos yeux et oreilles captent, les
étalonnent à la hauteur de sonnance générale.</p>
<blockquote>
<p>Reverb at once <em>represents</em> space and <em>constructs</em> it.
<span
class="sidenote-wrapper"><label for="sn-6" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-6" class="margin-toggle"/><span
class="sidenote">Sterne Jonathan, « Space within Space: Artificial
Reverb and the Detachable Echo », <em>Grey Room</em>, MIT Press, Volume
60, 2015, p. 112<br />
<br />
</span></span></p>
</blockquote>
<p>Le paysage re-sonne en nous, éternellement, à la manière du chœur
dans les cathédrales.</p>
<hr />
<p>[TRACK 08 - Forest Tone]</p>
<p>[TRACK 08BIS - Room Tone]</p>
<p>Le paysage ouvert ne laisse que peu survenir sa résonnance, mais dans
de rares instants, si l’on se place à un certain point et que l’on
articule, une réponse se mêle et semble s’étirer jusqu’au fond du
visible.</p>
<hr />
<p>Daphnis raconte à Chloé que Écho, mortelle, élevée par les nymphes et
muses, chantait si bien que Pan, jaloux et frustré par le talent d’Écho
ainsi que par le fait qu’elle l’ait repoussé, a soufflé la panique sur
les hommes alentour. Ces derniers la déchirèrent alors qu’elle chantait
encore et « dispersèrent ses membres pleins d’harmonie ». Et ainsi Écho,
éparpillée sur la Terre, ses os devenus pierres, ses cheveux feuilles et
sa peau humus, continue éternellement de contrefaire les sons, d’imiter
ce qui a été émis.</p>
<p>En production musicale, lorsque l’on applique un effet à un son, la
sortie modifiée est qualifiée de <em>wet</em> (mouillé), au contraire un
son sans effet sera <em>dry</em> (sec). Le seul son <em>dry</em>
possible est celui transitant directement de l’émetteur à l’oreille, la
seule caisse de résonance possible étant celle de l’instrument, un son,
quant émis dans l’espace, sera lui forcément <em>wet</em>, car la
réverbération y est présente. L’écho c’est la redite et l’espace qui
reflète en quantité le son, le rend à chaque couche de réverbération
plus confus, pour que ce dernier calque le contour de l’espace sur
lequel il a été projeté. L’écho devient la plainte des restes de la
chanteuse, récupérant un peu de son sur les flancs de montagnes. L’onde
est mouillée par les larmes de la morte.</p>
<p>Le son est ainsi découpé par l’espace et même si le son original
parvient à l’auditeurice, il est complété par ses restes réverbérés. Le
son est contrefait pour situer l’air, l’écho fait apparaître et trembler
l’invisible.</p>
<blockquote>
<p>L’écho est la voix de l’invisible. […] Dans l’écho, l’émetteur ne se
rencontre pas. C’est le cache-cache entre le visible et l’audible.<span
class="sidenote-wrapper"><label for="sn-7" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-7" class="margin-toggle"/><span
class="sidenote">Quignard Pascal, <em>La Haine de la musique</em>
(1996), Gallimard, « Folio », 2002, p.149<br />
<br />
</span></span></p>
</blockquote>
<hr />
<p>L’atmosphère, en sa brève résonnance de l’espace, fait émerger des
sources cachées et nous de ses sources on vibre, dans cette apothéose
chère aux romantiques, du paysage.</p>
<blockquote>
<p>L’écho affirme la présence d’un son en révélant le milieu dans lequel
il se déploie, les points où il se réfléchit délimitant alors son
périmètre<span
class="sidenote-wrapper"><label for="sn-8" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-8" class="margin-toggle"/><span
class="sidenote">Bonnet François J. , <em>Les mots et les sons, un
archipel sonore</em>, L’Éclat, 2012, p. 34<br />
<br />
</span></span></p>
</blockquote>
<figure>
<img src="../../BiblioIconoSono/Images/Caspar_David_Friedrich_-_Tageszeitenzyklus,_Der_Morgen_(1821-22).jpg">
<caption>
Friedrich Caspar David, <i>Tageszeitenzyklus, Der Morgen</i>,1821,
Wikimedia Commons
</figcaption>
</figure>
<p>La brume est l’équivalent pictural de ce que l’écho évoque en rapport
avec l’atmosphère, ce sont des moyens de simuler la sensation de
l’espace résonnant et fin, la brume insaisissable et couvrante et l’écho
glossolalie sur surfaces. Les deux - et c’est la particularité de
l’atmosphère aussi - semblent donner du poids à l’air. Le paysage se
recroqueville, l’infini reste, mais prend la forme d’un vase clos.
L’environnement change de géométrie et laisse apparaître le vecteur
sensible comme une quatrième direction.</p>
<hr />
<p>[TRACK 09 - Yau Randy H.Y., Arford Scott, Infrasound - Live in
Paris]</p>
<blockquote>
<p>The architectural space becomes an acoustic container, reacting and
multiplying low frequency arrays—solidifying a void where sound pressure
levels interject between audience and space. It is about the total
acoustic sense of space—observing sound to measure the capacity of
architecture.<span
class="sidenote-wrapper"><label for="sn-9" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-9" class="margin-toggle"/><span
class="sidenote">Arford Scott, Yau Randy H.Y., <em>Manifesto for
INFRASOUND 19 - Paris</em>, Erratum Musical, 2014, consulté en mai 2022
<a
href="https://soundcloud.com/erratum_musical/infrasound-19-randy-yau-scott-arford">https://soundcloud.com/erratum_musical/infrasound-19-randy-yau-scott-arford</a><br />
<br />
</span></span></p>
<p>Resonance: Activating space to generate sympathetic vibration in both
body and architecture— making all fields inseparable and one. It is
about the phenomenon of resonance or sympathetic vibration—all things
working in one continuum.<span
class="sidenote-wrapper"><label for="sn-10" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-10" class="margin-toggle"/><span
class="sidenote">Arford Scott, Yau Randy H.Y., <em>Manifesto for
INFRASOUND 19 - Paris</em>, Erratum Musical, 2014, consulté en mai 2022
<a
href="https://soundcloud.com/erratum_musical/infrasound-19-randy-yau-scott-arford">https://soundcloud.com/erratum_musical/infrasound-19-randy-yau-scott-arford</a><br />
<br />
</span></span></p>
</blockquote>
<p>La (dé)multiplication d’une onde sonore à travers l’espace crée un
tremblement, une vibration. Cette onde entre en <em>phase</em> avec
l’espace, alors que la lumière se voit éparpillée, absorbée par les
matériaux. Le son contraignant l’espace s’accole à la lumière
contrainte, dans le tremblement infime de la Terre se diffuse le
souffle.</p>
<blockquote>
<p>Chaque son est une minuscule terreur. <em>Tremit</em>. Il vibre<span
class="sidenote-wrapper"><label for="sn-11" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-11" class="margin-toggle"/><span
class="sidenote">Quignard Pascal, <em>La Haine de la musique</em>, op.
cit., p. 32<br />
<br />
</span></span></p>
</blockquote>
<p>Chaque vibration dans l’air et l’espace, chaque ondulation dans le
souffle (<em>pneuma</em>) entraîne une imperceptible modification de la
densité de l’espace, le corpusculaire pataugeant dans le reste, les
vibrations s’entrechoquant et créant de la chaleur. L’aurore boréale,
elle, résulte de conditions particulières des champs magnétiques, elles
sont les témoins des perturbations de l’invisible, dans leur écoulement
tranquille et lumineux elles mettent au jour l’avènement de l’atmosphère
plus haut dans le ciel. Elle est le tremblement des pôles, la peur
évacuée dans le ciel sous forme de signal.</p>
<figure>
<img src="../../BiblioIconoSono/Images/NIPR_image2.jpg">
<figcaption>
All-Sky Camera NIPR, Syowa Aurora Station,
http://polaris.nipr.ac.jp/~aurora
</figcaption>
</figure>
<p>[FIN INFRASOUND]</p>
<p>Les meubles qui vibrent à l’unisson avec un son, c’est leur manière
de s’accorder. La lumière qui irradie l’espace, c’est le ton à
donner.</p>
<hr />
<blockquote>
<p>Tone and emanation - in my terminology, ekstases - determine the
atmosphere radiated by things. They are therefore the way in which
things are felt present in space.<span
class="sidenote-wrapper"><label for="sn-12" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-12" class="margin-toggle"/><span
class="sidenote">Böhme Gernot, « The art of the stage set as a paradigm
for an aesthetics of atmospheres », op. cit.<br />
<br />
</span></span></p>
</blockquote>
<figure>
<img src="../../BiblioIconoSono/Images/annv.png">
<figcaption>
Janssens Ann-Veronica, <i>Exhibition view</i>, 2019
</figcaption>
</figure>
<p>Walter Benjamin argue que la multiplication (des photographies) par
la technique leur retire leur « aura », que le fait qu’une pièce/qu’un
objet soit unique lui procure une certaine impression sur le sujet. En
dérivant de son argument, on pourrait supposer une notion de
sur-saturation de cette aura dans l’œuvre (re)produite. Une saturation
qui arriverait au contraire de l’aura de l’œuvre unique, la
reproductibilité entraînant une charge supplémentaire, celle de la
recréation d’un dessein. Sur-saturation de ce qui est fait pour être vu,
entendu, une surcharge allopoïétique. Le paysage (son expérience) de ce
fait, détonne aussi par son mode de génération. Si les objets
artistiques tels que les photographies de paysage, les enregistrements
de soundscape sont débordés par le point de vue des artistes (on
pourrait alors dire préperçu, en analogie au prémâché), l’expérience
paysagère elle, crée sa propre aura, où le paysage naît dans le regard,
dans l’écoute d’un sujet transgressant/résonnant, désaturant sa charge
via le sensible. Le paysage s’auto-gènère, autopoïétique, se charge de
par une propre présence.</p>
<blockquote>
<p>The space creates an effect in its totality; the lights of the
special representation produce a self-contained impression; the space
stands in a unifying light</p>
<p>It does not want to shape objects, but rather to create
phenomena<span
class="sidenote-wrapper"><label for="sn-13" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-13" class="margin-toggle"/><span
class="sidenote">Kümmerlen Robert, <em>Zur Aesthetik bühnenräumlicher
Prinzipien</em>, Ludwigsburg, Schmoll, 1929, cité dans Böhme Gernot, «
The art of the stage set as a paradigm for an aesthetics of atmospheres
», op. cit.<br />
<br />
</span></span></p>
</blockquote>
<figure>
<img src="../../BiblioIconoSono/Images/turell2-06042022124912-0001.jpg">
<figcaption>
Meredith Etherington-Smith dir., <i>A Life in Light : James Turrell</i>,
scan du livre
</figcaption>
</figure>
<hr />
<p>[TRACK 10 - GOTO 80, Synkex Volvex]</p>
<p>L’aura, découle d’un arrière-fond, d’un « éclairage sonore ». Mais si
la lumière semble sortir des ténèbres, il y a toujours une source
annexe, même dans la plus ténébreuse des cavernes frémit une lueur, une
onde à la limite de l’imperceptible, elle glisse le long de l’échine et
lèche les yeux du spéléologue.</p>
<blockquote>
<p>Et pourtant, en contemplant les ténèbres tapies derrière la poutre
supérieure, à l’entour d’un vase à fleurs, sous une étagère, et tout en
sachant que ce ne sont que des ombres insignifiantes, nous éprouvons le
sentiment que l’air, à ces endroits-là, renferme une épaisseur de
silence.<span
class="sidenote-wrapper"><label for="sn-14" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-14" class="margin-toggle"/><span
class="sidenote">Tanizaki Junichiro, <em>Éloge de l’ombre</em> (1933),
trad. Sieffert René, Pof, « D’Étranges Pays », 2001, p.56<br />
<br />
</span></span></p>
</blockquote>
<figure>
<img src="../../BiblioIconoSono/Images/soulages1.jpg">
<figcaption>
Soulages Pierre, <i>Polyptique C</i>, huile sur toile, 1985
</figcaption>
</figure>
<figure>
<img src="../../whsop_g/Comet_67P_on_30_October_2014_NavCam_D.jpg">
<figcaption>
Comet_67P_on_30_October_2014_NavCam_D
</figcaption>
</figure>
<p>Il en va de même pour l’arrière-son permettant à l’atmosphère de se
dégager. Le fond inaudible insaisissable d’où sortira l’acoustique. Dans
une chambre anéchoïque, Cage entendait son sang circuler et son système
nerveux fonctionner, car le monde bruisse en permanence, il n’y a pas de
silence, seul le fond à la frontière de l’inaudible situé quelque part
dans la marge des sens. Il est la condition de possibilité pour le
signal et pour que ce signal, une fois émis développe l’atmosphère.</p>
<figure>
<img src="../../BiblioIconoSono/Images/koichi_sato.jpg">
<figcaption>
Sato Koichi, <i>Student Light Design Competition Poster</i>, Affiche
sérigraphiée, circa 1980/90
</figcaption>
</figure>
<p>Il faut noter que ce n’est pas un son en soi, comme l’écrit R.M.
Schafer en parlant de <em>keynote</em>, sorte de son revenant souvent,
le bourdon (<em>hum</em>) d’une ville, c’est ici plutôt un fond
inaudible, plutôt un son (ou tous les sons) <em>en puissance</em>, avant
qu’ils se manifestent dans l’espace. L’espace est l’arrière son aussi
bien que l’arrière son dessine l’espace comme caisse de résonance
potentielle.</p>
<blockquote>
<p>Just as objects fill visual space, noise is what fills the auditory
field<span
class="sidenote-wrapper"><label for="sn-15" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-15" class="margin-toggle"/><span
class="sidenote">Cox Christoph, « Sound Art and the Sonic Unconscious »,
<em>Organised Sound</em>, Volume 14, p. 20<br />
<br />
</span></span></p>
</blockquote>
<hr />
<p>L’espace rempli joue l’atmosphère en concordance avec le sujet
résonnant, le signal-impulsif émis et puis le reste de lumière et de
son, la fadeur de l’inaudible et du diffus teint doucement l’espace,
remplissant les murs, irradiant les objets d’une impureté certaine.</p>
<blockquote>
<p>Il y a un vieux verbe français qui dit ce tambourinement de
l’obsession. Qui désigne ce groupe de sons asèmes qui toquent la pensée
rationnelle à l’intérieur du crâne et qui éveillent ce faisant une
mémoire non linguistique. <em>Tarabust</em><span
class="sidenote-wrapper"><label for="sn-16" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-16" class="margin-toggle"/><span
class="sidenote">Quignard Pascal, <em>La Haine de la musique</em>, op.
cit., p. 62<br />
<br />
</span></span></p>
</blockquote>
<p>Le fond inaudible nous tarabuste de la même façon que la nuit et
l’obscurité le fait depuis l’enfance. Dès que nous sommes à leur portée,
on ressent viscéralement le vertige du non-dit, du non-vu, non-existant.
Ce que Quignard évoque comme une terreur à travers la panique, le Pan
grec et ses flûtes, c’est ce frémissement, le saisissement inclassable
qu’opère sur nous l’atmosphère.</p>
<hr />
<p>[TRACK 11 - Thomas Ankersmit, Perceptual Geography]</p>
<blockquote>
L’imperceptible […] n’est pas tant ce qu’on ne peut absolument pas
percevoir que ce qui résiste à la perception ce qui se loge dans les
limites de notre perception : « sites imprenables d’un territoire
imperceptible qui déborde toujours »
<figure>
<img src="../../BiblioIconoSono/Images/criton.png">
<figcaption>
Pascale Criton, « Territoires imperceptibles », <i>Chimères</i>, Volume
30, Printemps 1997, p. 65
</figcaption>
</blockquote>
<p>Le territoire de l’imperceptible s’étend sans s’étioler entre les
murs, sa géographie arpente les fenêtres et les sols, trace entre les
pointes de tables et se retrouve entre l’air et l’humus, c’est un
terrain dont émergent les choses, et où jamais elles ne retournent, le
temps avalant inexorablement les évènements qui sortent de ce strict
contour.</p>
<hr />
<blockquote>
<p>C’est pourquoi nous disons qu’il n’y a pas de véritable rite muet,
parce que le silence apparent n’empêche pas cette incantation
sous-entendue qu’est la conscience du désir<span
class="sidenote-wrapper"><label for="sn-17" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-17" class="margin-toggle"/><span
class="sidenote">Marcel Mauss, <em>Esquisse d’une théorie générale de la
magie</em>, cité dans Bonnet François J., <em>Les mots et les sons, un
archipel sonore</em>, op. cit.<br />
<br />
</span></span></p>
</blockquote>
<p>L’atmosphère se loge dans la sous-entente, loin du marteau et de
l’étrier, à des lieues du tympan, elle se cache au fond des boyaux,
stratifiés à la manière des cavernes résonnantes du paléolithique, elle
s’y love quelques instants pour pousser de là le cœur au bord des
lèvres. Venue s’enf(o)uir sous l’écoute et le regard, elle creuse un
sillon inaudible, irrépétable, elle pose un interdit sur sa redite,
frappant de sacré l’instant flottant, loin de l’inaudible et de
l’invisible elle flotte et s’accorde avec nos contrebasses intérieures
pour faire chanter à l’esprit le vertige pan-ique.</p>
<hr />
<p>L’image sans le son est morte, le paysage sans l’atmosphère n’est que
reproduction désincarnée, sans chair, sans saveur.</p>
