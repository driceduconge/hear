# vagues et horizon

Les vagues c'est un peu la montée en tension du tranquille. Le va vient incessant, le clapotis que l'on voit grimper en vague et qui alors sous l'eau, fait roulement de tambour sourd. On voit alors la vague qui monte, monte, gronde et puis s'enroule séchement sur elle même dans un éclatement sonore et visuel (l'écume qui s'éparpille en accents et virgules) et tout retombe ça bruisse vers vous jusqu'à vous mouiller les pieds. Le reste de vague, insignifiant milli- puis micro- puis pico -mètre griffe un peu le sable et couine fshhh, pauvre et s'écrase dans un bruit blanc, laisse un silence bref, et de l'écume parfois. *continuer sur le ciel et la plage*

# Montagne

# les sons qu'on n'entend que comme ça

# Les Cailloux de Caillois + Les coquillages