# Section I - Paysage, oeil & oreille


>#### Noun: landscape (countable and uncountable, plural landscapes)
>
> A portion of land or territory which the eye can comprehend in a single view, including all the objects it contains. 
>
>A sociological aspect of a physical area.
>
>A picture representing a real or imaginary scene by land or sea, the main subject being the general aspect of nature, as fields, hills, forests, water, etc.
>
>The pictorial aspect of a country.
>
>(computing, printing, uncountable) a mode of printing where the horizontal sides are longer than the vertical sides
>
>A space, indoor or outdoor and natural or man-made (as in "designed landscape")
(figuratively) a situation that is presented, a scenario

 

>#### Noun: soundscape (plural soundscapes)
>
 >An acoustic environment, a virtual/emotional environment created using sound. 
>
>A soundscape composition, an electroacoustic musical composition creating a sound portrait of a sound environment.

On verra qu'outre la notion d'environnement et l'étymologie en -scape qui semble les relier, les deux termes paraissent assez éloignés. Pourtant nous les regrouperons ici sous l'appellation de paysage [^14]. Non pas par praticité mais plutôt car les deux termes gravitent autour de la notion (encore anglaise) de *picturing*, qui signifie -traduction grossière - à la fois imaginer, décrire et (se) représenter.*développer ici sinon ça vaut rien*

[^14]: En français classique, on sépare les deux termes en "paysage" et "paysage sonore", le son se transforme alors comme une béquille au terme original, et nous ne voudrons pas de ceci ici.

Où l'on perçoit une section d'un espace réel ou imaginaire, où on se le représente à partir des percepts offerts. A travers principalement la vue et l'ouïe.


> Percepetion as a representing act is ddistinguished from immersion in the sensible ; it involves an anterior sorting out, presumes a directive surge of consciousness and an intended object.[^3]

---
 
![](../../BiblioIconoSono/Images/wikimedia_spectrevisible.png)

[spectre audible](https://www.youtube.com/watch?v=wbPclkhoXdE)

---



--- 

>Le paysage n'existe qu'en relation avec un sujet esthétique, il est l'objet d'une *perception* et d'une *expérimentation*, lesquelles sont médiatisées par toute une série de filtres culturels variés[^1]

[^1]: Justine Balibar, *Qu'est-ce qu'un paysage ?*, p. 8, 2021, ed. jsplus

L'avènement du paysage étant le produit d'une perception, il est aussi et surtout le produit de *régimes d'attention* de l'ouïe et de la vue, qui deviennent alors, regard et écoute. Pour exemple on voit l'arbre mais on regarde ses branches, son feuillage, ses noeuds... Ou alors on entend la voiture s'approchant mais on écoute la cadence du moteur, la vitesse de rotation des roues, la position de la source par rapport à nous... 

On fait donc ici la différence dans chaque sens entre son état passif et actif, Nancy parle de “nature simple” et d’état “tendu, attentif ou anxieux”.[^2] Cet état d'attention crée immédiatement la différence entre un espace et un paysage, une fois le processus cognitif-perceptif lancé le sujet mesure esthétiquement tous les virons (ciel-terre, terre-mer, soleil-yeux, vent-clapotis, cigales-grillons...). Ainsi le sujet non seulement est dans l'espace mais fait exister (en son intérieur) les signaux de ce dernier.
Ainsi on ne voit pas et n'on entend pas le paysage, mais plutôt on *le génère par l'écoute et le regard*.

**ce paragraphe vient en à côté**

On notera qu'il n'y a pas une manière de regarder et d'écouter mais énormément de variations possibles. Schaeffer et LMurray-Schaffer, chacun à leur manière ont tenté de nommer/normer les modalités d'écoute. Schaeffer parlait par exemple d'une écoute quotidienne, à mettre en opposition à une écoute réduite : la première étant purement fonctionnelle, à visée informative, tandis que la seconde visait plutôt une appréciation du son "tel quel", sans tenir compte des informations qui en viennent. M.-S. lui aussi, entrainaît ses étudiant·es à écouter les sons pour leurs qualités acoustiques [*insérer une planche d'exercices de M.-S. par ex.*].
Cette approche semble limiter la manière dont on écoute le son, car sortir un objet de son contexte afin de l'écouter soi-disant purement ne fait qu'appauvrir l'écoute du timbre. 

Cette question du son-objet rejoint les vieilles questions qui ont déjà marqués le monde de l'art sculptural et pictural. Joanna Demers l'aborde dans son article[^15] en mettant en perspective les travaux de Francisco Lopez et Toshiya Tsunoda avec les sculptures de Donald Judd et Morris entre autres.

>Reduced listening also focuses so much attention on minute details of sound that it can foster perceptual distortions. More fundamentally, reduced listening *perpetuates the falllacy that there is one universal listening experience untouched by culture*.[^15]

[*plus loin une double page avec en face à face donald Judd et un extrait de Wind[Patagonia]*]
[^15]:*Field Recording, Sound Art and Objecthood*, Joanna Demers

**fin du paragraphe  en à côté**


---

>[...] no matter how proximate, objects are always given across an intervening space. Seeing acknowledges distance even while compensating for it by bringin what is far off into visibilty. **(p. 188)**[^3]

[^3]: Edith Wyschogrod, _Doing before hearing : on the primacy of touch_, dans _Textes pour Emmanuel Levinas_, Paris, Jean-Michel Place, 1980

Le paysage visuel relèverai alors de l'acte de regard, de mesure esthétique des choses. Pas au sens du beau, mais plutôt dans le sens d'une *appréciation sensible* de l'espace, de la distribution du vide, des distances. Les différentes composantes visuelles permetteront, par leur frontalité et leurs limitations, au sujet de sentir si telle "vue" fait paysage. C'est " L'expérience d'un morceau d'espace perçu d'un coup "[^18].

[^18]: *Le paysage*, Michael Jakob, 2008, Infolio éditions, p.38


---

Un paysage "sonore" relève de l'arpège, de la gradation des sons allant de l'étouffé au clair, du murmure à l'assourdissement, puis peut être de nappes plus calmes. Tout cela advenant séparément, se recomposant  dans l'oreille et le coeur, nous laissant le loisir d'une autre mesure paysagère. On perçoit alors une autre limite à l'infini, un autre horizon, plutôt une nappe de fond, sonore.

---



Une fois créée,  le paysage particulier reste en nous, inscrit. Accroché à nos oreilles et nos yeux, rémanence du souvenir. Taches noires et acouphènes. Marqué·es au fer rouge on le reverra dans les cartes postales, le ré-entendra dans le chant d'une moissoneuse.

---

> Un jour viendra où nosu saurons percevoir à la mesure de nos souvenirs, où nous nous *ressouviendrons pour mieux ressentir*[^8]

> D'une part il [le paysage] se structure, il s'organise, il se refuyse à moi dans son originelle altérité, il se singularise par des signes qui nécessitent la plus grande attention. D'autre part, il *s'atmosphérise, il devient une vibration, une odeur, une émotion unique, il se mêle à moi comme s'il n'était pas distinct de moi*.[^8]

[^8]: Pierre Sansot, *Variations Paysagères*, p. 30, Paris, ed. Klincksieck, 1983

---

Le paysage prenant pour origine une perception sensible et attentive fusionne avec le sujet, iel se retrouve non pas pénétré, mais résonnant avec le paysage. Le coeur qui suit le rythme de la rivière et la peau qui s'harmonise avec le soleil du Midi.

---

Lors des balades d'été dans les vignes vironnantes de chez mes parents, le soleil frappait ma peau et pour souffler un peu, j'allais m'asseoir à l'ombre de l'ancien four à chaux. Une structure en pierre en ruine, laissant apparaître 8 murs, pas de plafonds, les fours ainsi que l'arbre qui venait m'abriter. Les cigales et la brise tendait tout l'espace ensemble, le bourdon le remplissant et le vent faisait revivre quelques instant la terre, bruissait dans l'arbre et quelques jours soufflait le calcaire des pierres. La pendularité de cette balade de quelques kilomètres, réalisée le plus souvent seul, à pied à vélo tissait les liens entre l'ancienne