> Reverb at once *represents* space and *constructs* it. [^1]

 [^1]: *Space within Space: Artificial Reverb and the Detachable Echo* - Jonathan Sterne

---

Le paysage re-sonne en nous, éternellement, à la manière du choeur dans les cathédrales.

---

Chaque espace a sa résonnance, son identité, son seuil 

[room tone forest et room tone room]

Le paysage ouvert ne laisse que peu survenir sa résonnance, mais dans de rares instants, si l'on se place à un certain point et que l'on articule, une réponse se mêle et semble s'étirer jusqu'au fond du visible.

[MYTHE D'ECHO]

>L'écho est la voix de l'invisible. [...] Dans l'écho l'émetteur ne se rencontre pas. C'est le cache-cache entre le visible et l'audible.[^2]

[^2]: La haine de la musique p.149

---

*ici de la réverb-résonnance vers la "présence" en somme toute du wet vers le dry*

La réverb comme chose floue / le dry comme le net. Le dry comme "son en soi", le wet comme son dans le paysage.