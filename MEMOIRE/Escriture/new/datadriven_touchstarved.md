﻿# Section 2.5 Data-driven and touch-starved landscape


<figure><img src="../../whsop_g/3K.png"><figcaption>Caillois Roger, <i>Lecture de Pierres</i>, EXB,  scan</figcaption></figure>

<figure><img src="../../BiblioIconoSono/Images/Agate-005b-6mm_wikimediacommons.jpg"><figcaption>Koltovoi, Agate, Wikimedia Commons</figcaption></figure>

Les pierres ont longtemps fascinés les élites chinoises, car parfois on pouvait retrouver dans ces dioramas de pierres, taillées par l’érosion, des creux et des pics qui évoquaient tantôt une île tantôt une montagne. Il est dit que certains sages pouvaient se miniaturiser tant qu’ils pouvaient alors expérimenter ce paysage à la même manière que le réel. Ces pierres devenaient alors leur palais mental, voire leur tombeau. Dans la surface dure, iels percevaient une élasticité de la forme, peut-être un reste de l’eau dans laquelle des pierres ont baigné jadis.

<figure><img src="../../BiblioIconoSono/Images/Lingbi_wikimedia_commons_1.jpg"><img src="../../BiblioIconoSono/Images/Lingbi_wikimedia_commons_2.jpg">
<figcaption><figcaption>Deux Gongshis ou Lingbis, ou « Pierres de contemplation », Wikimedia Commons</figure>

> Les remous du liquide ajoutent en filigrane ce lac sonore et indistinct, rapetissé jusqu’à tenir à l’intérieur d’une pierre, comme le mystère d’un paysage spectral, brumeux, pourtant plus réel et plus lourd que les paysages évasifs que l’imagination, au premier appel, se hâte de projeter dans les dessins des agates.[^10]

[^10]: Caillois Roger, *Lecture de Pierres*, Éditions Xavier Barral, 2014

---

[Track 12 - SeaShell Sound]

La paréidolie que l’on infère aux pierres se retrouve aussi dans le coquillage (souvent la conque) ramassé en bord de plage qu’une fois ramené chez soi, l’on colle à l’oreille pour réentendre comme un bout de mer enfermé. En fermant les yeux, on ressent alors peu à peu les vagues aux loin remuant les fonds, nous les entendons s’ébruisser sur le sable en écume. L’on prête attention à la plus infime variation dans le bruit blanc de la nacre pour écouter la marée. 

[1mn15]

L’oreille se tend toute entière vers ce paysage enfermé, accessible par la mise en tension de nos sens, par la résonance entre la nacre et notre souffle.

[2mn]

> Poser l’oreille contre un coquillage, c’est se donner à entendre tout un océan. Le bruit coloré afflue et reflue en vagues interminables enserrées dans l’espace nacré et étroit. Pourtant, c'est un en-dehors qu'on perçoit[^5]

[^5]: Bonnet François J. , *Les mots et les sons, un archipel sonore*, L'Éclat, 2012, p. 13

[4mn]

Le coquillage, reproduit dans ses aspérités ondulées régulière l'onde du champ où il se trouve, il contient en lui le murmure des moutons et s'enroule dans une spirale logarithmique

[fin coquillage]

---

>Malheureusement, l’espace est resté voyou et il est difficile d’énumérer ce qu’il 
engendre. Il est discontinu comme on est escroc[^200]

[^200]: Bataille Georges, « Espace », *Documents*, vol. 2, 1930, p. 41 cité dans Le Lay Yves-François, « Emotionscapes. S’é-mou-voir des situations géographiques », thèse d’Habilitation à diriger les recherches, Géographie ENS Lyon, 2019, p. 21

Dans le bruit blanc se trouve l’ouverture à l’espace, la conque marine portée à l’oreille semble contenir un pays entier alors que l’espace trompeur ne renferme que des stries de nacres. La coquille s’apparente à l’enveloppe des montagnes sur lesquelles le regard ricoche. Il suffit de dire leurs noms pour que ces amas de cailloux deviennent des situations géographiques dont on s’émeut. La conque marine dit-elle son nom en présentant la mer en son intérieur ? On ne saura qu’en l’écoutant longuement et peut-être qu’en murmurant à notre tour les moutons d’écume nous créerons un espace à l’intérieur de nous, un imaginaire hermétique.

---

Ces paysages contenus dans des objets sont des paysages non pas privés d’expériences sensorielles, mais privés de toucher, d’arpentage physique. Ce sont des espaces trop petits, trop frontaux pour que l’on s’y déplace, que l’on en aie une expérience réelle, il en revient à se dire que la force de ces paysages c’est qu’ils passent par un médium. 

>Touch, like hearing, encounters its objects in sucessive apprehensions while, like sight, it synthesizes its data as a static presence of the given[^54]

[^54]: Wyschogrod Edith, « Doing before hearing : on the primacy of touch », dans _Textes pour Emmanuel Levinas_, Paris, Jean-Michel Place, 1980, p. 197

À la manière du paysage représenté en peinture ou en œuvre sonore, ils dépendent d’un medium et de notre capacité à porter une attention concentrée envers ce medium. On rejette la force atmosphérique phénoménologique du paysage réel pour plonger dans une forme d’hyperrésonnance avec un paysage réduit à peau de chagrin. On opère une concentration dans la métonymie. L’objet devient un tout temporaire, extrait de son espace, on le déploie en nous, dans l’imaginaire. 

[TRACK 13 Rone - Bora Vocal ]

---

> La nature reflétée par le miroir convexe noir est toujours fictive et incomplète, c’est-à-dire en défaut par rapport à la nature elle-même, parce que reflétée (aspect mécanique) et réduite (aspect fragmentaire) par ce miroir. Cependant cette double imperfection du reflet de la nature tendrait alors à être […] idéalisée par le miroir lui-même […] le miroir tend à abstraire.[^63]

[^63]: Maillet Arnaud, *Le miroir noir, enquête sur le côté obscur du reflet*, L’Éclat, « Kargo », 2005, p. 128

Ce qui fascinait les amateur·ices de pittoresque, ce n’était pas tant le paysage vu par l’œil, ni l’expérience de parcours de celui-ci. Non, même si toutes ces sensations faisaient partie de leur rituel, la véritable mystique se trouvait dans le reflet embrumé de leur miroir. La capture de l’espace dans un endroit plus petit, l’enserrement des perspectives dans une rétine de poche, c’est ici que se déploie toute la force du miroir noir. 

L’imaginaire —la fiction déployée à première vue dans le reflet du miroir noir  —se revêt d’un voile flou, le réel vient doucement devenir un espace fictionnel. 

L’espace qu’on y perçoit est analogue à ces terres inconnues, notées sur les cartes comme l’endroit où sommeillaient des dragons, à leur manière il recèle la part d’incertitude sur ce qui passe dans le reflet du miroir.


---

Ces trois objets, coquillage, agate et miroir noir participent à faire ressentir une *sensation de paysage* contre leur gré. Nous leur apposons nous-mêmes ce statut de « faux » paysages, d’illusion. Pourtant, deux différences subsistent entre l’objet technique et les deux autres. D’abord, le miroir a été *pensé* pour refléter l’espace et créer le paysage, il « copie » d’une certaine façon le réel pour le retravailler légèrement, et tout cela sans grosses opérations de traduction, répercutant seulement la lumière de façon dissonante. Ensuite, l’agate et le coquillage se sont simplement sédimentés de cette façon, et même si les deux objets produisent continuellement ces formes il faut que nous leur prêtons attention pour découvrir, à travers le regard ou l’écoute, le paysage qu’ils renferment.

>we will need to reconsider the signification of imagination: to think of it not just as a capacity to construct images, or as the power of mental representation, but more fundamentally as a way of living creatively in a world that is itself crescent […] participat[ing] from within, through perception and action, in the very becoming of things.[^6]

[^6]: Ingold Tim, « Landscapes of perception and imagination », dans *Imagining for Real, Essays on Creation, Attention and Correspondence*, Routledge, 2022, p. 32

L’ordinateur (le calculateur), lui se pose comme une machine à fabriquer du sensoriel. Si le cinéma, comme le miroir noir, travaille à partir du réel pour soit le montrer, le sublimer ou le détourner, le processeur, construit à partir d’une infime part du réel (des signaux électriques conditionnés par des portes logiques) la possibilité de l’infini dans un objet. En ce sens on peut l’apparenter à la pierre ou au coquillage, il peut délivrer de manière obtuse des sensations de paysage, mais la frontalité de l’expérience de l’écran et des haut-parleurs pousse à se poser la question de ce que l’on perçoit. L’on ne voit pas le courant et les calculs lorsque nous parcourons un espace généré par ordinateur, on ne voit que le résultat d’opérations physiques sur des objets techniques variés. L’ordinateur masque ce qu’il est et tente de formuler le réel, de sédimenter couche à couche ce qui apparaitrait comme un paysage.

`let analyse du système de lecture = contrôle` 
`if (analyse du système de lecture == contrôle){`
`				alert(vous formez l'image d'une main);`
`			alert(vous mettez le violet dans les soleils);`
`				alert(vous activez le point de vue d'un peronnage, vous projetez déjà des intentions, vous supposez déjà des souvenirs)}`[^3]

[^3]: Boyer Elsa, *Orbital*, Éditions MF, « Inventions », 2021, 1ère de couverture

> The making of atmospheres is therefore confined to setting the conditions in which the atmosphere appears.[^120]

[^120]: Böhme Gernot, « The art of the stage set as a paradigm for an aesthetics of atmospheres », *Ambiances*, 2013, connection on 22 September 2020 [http://journals.openedition.org/ambiances/315](http://journals.openedition.org/ambiances/315)

---

![](../../BiblioIconoSono/Images/tracker.jpg)

>Si la partition n’est plus un texte, mais un *prétexte*, si ce qui a été composé évolue d’une exécution à l’autre, et même dans le cours d’une même exécution […], alors le réseau de possibles qu’est l’œuvre se laisse saisir.[^12]

[^12]: Charles Daniel, *Gloses sur John Cage*, UGE, « 1018 », 1978, p.20

Le pré-texte prépare la réalité d’une action, la programmation informatique permet de faire jouer une logique sémantique intégrée elle-même dans l’environnement numérique via une syntaxe. On somme l’ordinateur de produire un évènement en traduisant une description (lacunaire, asensorielle) vers un bourdonnement électrique signifiant.

— >L’interprète est un preneur de sons, au sens où l’on parle d’un preneur d’images[^13]

[^13]: Ibid., p. 127 


Le calculateur prend le bourdonnement électrique des cycles de fonctionnement des cœurs et le module dans les haut-parleurs, il s’égosille de recracher la synthétisation d’un tapuscrit en son, il tente de faire le pont entre le sens et le sensible, trahit par le grésillement du câble liant la machine à son organe. Trahi par son battement de cœur. John Cage dans la chambre anéchoïque.

La description d’un paysage par ses noms est (pour l’instant) impossible.

> Mon nom, le tien, celui d'un enfant qui n'a pas encore  
> vu le jour, tous forment les syllabes du grand mot 
> que prononce très lentement l'éclat des étoiles.[^14]

[^14]: Le Guin Ursula K., *Terremer, Les Tombeaux d’Atuan* (1970), OPTA, « Aventures fantastiques », 1977 

---

>Le phénomène numérique ne fait que rendre visible, par son ampleur, un trait philosophique caractéristique de toute technique en général, resté relativement inaperçu mais essentiel : *la technique est une structure de la perception*, elle conditionne la manière dont le réel ou l'être nous apparaît.[^11]

[^11]: Vial Stéphane, *L’être et l’écran, comment le numérique change la perception*, PUF, 2013,  p. 99 

Certaines algorithmes de DL, on citera notamment DALL-E ou MidJourney, permettent de produire des simulacres de reproductions paysagères à partir de descriptions sommaires. Mais elles laissent inassouvie (pour le moment) la (les) sensation résonante intrinsèque à l’évènement paysage.

Elles produisent des images mortes. Reflets de Narcisse.

<figure><img src="../../BiblioIconoSono/Images/midjourney_3.webp"><figcaption>Une image de paysage créée à l’aide de l’algorithme MidJourney</figcaption></figure>

>On veut toujours que l’imaginaire soit la faculté de « former » des images. Or elle est plutôt la faculté de « déformer » les images fournies par la perception, elle est surtout la faculté de nous libérer des images premières, de « changer » les images. S’il n’y a pas de changement d’images, union inattendue des images, il n’y a pas imagination, il n’y a pas d’« action imaginante ».
*L’Air et les Songes, 1943, Gaston Bachelard.*

---

<figure><img src="../../BiblioIconoSono/Images/uxn_linux.jpg"><figcaption>
Image d’UXN, une VM (virtual machine), Hundred Rabbits, <a href="https://100r.co/site/uxn.html">https://100r.co/site/uxn.html</a>, consulté le 24 octobre 2022</figcaption></figure>

Le bytecode et le langage de compilation des ordinateurs sont bas niveau, au sens qu’ils perdent toute accessibilité sensible quand on les voit. Les enchaînements de lettres et de chiffres a priori abscons se mettent en tension avec la perfection de la simulation. Ces suites binaires, hexadécimales semblent distendre l’espace sur une ligne, l’écrasent en une dimension. Le paysage se replie sur lui-même. Nous le déploierons par *abstraction*, en feignant d’oublier, à cet instant, le simulacre.

>L’arbre n’est pas le nom arbre, pas davantage une sensation d’arbre: c’est la sensation d’une perception d’arbre qui se dissipe au moment même de la perception de la sensation d’arbre[^38]

[^38]: Paz Octavio, *Le Singe grammairien*, 1970, cité dans Charles Daniel, *Gloses sur John Cage*, op. cit.

L’invocation des mers et des vents nécessite de savoir les appeler.

>le monde spirituel n’est autre que le monde des sens, et le monde des sens n’est autre que le monde de l’esprit. Le monde est un et parfaitement total[^39]

[^39]: Suzuki, *L’Essence du Bouddhisme*, 1955, cité dans Charles Daniel, *Gloses sur John Cage*, op. cit.

>Inutile de parier sur les sons […] on ne s’assure pas de l’existence de la mer, ni de l’existence du vent.[^57]

[^57]: Charles Daniel, *Gloses sur John Cage*, op. cit., p. 57

---

le paysage comme souvenir d’une émotion fugitive, qui nous remonte aux bords des lèvres, en fermant la bouche on tente de fixer cette sensation de vertige…
une envie démiurgique de recréer ce qui a été perdu

Par l’action d’un ou plusieurs personnages dans un espace fictif numérique on éprouve d’une nouvelle manière le toucher paysager, l’espace restructuré par une mise en image en son et en musique. L’ordinateur programmé fait la corrélation entre des éléments sémantiquement séparés en lui pour donner l’impression de réel. J’appuie sur une touche (impulsion depuis le périphérique), une entité (mon personnage) se déplace, j’entends ses pas (fichier son) dans un espace lui-même sonorisé. 

---

Le numérique chiffre l’image, le son (traduite) et les déploie sur l’onde et la matrice. Nous on ré-absorbe, on re-calcule, dé-chiffre l’information. On dé-duit alors à partir du calculé un ressenti certain, on cherche à retrouver une sensorialité *par* le regard et l’écoute *par* le déchiffrage. Le pixel chemine jusqu’à l’œil, mais le regard qu’on portera sur ce gris optique.
La reconstruction inconsciente des points lumineux en taches colorées, des taches en modelés et le tout en formes et fuites, on re-constitue l’image. 

[TRACK 13 - Sarah Sze Twice Twilight]

> L’architecture de ténèbres demeure imperturbable. Certes, il est commun d’être couleur d’encre. Mais cette nuit, d’une espèce nouvelle, est partout exacte et construite, formée de flancs parallèles, de biseaux homologues, de justes médiatrices, d’angles inévitables. Une géométrie stricte proclame qu’elle n’est pas un néant à combler, encore moins un oubli à réparer, mais un ordre qui a ses lois et qui publie sa valeur d’ordre.[^4]

[^4]: Caillois Roger, *Pierres*, Gallimard, 1971

---

>Entendre est une manière de toucher à distance[^2]

[^2]: Murray-Schaffer R., *Le Paysage sonore, le monde comme musique* (1977), Wildproject, « Domaine sauvage », trad. Sylvette Gleize, 2010

Toucher des yeux, c’est se placer en présence du simulacre.

<figure ><img src="../../BiblioIconoSono/Images/Marinella_Pirelli-Sole_in_Mano_2.jpg"><figcaption>Marinella Pirelli, <i>Sole In Mano</i>, 1970, film, 6mn, captures d’écran</figcaption></figure>

![](file:///D:/gitlab/hear/MEMOIRE/BiblioIconoSono/Images/Marinella_Pirelli-Sole_in_Mano_3.jpg)

Pour certains, le paysage serait équivalent au suc du réel dans lequel on plante goulument les dents, dévorer (des yeux) la montagne, disséquer (des oreilles) la structure de l’espace ; on éprouve une *physicalité*. Mais les organes face au flux numérique semblent ne plus savoir où porter les canines, on se perdrait dans un flux de données, on serait submergés, victime de dépréciation sensorielle. Mais alors on oublie que la mâche c’est aussi le cœur et la tête et que la projection dans l’espace, bien que désarticulante, se voit restructurer temporairement nos manières de sentir. Je me retrouve alors à sentir la brise venant de mon écran et à mesurer le monde qui s’offre à moi.

![](file:///D:/gitlab/hear/MEMOIRE/BiblioIconoSono/Images/Marinella_Pirelli-Sole_in_Mano_1.jpg)

— Lorsque le paysage se trouve perturbé par le médium, que l’on voit que le sol brusquement devient transparent, que le bug révèle la matrice du monde factice, on retrouve la même distance avec lui. Le chiffre perd de sa puissance simulatrice, on se retrouve aux confins du calculé, le paysage se transforme et devient de l’imprévu. Lors de ces instants, les sons et l’image ne se synchronisent plus et faillent à leur qualité illusoire. Poussé dans ses retranchements, le sortilège s’efface et le métamorphe redevient un amas de formes et de sons. 

<figure><img src="../..//BiblioIconoSono/Images/farocki1-1.webp"><figcaption>Harun Farocki, <i>Parallel I-IV</i>, capture d’écran, documentaire, 43mn, 2014</figcaption></figure>

<figure><img src="../../BiblioIconoSono/Images/oobbreathofthewild.png"><figcaption>Gaming Reinvented, <i>Going Out of Bounds in the Trial of the Sword (Legend of Zelda Breath of the Wild Glitch)</i>, 2018, capture d’écran, vidéo YouTube, 1mn50, https://www.youtube.com/watch?v=HLqY_WDUlkI consulté le 20 novembre 2022</figcaption></figure>


C’est ici que l’imaginaire revient, que le sensible refait surface, car nous retrouverons ces paysages au fur et à mesure, nous fixerons de nouveaux horizons numériques et goûterons à un autre toucher, celui de la vallée entre la perfection et le bégaiement informatique, le sigil de travers matérialisant un autre souffle.


