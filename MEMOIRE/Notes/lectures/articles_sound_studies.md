#Liste d'articles

- [What in the world do we hear? An ecological approach to auditory event perception - William M. Gaver](#whatdowehear)
- [Field Recording, Sound Art and Objecthood- Joanna Demers](#soundart)
- [Space within Space: Artificial Reverb and the Detachable Echo - Jonathan Sterne](#spacewithin)
- [Sound Art and the Sonic Unconscious - Christoph Cox](#unconscious)
- [Yannick Dauby - Interview Librarioli Escola](#dauby)

## *What in the world do we hear? An ecological approach to auditory event perception* - William M. Gaver {#whatdowehear}

![](../images_tirées_darticle_maybe/schema_sound_gaver_whatdowehear.PNG)

> the entire pattern of the car's vibrations is meaningfully structured by its components.
> 
> Unlike radiant light, which is relatively unstructured and thus uninformative  [...] radiant sound is richly structured by - and thus may provide information about - its source

>Sound converges on a potential listening point from every direction [...]. The result is and *auditory array*, analogous to the optical array. [...] Reverberant sound is informative about the size and layout of the surrounding environment.

> sound provides information about _an interaction of materials at a location in an environment_.

On décrit le plus souvent un son par *sa source*, et moins souvent par les qualités acoustiques de ce dernier. C'est ici tout la difficulté du *timbre*, chose qu'on ne retrouve pas dans une description ocularocentrée (la montagne peut être analogue à un jeu de lignes, à une intensité blanche, etc.). *Le son, dans une écoute __quotidienne__ informe*, permet de se *situer* dans un environnement, le lexique sonore est lié à l'origine du son, à une source et une position relative à un·e auditeur·ice audiopositionné·e alors que le visuel suggère un placement *automatique*. 

L'endroit où visuel et son sont équivalent c'est lors du *feedback* par rapport à une action, qu'elle soit dans un écosystème réel ou informatique :

>For instance, selecting a file icon might make the sound of tapping an object

Il y a aussi dans l'article une différence faite entre l'*écoute musicale* et l'*écoute quotidienne*. On retrouve ici deux échelles d'attention dans une même écoute *active* des sons.

## *Field Recording, Sound Art and Objecthood* - Joanna Demers{#soundart}

Francisco Lopez & Toshiya Tsunoda

Le travail de field recording de ces deux artistes (souvent avec très peu ou pas du tout d'interventions sur les enregistrements) est lié à l'*écoute réduite* théorisée par P. Schaeffer du GRM, où l'intention est d'écouter seulement l'objet sonore. On s'éloigne donc de l'*écoute quotidienne* dont parle Gaver dans l'article plus haut.

>There can only be a documentary or communicative reason to keep the cause-object relationship in the work with soundscapes, never an artistical/musical one [...] [it should have] the *full right to be self-referential*  (Lopez, 1997)

Francisco Lopez pense donc que tout travail de paysage sonore [*soundscaping*] se doit de s'émanciper du couple cause-objet sonore. Il faut supprimer, par l'écoute, l'information transmise par le timbre, la localisation etc. Mais en quelque sorte cela ne reviendrait-il pas à supprimer la compréhension spatiale inhérente au paysage ? Pour lui, *pas de ré-intégration du sujet avec l'environnement*.

C'est là qu'on se rend compte que l'approche de Rosario Assunto de la métaphysique du paysage est lacunaire pour le mémoire, dans le sens où si l'on définit strictement le paysage comme *issu du réel / réel*, alors dès qu'il s'agit de vouloir le sortir de son contexte et considérer sa représentation ou retransmission on peut *perdre* cette *situation dans un espace limité et ouvert*. 

Mais lorsque l'on perd cette spatialité, qu'advient-il du paysage ? Est-ce que le concept s'effondre avec l'espace pour laisser la place alors à un bâtard atmosphère/paysage ? L'espace acoustique et sonore se resserre, *non pas autour* du sujet mais *face à* lui. Loin de l'audioposition on se retrouve alors dans la situation du théâtre, spectateur·ice d'une *remise en marche d'une impression d'espace*.

Joanna Demers remet ça en perspective en comparant l'objet-sonore avec le parti pris minimaliste de Morris, D. Judd & co.

>Minimalism was the first attack on illusion because it brought elements of the *outside world back into the art work*, yet it still operated with the faith that the resulting art work could *still maintain its autonomy and abstraction*.

L'écoute réduite dont l'on parle plus tôt de sons "pour eux mêmes" de Schaeffer ne serait que mythe (de la même que l'on ne peut réellement regarder une image pour elle-même), *les qualités acoustiques sont interdépendantes de facteurs physiques, sociaux, techniques et culturels.*

>Reduced listening also focuses so much attention on minute details of sound that it can foster perceptual distortions. More fundamentally, reduced listening *perpetuates the falllacy that there is one universal listening experience untouched by culture*.

## *Space within Space: Artificial Reverb and the Detachable Echo* - Jonathan Sterne{#spacewithin}

La réverbération est partie intégrante, sinon obligatoire et logique, de la construction du son et de son écoute dans l'espace.

> Artificial reverb at one *represents* space and *constructs* it. 

>[citation of  J.Stanyek & B.Piekut] "where sounds and bodies are constantly *dislocated, relocated, and co-located in temporary aural configurantions*"

>All sound needs a medium. Which is to say that acoustic space can never be empty or neutral. [...] sonic space *cannot be Euclidean space* [...] Sound *cannot move through empty space*  [...]
>
>The very idea of a detached echo, a truly pure sound without echo, requires us to imagine an *impossible sound* that exists outside relations, outside space, a sound that brackets its own historicity and situatedness __(p.114)__

Les technologies de captation et de retransmission du son ont abouti à une imbrication [nesting] d'espaces (sonores) les uns dans les autres.

> These multiple spaces may occur within recordings themselves; for instance, in a popular music mix where the drums are recorded in one space and the vocals another __(p.115)__

Ce n'est pas une imbrication de poupées russes, précise Sterne, mais plutôt des couches d'espaces qui se superposent et co-habitent dans l'espace du morceau. Il en va de même pour leur retransmission : les hauts-parleurs envoient le son dans un espace (et il sont eux-mêmes un *espace fréquentiel*, avec leur plage limitée par des filtres passe-haut et passe-bas).

Plus loin Sterne aborde le *detachable echo*

>Detachable echoes proliferate possible realities, experiences and perspectives on them.

Le son "vient" d'un autre espace, déterminé ou non par le type de reverb que l'on perçoit. Ce son sera qualifié de *wet*, modifié par la réverb, il s'en verra caractérisé par rapport à un espace et *audiopositionnera* le·a spectateur·ice dans ce-dit espace. Au contraire un son *dry* se verra donc comme un son dépourvu d'espace, sans chambre de résonance et donc, d'écho perceptible. Sterne met en exemple le téléphone où l'on parle directement de la bouche contre l'oreille, *en écrasant l'espace par la télécom*.

>The overlay of physical and mediatic space in digital media *has already happened* in the sonic domain. [...] By their nature, the sounds are interactive, as our hearing of them echanges each time we move around the room.

Selon Sterne la réalité augmentée, dans le sens de multiples *overlays* sensoriels fabriqués au sein d'un espace réel, existe depuis longtemps (depuis l'avènement des hauts-parleurs). Il remet aussi en jeu le terme d'interaction (on se déplace pour s'éloigner d'un son, on le coupe, le rewind etc.)

Il y a par ailleurs deux types de reverbération :

1. Réverb algorithmique
2. Réverb par IR (Impulse Response)

La __première__ consiste en un algorithme complexe qui génère des valeurs aléatoires qui calculent en fonction de différents paramètres *arbitrairement choisis* pour générer un faux espace sonore.

>their ultimate goal is to produce a particular kind of perceptual effect, not to map a space. Reverberation [...] gives the *feel* of a space. __(p. 122)__

>Manfred Schroeder un acousticien/mathématicien à propos du premier réverbérateur électronique "we just picked numbers and subjectively listened to the results until we were happy.".

La __seconde__ repose sur un principe *analogous to photography*. On mélange le spectre d'un son dry avec le spectre d'une Impulse Response (IR).

> "impulse response" is created by introducing a short burst of sound in a room and recording the echo. [...] even though it is completely contrived, the relationship between the impulse response and the physical space it seeks to model is *causal* __(p.124)__

## *Sound Art and the Sonic Unconscious* - Christoph Cox{#unconscious}

> *Abstract* : This essay develops an ontology of sound and argues that sound art plays a crucial role in revealing this ontology. I argue for a conception of sound as a continuous, anonymous flux to which human expressions contribute but which precedes and exceeds these expressions [...], I propose that this sonic flux is composed of two dimensions : a *virtual* dimension that I term *noise* and an *actual* dimension that consists of *contractions of this virtual continuum*.

Le *noise* [le bruit de fond] est le flux, fond de toute chose, en quelque sorte l'énergie même de la possibilité de vie : 

>The background noise never ceases; it is *limitless, continuous, unending, unchanging*. It has itself no background, no contradictory. [...]
>
>*As soon as a phenomenon appears, it leaves the noise*; as soon as a form looms up or pokes through, it reveals itself by veiling [dissimuler, voiler] noise. So noise *is not* a matter of phenomenology, so *it is a matter of being itself*
>
 **[M. Serres, Genèse, 1982]**


>Just as objects fill visual space, noise iw what *fills the auditory field*

La précision ici revient sur la distinction son de fond et son signal, et par cette analogie : tout objet peut devenir signal, pour peu qu'on le regarde (porte de l'attention), il en irait-il de même pour le bruit de fond ? Dès qu'on en écouterait un pan choisi, on lui inférerait un sens et donc cesserait-il d'être noise ? Le noise est la *condition de possibilité pour le signal*. 

L'écoute agit comme révélatrice et productrice de signal. Autrement le bruit de fond ne serait qu'entendu "inconsciemment", donc non perçu.

>Leibniz thus makes it possible for us to grasp the distinction between signal and noise *not* as one between *part and whole, ignorance and knowledge*, but as *one between the singular and the ordinary, perception and its condition of genesis*, the **actual** [signal] and the **virtual** [noise]. 

Le bruit de fond serait aussi partie du "silence", tant que non écouté.

>This is the idea I want to pursue here: noise *as the ground, the condition of possibility* for every significant sound, as that from which all speech, music and signal emerges and to which it returns.
>[...]
>
>Cage : "Music is continuous, only listening is intermittent"

## Yannick Dauby - interview dans Librarioli Escola{#dauby}

>Il est possible de percevoir, dans une langue l'écho d'une autre. Selon l'idiome et la sensibilité de l'oreille qu'on lui tend, néanmoins, la nature de la résonance varie dans des proportions considérables. *David Heller-Roazen: Echolalies: essai sur l'oubli des langues*

notes articles à lire Jean C. Roché ainsi que Chris Watson

>Ecouter un animal, c'est se jeter un peu dans sa temporalité et mode d'interaction avec l'environnement. (p.57)
