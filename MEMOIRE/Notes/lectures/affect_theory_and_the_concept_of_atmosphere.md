# Steven D. Brown, Ava Kanyeredzi, Laura McGrath, Paula Reavey & Ian Tucker, Affect theory and the concept of atmosphere, dans *Distinktion, Journal of Social Theory, 20:1, p.5-24*

>Our emotions are a response to the way we culturally perceive the world around us. They are an echo or reflection of our powers of thought that comes back to us as cultivated feeling. **p.6**

>We have been saying this many times by now: atmospheres are feelings poured out into space. They are modes of a corporeal predualistic communication that at times is supersubjective and superobjective – the calm before the storm, the fever of the limelight, the numinous, the wind etc – and at times is more dependent on the subject, or condensed into (or anchored to) preferential objects. In any case, they are quasi-things whose ecstasies are expressive characters of qualities and whose extraneousness to thingly dimension and to the predicativestructure often lead to misleading projectivist explanations. (Griffero 2014, 108–9)

>feelings reside in a layer of experience
that comes before any clear distinction between subject and object **p.9**

atmos = exhalation/vapour; spharia = globe/sphere

>Where the network has nodes, the meshwork ... has knots. Knots are places where many
lines of becoming are drawn tightly together. Yet every line overtakes the knot in which it
is tied. Its end is always loose, somewhere beyond the knot, where it is groping towards an
entanglement with other lines, in other knots. (Ingold 2013, 132)

la notion de noeud peut être assez intéressante par rapport au paysage : les composantes de ce dernier se nouent du seuil perceptif jusqu'à l'infini, donc avec une possibilité de maillage de noeud infinie. Ainsi on ne perçoit pas chaque élément indépendamment (comme les neurones d'un réseau), mais plutôt comme des fils, des chemins, un élément glisse vers l'autre. La vallée est un chemin du ciel au ria, le son de l'eau du ria se répercute dans la vallée et le sujet écoutant perçoit l'eau et la vallée en lui même. Ingold parle d'une *danse de l'agentivité*[^1] On ne perçoit qu'une partie de la nasse et ses enchevêtrement complexes de fils se copénétrant inlassablement parviennent à tisser la globalité paysage dans laquelle chaque élément peut ressurgir indépendamment. La maille, à la différence du réseau, s'étire en puissance dans le temps, un noeud étant toujours possible, tant qu'on aura a minima deux éléments flottants.

[1]: danse of agency ingold,  2013