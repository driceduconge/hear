# *Paysages en Mouvement*, Marc Desportes, 2005

## Sommaire
- [Introduction](#intro)
- [L'oeil & le mouvement](#oeil)
- [Coupure Technique](#coupe)
- [Objet technique, déformation, séquençage](#objdeseq)

## Introduction {#intro}
Le paysage est né avec/par le progrès technique (la _distanciation humain/environnement par la technique_)

>La technique a joué un rôle de premier plan en créant de _multiples médiations_ qui s'interposent entre nous même et notre cadre de vie. **(p. 8)**
>
>les  _"paysages de la technique"_ [...] qui désignent [...] les regards induits par ces infrastructures sur le cadre qui les environne. **(p. 8)**

Ici on peut se dire en parallèle que les _environnements numériques_ sont porteurs eux-mêmes à la fois de regard (et d'écoute) sur un environnement (_médiation technique_) et à la fois producteurs d'environnement sur lesquels se portent ces percepts  ( _autopoïèse_[^1] ).

[^1]: L'autopoïèse (du grec auto soi-même, et poièsis production, création) est la propriété d'un système de se produire lui-même, en permanence et en interaction avec son environnement, et ainsi de maintenir son organisation (structure) malgré son changement de composants (matériaux). [Wikipedia](https://www.wikiwand.com/fr/Autopo%C3%AF%C3%A8se#/)

### La méthode Desportes

1. étudier la genèse du mode de transport (tech-socio-éco)
2. dispositions techniques du système de transport
3. étudier le nouveau régime de regard paysager produit
4. liens intentions techniques <--> culture (comment l'art et la perception provoquée se répondent et s'influencent)

**Différence entre le paysage et la vue**

>Les premiers [paysages], offrant une certaine liberté de composition, les secondes [vues] devant s'attendre à une exactitude topographique. __(p. 64)__

## L'oeil & le mouvement {#oeil}

>Il semble même qu'une part du spectacle réside dans sa _dynamique_. C'est au cours d'un mouvement que peut-être apprécié le jeu des ondulations et des perspectives changeantes. **(p. 73)**

La dynamique du mouvement produit chez l'oeil une _transformation des attributs formels_ (et sonores) du paysage perçu. Certaines lignes alors droites vont se déformer sous l'impulsion de différentes _vitesses_, le regard (non pas la vue) va alors porter sur d'autres éléments qui auraient pu être anecdotiques à l'arrêt :

>La ligne droite, lors de son parcours, joue le rôle d'un révélateur et confère le statut de signe topographiques aux variations du cadre naturel. C'est une sorte de _filtre sémiologique_. **(p. 84)**

Le régime de regard, alors _passif_ face à un paysage fixé se transforme en un régime de regard _attentif_ aux différents signes que le mouvement lui donne. Le mouvement, lorsque uniforme, va créer une variation visuelle continue (_fondu entre des images_), équivalent au bourdon d'un moteur continu que l'on va _écouter passer_ ou _dépasser_[^2]. L'on percevra alors non pas simplement l'aspect purement formel de la chose (c'est bleu, rouge, la montagne est courbée etc.) mais l'on percevra alors des sensations paysagères car l'on pourra _apprécier la distance qui nous sépare de plusieurs éléments_ à l'intérieur d'un environnement.

[^2]: La perception d'un son nous permet directement de déterminer son origine et le plus souvent sa localisation ainsi que sa vitesse : What in the World Do We Hear?, An ecological approach to auditory event perception. _Ecological Psychology, 5_, p. 1-29, William W. Gaver

>Toutes les descriptions supposent de la part du voyageur une forme _d'attention_. Il lui faut distinguer, sélectionner, certains faits, certains traits, certains éléments ; [...] l'imagination d'un récit qui _insère le fait observé dans une trame narrative_. **(p. 84)**
>
>On comprend dès lors en qoi consiste l'impression de se situer dans l'espace, celle-ci naît lorsque je me ressens comme _l'instance de coïncidence entre un champ d'agir et un champ objectif_. **(p. 87)**

Champ d'Agir
: Espace fluent, fait d'_intentions_, désirs et rejets. Ce que je _perçois_, représenté via le paysage.

Champ Objectif
: Espace qui existe par lui-même, en _dehors de soi_. Représenté par une carte par exemple.

>toute perception réveille l'opération intellectuelle qui l'a instituée comme signe. [...] l'expérience de la route conduit à des perceptions faisant revivre l'activitéde l'ingénieur. **(p.86)**

La conception de l'espace (la carte) impacte directement la perception paysagère de ce dernier, la composition bi ou tri-dimensionelle instaure des suites *prévues* de sensations esthétiques (je perçois puis je ressens). On peut se référer à la fabrication de paysage comme un travail d'ingénieur sur des paradigmes de représentation.

**L'art**, du moins le prisme artistique[^3], transforme donc aussi les modalités de perceptions du champ objectif (ou paysage réel pour Balibar, paysage authentique pour Jakob). _Le paysage réel se forme aussi par l'influence du paysage artialisé_, nos manières de regarder et regarder sont influencées par ce dernier (par ex. je veux voir la mer bleue, donc elle le sera pour moi, même si _objectivement_, elle est grise).

[^3]: Renvoie à l'artialisation du pays qui fait paysage.

>Je voudrais [...] ne jamais penser à la peinture quand je regarde le paysage, à la musique quand j'écoute le vent, à la poésie quand j'admire et goûte l'ensemble.[^4]

[^4]: Georges Sand, _François le Champi_, (1847), Paris, GF, 1973, p. 47

## Coupure Technique {#coupe}

Ici principalement à travers l'étude du train par M. Desportes.

>Le train [...] rend visible une succession de sites, mais sur un mode qui éloigne, qui repousse au loin. **(p. 152)**
>
>Les éléments du paysage apparaissent disparaissent sans continuité, sans lien, sans enchaînement. **(p. 154)**

Le glissement du train, dû non pas seulement à sa vitesse mais à sa _technique_, transforme la perception et brise la continuité jusqu'alors inhérente à un mouvement uniforme.

>Bris du rythme des choses, bris de leur mémoire, bris de la durée. **(p. 154)**

On empêche donc par ce bris, ce découpage brut par le système de transport, la possibilité de circonstance d'émissions sonores et visuelles[^5]. En créant une cassure entre le corps, le paysage (aussi une autre cassure inter-paysages) on empêche le déroulement de processus qui pourraient être de l'ordre de la rémanence, de _larsens percepts/réalité_ qui co-construisent les sensations de proprioception.

[^5]: Frédéric Mathevet, Reste de Son ou comment envisager d'autres modalités d'apparition, _L'autre Musique_, n°2, janvier 2013

Si ce découpage abrupt inter et intra-paysager "sort" le·a specateur·ice du paysage réel ou du moins, la _sensation de s'y trouver_, il n'annule pas la sensation paysagère et transforme simplement son caractère continu en un caractère _discontinu_. Cette discontinuité change les modes de perceptions du paysage, en l'approchant plutôt comme _fragmentaire_ et _rythmé_ que comme _uni_ et _bourdonnant_.

La technicité du train agit (selon M. Desportes), de manière analogue à la photographie sur ce point : 

> La photographie n'enregistre pas la réalité matérielle qui se trouve devant l'objectif mais son aspect visible, déterminé par le point de vue et le champ visuel à un moment précis et dans un éclairage donné. [...]  [le photographe] s'expose à créer des _hiatus_, des téléscopages, des juxtapositions fortuites, des condensations et _des lacunes inattendues dans la logique de l'organisation spatiale_.[^6]

[^6]:  Peter Galassi, _Before Photography: Painting and the Invention of Photography._ New York, MoMa, 1981 (trad. par M. Desportes j'imagine)

La différence principale dans la résultante esthétique entre le train et la photographie est que la photographie, même si elle aussi fragmentaire et rythmée, implique de par son rythme une _possibilité de déplacement mental_ dans le paysage. Par conséquent une certaine sensation de proprioception.

>[...] si le sujet s'avance, c'est que le lieu présente une profondeur; mais si ce même lieu semble profond c'est aussi que le sujet [...] perçoit en s'avançant que ce lieu est prêt à accueillir le mouvement de son corps. **(p. 174)**

## 	Objet technique, déformations, séquençage {#objdeseq}

Desportes définit tout d'abord deux types d'objet technique :

L'objet technique Majeur
: trop complexe pour que son fonctionnement effectif soit compris par l'utilisateur

L'objet technique Mineur
: Où la forme et la fonction vont de pair (ex: une hache)

Les objets techniques majeurs englobent des catégories larges "d'objets" techniques, on parle tout autant d'une gare ferroviaire qu'un ordinateur.

L'objet technique s'apparent alors à un objet magique : il performe des actes dont l'on comprend la résultante (le pixel s'affiche sur l'écran, le son sort des enceintes), mais dont seulement certain·e·s comprennent le fonctionnement. L'impulsion sur un bouton crée l'image, le son. Il y a une discontinuité évidente entre les physicalités[^7]. 

Desportes poursuit ça en couplant cette idée à la notion d'_espace_ :

Espace Mineur
: "en entendant par paysage [...] la relation consciente et valorisée d'une communauté à l'espace qui l'entoure", hi-fi[^8] sensoriel

Espace Majeur
: lo-fi[^9] sensoriel, chargé de signes isolant l'usager. Hyposignifiant (Hypo = insuffisance de)

[^7]: Philippe Descola, _Par-delà nature et culture_, Gallimard, 2005
[^8]: R. Murray Schaffer, _Le Paysage Sonore, le monde comme musique_, 1979
[^9]: Ibid.

>Le paysage apparaît comme un équilibre : équilibre entre une expérience mineure riche, multisensorielle, et une expérience majeure abstraite, distante ; équilibre, le plus souvent insu, _impliquant des processus de médiation_, picturaux, techniques ou autres, permettant d'assouvir ce désir d'une expérience spatiale qui engagerait pleinement le corps. **(p. 199)**

L'automobile pourrait alors être un exemple du paysage "équilibré", qui est à la fois chargé en signes, monosensoriel mais étant tout de même riche en possibilités d'expériences narratives. Le paysage se déroule, devient épopée.

>Ce paysage [...] c'est une _succession de vues_, s'enchaînant selon des angles variés pour peu que la route serpente. D'où ces effets de découverte, de masque, d'aperçu, dus à un _horizon qui s'ouvre_, à un relief que l'on contourne, à des obstacles dépassés. [...] Le paysage automobile naît d'une _conjonction entre le site traversé et la conduite adoptée_. **(p. 240)**

Cela peut ramener à la narrativité d'un espace numérique : ce dernier est en général perçu à travers un écran et/ou des enceintes et l'on interagit avec au moyen de périphériques qui répondent à des mouvements _signifiants_, la balade de la souris, le mouvement circulaire d'un potentiomètre... On est à distance de la réalité de l'espace numérique mais on en perçoit une fraction (la fenêtre, le son transformé) à travers l'outil médiateur. 
L'on se crée une narration par le biais de _l'interaction_ et des effets résultants sur la partie perceptible. (par exemple nivellement d'un filtre passe-bas, déplacement dans une vue 3D...). L'on reste immobile mais l'on opère des formes de _déplacements_.

En voiture il a y a des similitudes :

>C'est en voyant défiler, cachés par un pare-brise, les composantes de l'autoroute et les sites qu'elle donne à voir que ce spectacle peut être reçu comme une séquence rythmée. [...] L'automobiliste voit venir à lui des éléments, les dépasse, en découvre d'autres. **(p. 336)**

>Le parcours autoroutier est vécu, non pas comme une expérience totale, mais comme une _succcesion de séquences_, [...] se détache de son cadre spatial. **(p. 338-339)**

On peut donc découper d'un prisme technique selon M. Desportes le cadre créé par la voiture et l'autoroute, du plus proche au plus lointain : 

1. Les éléments de médiations techniques (volant, compteur, frein à main...)
2. L'infrastructure (l'autoroute)
3. L'espace environnant (abords de l'autoroute, ce que l'on considèrerait comme paysage)


##