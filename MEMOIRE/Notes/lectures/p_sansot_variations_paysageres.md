# *Variations Paysagères*, Pierre Sansot, Paris, ed. Klincksieck, 1983

> Une recherche sur le paysage devrait se montrer fidèle à une sorte de principe de visibilité, c'est-à-dire s'attazcher d'une façon constante ou du moins prioritaire à ce qui s'offre aux sens, à ce qui se perçoit, s'entend, se subodore. **(p. 25)**

## Le principe de visibilité

> Un jour viendra où nosu saurons percevoir à la mesure de nos souvenirs, où nous nous *ressouviendrons pour mieux ressentir*

> D'une part il [le paysage] se structure, il s'organise, il se refuyse à moi dans son originelle altérité, il se singularise par des signes qui nécessitent la plus grande attention. D'autre part, il *s'atmosphérise, il devient une vibration, une odeur, une émotion unique, il se mêle à moi comme s'il n'était pas distinct de moi*. **(p. 30)**

Le paysage prenant pour origine une perception sensible et attentive fusionne avec le sujet, iel se retrouve non pas pénétré, mais résonnant avec le paysage. Le coeur qui suit le rythme de la rivière et la peau qui s'harmonise avec le soleil du Midi.

>On éprouvera une jouissance certaine à transmuer notre expérience vivante à un niveau qui n'était pas le sien. **(p. 33)**

> Il vaudrait mieux alors parler du paysage au gsingulier pour évoquer cette continuité ou plutôt cette possibilité indéfinie duiu monde de nous adresse des signes, *d'advenir jusqu'à nous sous une forme sensible*. **(p. 35)**

> Ce que je rencontre dans le sensible, je ne peux pas en être l'unique possesseur, l'unique donateur de sens. Il est bon que je reconnaisse les autres source d'intelligibilité et de sensorialité. [...] Une sorte de reliefs va naîtrede cette multiplicité de regards et de constitutions de l'espace. **(p. 39)**

Le paysage n'existe pas uniquement dans un moment flottant et solitaire comme pourraient le faire croire les nombreux tableaux romantiques (avec Caspar F. en tête), mais aussi dans uen construction sociable et sensible, ou différents percepts s'accumulent pour donner forme à un site comme paysage.

## La transgression paysagère

>n'avons nous pas oublié que même l'espace pour palpiter a besoin du temps, ou alors qu'il n'est plus qu'une retombée du devenir ? **(p. 53)**

>C'est dire que nous n'excluons pas le mouvement, l'incident qui vient se loger dans l'écoulement du lieu **(p. 54)**

Le paysage n'est pas une vue immobile (au contraire de l'image-paysage), l'expérience sensorielle du paysage n'est pas un flottement, un moment de pur silence et d'appréciation que l'on viendrait briser. Non, le vertige paysager s'apparente à un enveloppement des sens par l'instant et l'espace, où la route se même aux pinsons, la sinuosité du trotoir avec les réverbères.

> Certes on délimite, on cadre un paysage pour le soustraire au trouble du reste du omnde. Mais les choses, pour coexister aussi unimment en un même fragment du monde, ont à inaugurer *leur propre espace et leur propre temps* différents de l'espace et du temps de l'univers. **(p. 61)**

## L'affection paysagère

>Je ne peux pas exactement désigner le siège, l'origine, les points forts de l'atmosphère, ce qui signifie qu'il n'y a pas de face à face et que je ne peux adopter une position frontale. [...] Loin de là un changement imperceptible peut apporter un bouleversement radical et crever la bulle magique **(p. 62)**

> Il n'existerait pas un paysage mais une multitude de paysages en puissance autour d'un lieu et le long d'une déambulation. **(p. 64)**

>Nous songeons à une propagation plus ondulatoire et moins corpusculaire. **(p. 66)**

> J'ai le pouvoir d'enjamber, de m'attarder sur les points forts, de neutraliser certaines zones sous la forme de blancs ou de marges inintéressantes. 

Le paysage est alors choix esthétique attentif.

>Enfin c'est une part de nous-mêmes qui serait sensible, végétale, minérale, aussi bien ciel que sabnle, que ville de pierres, que prairie, que forêt [...] **(p. 70)**

>*(une philosophie) du paysage doit, plus que toute autre, savoir être modeste et se montrer respectueuse de ce qu'elle rencontre mais qu'elle ne s'explique pas.* **(p. 72)**

## Le paysage sonore

> Il se produit plus fondamentalement une liaison du silence et de l'évènement sonore - et l'on voit mieux qu'il faut distinguer, en quelque sorte, un silence essentiel et une perte de voix momentanée [...]. Ce silence essentiel doit se comparer à *l'horizon* [...]
> 
>Car comme nous l'avons dit, une certaine amplitude semble utile à la constitution d'un paysage **(p. 76-77)**
 
>Quand nous songeons aux conditions de possibilité de la constitution d'un paysage, il nous faudrait accorder une place essentielle à ce *fond-insonore*, pas plus repérable matériellement, que l'invisible [...] pusique l'espacement des être et des objets apparaît comme pplus originaire que l'ampleur d'un regard, le dégagement d'un horizon etc. **(p. 80)**

On retrouve ici l'idée de Cox et dans la théorie des atmosphères de sorte de fond insonore, de fond sensible, d'atmosphère en quelque sorte, d'où viennent les sons du paysage, et sans ce fond insonore, cet *horizon* en quelque sorte pas de sensation paysagère, pas *d'oreille vers le large*.

> Si nous consentions à établir à nouveau une opposition un peu forcée entre le visuel et le sonore, nous dirions que le premier nous permet de mieux distribuier et de mieux articuler le paysage tandis que le second nous assure le plus souvent de son existence **(p. 82-83)**

L'image sans le son est morte, le paysage sans l'atmosphère n'est que reproduction désincarnée, sans chair, sans saveur.

