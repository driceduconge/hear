# _à l'écoute_, Jean-Luc Nancy, 


>Le sonore [...] emporte la forme. Il ne la dissout pas, il l'élargit plutôt, il lui donne une amppleur, une épaisseur et une vibration  [...] **(p.14)**

écouter c'est

>_suprendre la sonorité plutôt que le message_ **(p.17)**

On fait donc ici la différence dans chaque sens entre son état _passif_ et _actif_, Nancy parle de "nature simple" et d'état "tendu, attentif ou anxieux".

Voir & Regarder | Entendre & Ecouter

Le son est plus lent que la lumière, la lumière est _instantanée_, le son _va-et-vient_, il remplit l'espace que la lumière accable.

> le sentir (_l'aisthesis_) est toujours un ressentir, c'est à dire un _se-sentir-sentir_ : ou bien si l'on préfère, _le sentir est sujet_, ou il ne sent pas. **(p.23)**

>[...] le visuel serait tendanciellement mimétique, et le sonore tendanciellement méthexique[^1] **(p.27)**

[^1]: En cosmogonie, le terme de métaxie concerne toute chose intermédiaire variant comme moyen terme entre des extrêmes invariables. On aperçoit par cela qu'une indéfinité de réalités intermédiaires, relatives entre elles, participent des extrêmes opposées en tant que constitutions mixtes. *Exemple*: apprendre est métaxie entre l'ignorance et le savoir.

>Mais l'enjeu d'un travail sur les sens et les qualités sensibles est nécessairement celui d'un empirisme par lequel on tente une _conversion de l'expérience en condition à-priori de possibilité_... **(p. 28)**

Le présent sonore ne serait pas linéaire :

>C'est un présent en vague sur un flot, non en point sur une ligne, c'est un temps qui s'ouvre, qui se creuse et qui s'élargit ou se ramifie, qui enveloppe et qui sépare [...], etc. **(p. 32)**

Ecouter, ce n'est pas seulement entendre ce qu'il y a dans l'espace, c'est _entendre l'espace_, c'est tout à la fois prendre partie à un récit que d'en être le·a spectateur·ice. Ecouter, c'est autant _faire_ que _entendre faire_, passif et actif.

>[...] c'est être _en même temps_ au dehors et au dedans, être ouvert _du_ dehors et _du_ dedans, de l'un à l'autre et donc de l'un en l'autre. **(p.33)**

>La présence visuelle est _déjà là_ disponible avant que je la voie, la présence sonore _arrive_ : elle comporte une _attaque_ **(p.34)**

Il omet peut être la notion de déplacement, où tout autant qu'acoustiquement que visuellement, des choses apparaissent et disparaissent, _viennent vers nous_.

>Là où la présence visible ou tactile se tient dans un "en même temps" _immobile_, la présence sonore est un "en même temps" _essentiellement mobile_, vibrant de l'_aller-retour entre la source et l'oreille_, à travers l'espace ouvert. **(p. 36)**

Le son crée le temps, rend le visuel _tactile_[^2], car la vue prend alors vie, on s'éloigne du paysage silencieux - un silence de mort - pour insuffler le mouvement depuis un point de perception fixe. Si l'espace visuel immobile est considéré commez _zone morte_ alors la simple ouverture à l'écoute induit des échelles, des _déplacements_, des _variations_, fade-in & out d'une voiture, bourdon de la rivière, évènement d'un bang quelques rues plus loin...

[^2]: Edith Wyschogrod, _Doing before hearing : on the primacy of touch_, dans _Textes pour Emmanuel Levinas_, Paris, Jean-Michel Place, 1980

>Le "silence" en effet doit ici s'_entendre_ non pas comme une privation mais comme une _disposition de résonance_ **(p. 44)**

Si le son agit comme jalon temporel on reprend les mots de Pierre Schaeffer : 

>La seule introduction possible du langage dans la musique est celle des conjonctions [mais, où, est, donc, or, ni, car]

Si le son est conjonction, le visuel est les noms et le sujet est lui-même, performant les verbes. Il y a donc bien une sorte de _dynamique_ son et image, composer un paysage en omettant le son reviendrait à lui enlever des _connecteurs_, ce qui ne va pas forcément ruiner la phrase "J'aime les chaussures elles sont belles" mais lui enlever une composante fine de cette dernière "J'aime les chaussures, or elles sont belles", "car elles sont belles". Je m'avancerai et dirait que le sonore est de l'ordre de la _ponctuation et de la diacritique_, le son souligne, sur-signifie, transforme, retarde, rapproche, _respire_.

>Comme ce n'est pas par un acte distinct de l'esprit que nous composons l'idée du timbre, il n'est peut-être pas juste de l'appeler perception ; en percevant le timbre, nous ne mesurons rien. [...] L'intensité et le timbre sont des sensations immédiates, dans lesquelles nous ne pouvons remarquer de complexité qu'en nous servant de l'analyse extérieur.[^3]

[^3]: Lagneau, _Célèbres leçons et fragments_, Paris, PUF, 1964, p. 200




##