# *Les mots eet les sons, un archipel sonore*, François J. Bonnet, 2012, L'Eclat

> Poser l'oreille contre un coquillage, c'est se donner à entendre tout un océan. Le bruit coloré afflue et reflue en vagues interminables enserrées dans l'espace nacré et étroit. Pourtant, c'est un en-dehors qu'on perçoit **(p. 13)**

>Si la voix est le biais premier qui tisse un lien d'intimité entre le son et celui qui en fait l'expérience [...] elle est également puissance expressive, puissance d'évocation. **(p. 22)**

>Le son magique, lui, est actif. Il [...] est tendu vers un résultat. **(p.25)**

>Le son magique par excellence, c'est l'incantation. Le pouvoir magique réside dans la forme même du chant, mais également dans le *code* qu'il délivre [...]. L'incantation est en deçà ou au-delà du langage. **(p. 27)**
>
>La façon dont le code est formé, sa tonalité, est tout aussi importante que les syllabes elles-mêmes et leur enchaînement. *L'efficace magique découle uniquement de la confonction du code et de la sonorité qu'il prend*. **(p. 27)**

Le code informatique, sa logique implacable et sa syntaxe cryptique participe à l'incantation électrique de l'imaginaire, où le circuit agit comme canalisation et les périphériques comme cibles et récipients de la conjuration. A l'instar des tracés sur les tables tournantes, les portes logiques et leurs sillons chaînent une énergie modulée vers leurs réceptacles. Les pilotes constituants les différentes couches d'interfaces entre logiciel et périphériques effectuent la traduction de l'imaginaire au mirage ondulatoire et corpusculaire. 

---

> L'écho affirme la présence d'un son en révélant le milieu dans lequel il se déploie, le![])s points où il se réfléchit délimitant alors son périmètre **(p. 34)**


---

>En termes très concrets, on peut voir une fonction de hachage (non cryptographique) comme un moyen de replier l'espace de données que l'on suppose potentiellement très grand et très peu rempli pour le faire entrer dans la mémoire de l'ordinateur. [wikipedia](https://www.wikiwand.com/fr/Fonction_de_hachage)

>A la question "Qu'est ce qu'au juste l'aura ?", Benjamin [W. Benjamin] répond: " Une trame singulière d'espace et de temps: l'unique apparition d'un lointain, si proche soit-il." **(p. 49)** *extrait de W. Benjamin, "Petite histoire de la photographie" 1931, in Oeuvres II, trad. Pierre Rusch, Paris, Gallimard, Folio Essais, 2000, p. 311*

>Par l'entremise du phonographe, source actuelle et artificielle du phénomène, la source originelle du son reproduit est suggérée mais demeure inaccessible. [...] La source originelle est *simulée* par un générateur standard, le phonographe **(p. 53)**

>Comme autrefois le son-souffle l'était avec l'air, le son radiodiffusé devient intrinsèquement lié au champ électromagnétique. **(p. 60)**

>En utilisant nos yeux comme s'ils étaient nos oreilles, nous nous donnerons un peu d'air et de lumière dans le discours étroits et les définitions confinées liés aux pratiques sonores. *David Toop, Notes préliminaires à une histoire de l'écoute, in Art Press 2, L'Art des Sons, numéro 15, nov. 2009-janv. 2010, p. 13, trad. du gars du livre*

>Chaque écoute est ainsi l'occasion, ou l'activation, d'un processus réflexif à destination de celui qui est toujours à l'origine de l'écoute : l'auditeur. **(p. 86)**

---

> La perception est - entre autres - un mécanisme de génération des croyances, et ces croyances peuvent sûrement différer selon les modalités sensorielles qui interviennent dans leur formation. *Roberto Casati, Jérôme Dokic, La philosophie du son, p. 25*


>L'environnement dont nous faisons l'expérience, celui que nous connaissons et dans lequel nous évoluons, n'est ps découpé selon différents chemins sensoriels par lesquels nous y accédons. [...] La puissance du concept prototypique de *landscape* réside précisément dans le fait qu'il n'est lié à aucun registre sensoriel spécifique. *Tim Ingold, "Against soundscape", in *Autumn Leaves*, ed. Angus Calyle, London-Paris, CRiSAP / Double Entendre, 2007, trad. François J. Bonnet*

>la fiction n'est pas la création d'un monde imaginaire, opposé au monde réel. Elle est le travail qui opère des *dissensi*, qui change les mode de présentation sensible et les formes d'énonciation en changeant les cadres, les échelles ou les rythmes, en construisant des rapports nouveaux entre l'apparence et la réalité, le singulier et le commun, le visible et sa signification. *Jacques Rancière, *Les Spectateur émancipé*, Paris, La Fabrique, 2008, p. 72*

> [Il ne faut pas] résoudre le discours dans un jeu de significations préalable ; ne pas s'imaginer que le monde tourne vers nous un visage lisible que nous n'aurions plus qu'à déchiffrer. *Michel Foucault *L'Ordre du discours*, p. 55*

LIRE JEAN BAUDRILLARD *SIMULACRES ET SIMULATION*

> Le langage habite déjà le sensible [...] *J.F. Lyotard, *Discours, figure*, p. 41*

> Le lieu c'est le palimpseste *Michel de Certeau *L'invention du quotidien*, I, p.295*

---

>Je crois à ce que j'appelle la pensée archipélique. Parce qu'elle n'impose pas [...] c'est toujours une pensée de l'errance, une pensée du déplacement et non pas une pensée de l'imposition. Elle dit que le lieu n'est pas contradictoire avec l'ailleurs [...]. Et ce n'est surtout pas une pensée de système.[^1]

[^1]: Edouard Glissant cité in David Marron, "Elements pour une approche de l'archipel lyrique contemporain", communication lors de la journée d'étude "Littérature et musique" du 31 mars 2009 à l'ENS

>Les lignes d'erre marquent l'espace en territoires mouvants, hétérogènes et anarchiques **(p. 283)**

>L'imperceptible [...] n'est pas tant ce qu'on ne peut absolument pas percevoir que ce qui résiste à la perception ce qui se loge dans les limites de notre perception : "sites imprenables d'un territoire imperceptible qui déborde toujours"[^99]

[^99]: Pascale Criton, "Territoires imperceptibles", in *Chimères* numéro 30, printemps 1997, p. 65

![](../../BiblioIconoSono/Images/criton.png)[^99]

ECOUTER PHIL NIBLOCK ET CHARLEMAGNE PALESTINE ET SCELSI

CHECK DES ESSEINTES

LIRE DAVID TOOP SINISTER RESONANCE

>C'est pourquoi nous disons qu'il n'y a pas de véritable rite muet, parce que le silence apparent n'empêche pas cette incantation sous-entendue qu'est la conscience du désir *Marcel Mauss, *Esquisse d'une théorie générale de la magie*, chap. II, "les actes"*

L'atmosphère se loge dans la sous-entente, loin du marteau et de l'étrier, à des lieues du tympan, elle se cache au fond des boyaux, stratifiés à la manière des cavernes résonnantes du paléolithique, elle s'y love quelques instants pour pousser de là le coeur au bord des lèvres. Venue s'enf(o)uir sous l'écoute et le regard, elle creuse un sillon inaudible, irrépétable, elle pose un interdit sur sa redite, frappant de sacré l'instant flottant, loin de l'inaudible et de l'invisible elle flotte et s'accorde avec nos contrebasses intérieures pour faire chanter à l'esprit le vertige pan-ique. 

Le son et l'image ne sont pas atmosphère, tout comme le son et l'image ne sont pas le paysage. Les deux sont autopoïétiques et les circonstances environnantes, son et image en seraient plutôt des composantes que des causes ou conséquences. Ce sont des expériences qui se situent à la frontière des sens, et penser que tel son ou image provoque ou découle de l'atmosphère/ du paysage relève de l'apophénie, au sens où le paysage et l'atmosphères ne sont pas des objets perçus mais plutôt l'infiligement d'un discours à une sensation étrange, l'arbre qui tombe dans la forêt et l'orage.


