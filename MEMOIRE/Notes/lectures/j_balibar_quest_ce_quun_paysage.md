# *Qu'est-ce qu'un paysage ?*, Justine Balibar, 2021

## Sommaire
- [Réel & Représenté](#reelrep)
- [Le réel sans la représentation](#wout)
- [Métaspatialité](#meta)

## Réel & Représenté {#reelrep}

>Le paysage n'existe qu'en relation avec un sujet esthétique, il est l'objet d'une *perception* et d'une *expérimentation*, lesquelles sont médiatisées par toute une série de filtres culturels variés **(p.8)**
>
>La différence fondamenbtale entre paysage *réel* et paysage *représenté* tient à la situation spatiale du sujet percevant *par rapport* à l'espace perçu) **(p.9)**

Plus loin Balibar poursuit la différence de spatialités des deux types de paysage :

>[...] d'un côté l'expérience *contemplative* de la perception d'un espace séparé du nôtre, [...] de l'autre l'expérience *immersive ou intégrative* de la perception d'un espace en continuité avec le nôtre **(p. 11)**

Aisthesis 
: la sensibilité, la *perception sensible*

>Le dédoublement sémantique du mot "paysage" [...] semble surtout favoriser une tendance de *l'un à déteindre sur l'autre* **(p.12)**

On essaie alors pas forcément de prouver l'antériorité d'un type de paysage sur un autre, comme l'ont fait de nombreux·ses philosophes iconisters (A. Cauquelin, A. Berque, A. Roger) mais plutôt à remettre en valeur le paysage réel, à remettre en avant une expérience *sensible* d'un espace, d'un territoire. Réhabiliter le sensible pousserait alors à oublier, mettre de côté l'histoire de l'art, ses modes d'analyse lourds et superflus quand on parle de sensible. On n'aborderait alors plus une qualité de composition ou autre cadrage possible, mais plutôt un mode d'attention, un vertige sensible diffus et disponible.

>L'oeuvre invite à une véritable médiation sur les liens entre réalité et représentation, entre perception immédiate et perception médiatisée **(p. 28)**

<figure><img src="../../BiblioIconoSono/Images/Nancy Holt, Sun Tunnels, 1973, Great Basin Desert, Utah. Dia Art Foundation with support from Holt Smithson Foundation.jpg"></img><figcaption>Nancy Holt, *Sun Tunnels*, Installation, Great Basin Desert, Utah, 1973</figcaption></figure>

>[...] risque de projeter dans le paysage réel les infirmités ou les manques constitutifs de l'image ? Ne risque-t-on pas de se priver du principal intérêt de l'expérience d'un paysage réel [...] où l'on cesserait de le voir comme une image pour le voir comme un espace à parcourir et littéralement à *trans-gresser*, au moment où l'on renoncerait à la fenêtre pour passer plutôt par la porte **(p. 28)**


## Le réel sans  la représentation {#wout}

>il suffirait à un environnement d'être 'esthétique' ou d'être "esthétiquement perçu" pour être un paysage [...] La beauté, l'artialité et la contemplation désintéressée seraient donc trois interprétations possibles [...] de sa qualification au titre de paysage. Mais ces interprétations sont-elles pertinentes ? **Non** [...] elle est trop générale [...], elle est aussi trop partielle et trop partiale **(p. 36-37)**

Mais donc ce qui caractérise proprement un paysage c'est alors *l'ouverture spatiale* qui *"s'éprouv[e] de manière évidente et intuitive"* **(p. 46)** 

>Entre les espaces paysagers et les espaces non-paysagers, la frontière est moins une ligne mince et nette qu'une *zone d'imprécision, d'indécision*. Le concept de paysage implique de manière essentielle qu'il soit possible d'hésiter, de se demander si tel pan de colline suffit à faire un paysage **(p. 46)** 

Pour Balibar, le mouvement est essentiel comme médiation de perception du paysage réel :

>Le mouvement rend ainsi possible une variation constante des angles de vue et par conséquent une démultipilication indéfinie des vues paysagères.**(p. 54)** 
>
>Le paysage réel, parce qu'il repose sur une perception en mouvement, n'est pas une vue ou une série de vues discontinues comme le paysage représenté, c'est un *continuum visuel* **(p. 55)**

Pour elle (en se basant énormément sur la théorie de métaspatialité du paysage d'Assunto que l'on verra plus tard), le paysage en mouvement resserre le paysage au travers d'un prisme visuel, mais l'expérience justement d'un paysage réel ne prend-elle pas en comptes beaucoup d'autres évènements sensoriels ? (climat, terrain, son) 

## Métaspatialité  {#meta}

(une analyse et traduction de Paesaggio e estetica de Rosario Assunto)

Un paysage serait un type d'espace et non *un objet dans l'espace (ou la représentation d'objets dans l'espace)*

>Le paysage est espace, la représentation d'un paysage est représentation d'espace **(p. 105)**

Un paysage serait, pour en avoir une définition un espace ouvert et limité :

>Et la limitation du paysage en tant qu'espace, c'est *l'infini qui s'autolimite* **(p. 107)**

Cependant Balibar le précise :
>ce problème de définition d'un concept de paysage réel ne se pose guère que dans le monde des philosophes et des esthètes.


Plus loin précisant la définition d'Assunto

>C'est un espace intermédiaire, d'où la définition volontairement paradoxale ou oxymorique proposée par Assunto : il s'agit d'*un espace à la fois fini et infini, limité et illimité* **(p.117)**

Le paysage sous entend donc d'un point de percept ancré dans le sol, qui permet au ciel d'ouvrir l'espace.

>Le paysage, donc [...], n'est pas une représentation de l'infini, mais une mise en présence de l'infini. **(p. 120)**

