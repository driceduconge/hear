#*Les jardins de l'écoute*, Jean-Luc Hervé & Anne Cauquelin

>J'aime bien que le temps soit inconsistant, qu'il attende mon arrivée, tranquille, inconscient. L'attente n'est pas de mon fait, mais du sien. Car il n'est que cela : *étendue sans contour, espace vague en attente d'évènement.* (p.20)

>Modalité du temps, l'éphémère est dans l'entre-deux du jour qui se lève et du soir finissant, le temps d'un passage, d'une voie. (p. 32)

>[le baladeur] C'est le désir de modifier la réalité, d'ajouter une musique [...] qui joue le rôle d'une 'bande son" ajoutée à l'environnement (p.39)

>L'ordinateur actualise en permanence et différemments des modèles définis par le compositeur [pr projet de diplôme]

>*Les sons se déplacent (contrairement aux plantes)* (p.70)

>Soung-gui Kim "Un jour d'été je me promenais sur la haute colline de Pierre Feu où habitaient Jean et OUlga. J'ai été interrompue par une très belle pierre qui se trouvait à mes pieds. Un granit tout blanc taché d'un nuage. Je me suis dit que le nuage était tombé du ciel."