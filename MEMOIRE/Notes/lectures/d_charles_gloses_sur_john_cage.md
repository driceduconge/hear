# *Gloses sur John Cage*, Daniel Charles, UGE, Coll. 1018, 1978

>Composer signifie seulement suggérer à l'interprète la possibilité objectivement réelle d'une action - c'est à dire *ouvrir un espace de jeu*.(p. 16)

Laisser l'espace ouvert aux possibles, c'est l'affaire du jeu vidéo ? à voir si l'on peut gloser là dessus.

>Si la partition n'est plus un texte, mais un **prétexte** , si ce qui at été composé évolue d'une exécution à l'autre, et même dans le ocurs d'une même exécution [...], alors le réseau de possibles qu'est l'oeuvre se laisse saisir.(p. 20)

Le pré-texte prépare la réalité d'une action, la programmation informatique permet de faire jouer une logique sémantique intégrée elle-même dans l'environnement numérique via une syntaxe. On somme l'ordinateur de produire un évènement en traduisant une description (lacunaire, asensorielle) vers un bourdonnement électrique signifiant.


>L'interprète est un preneur de sons, au sens où l'on parle d'un preneur d'images(p.127)

Le calculateur prend le bourdonnement électrique des cycles de fonctioonnement des coeurs et le module dans les hauts-parleurs, il s'égosille de recracher la synthétisation d'un tapuscrit en son, il tente de faire le pont entre le sens et le sensible, trahit par le grésillement du câble liant la machine à son organe. trahit par son battement de coeur. John Cage dans la chambre anéchoïque.

La description d'un paysage par ses noms est (pour l'instant) impossible.


Le bytecode et le langage de compilation des ordinateurs sont bas niveau, au sens qu'ils perdent toute accessibilité sensible quand on les voit. Les enchaînements de lettres et de chiffres a-priori abscons se mettent en tension avec la perfection de la simulation. Ces suites binaires, hexadécimales semblent distendre l'espace sur une ligne, l'écrase en une dimension. Le paysage se replie sur lui même. Nous le déploierons par *abstraction*, non pas celle mathématique, mais bien en semblant oublier, à cet instant, le simulacre.

>L'arbre n'est pas le nom arbre, pas davantage une sensation d'arbre: c'est la sensation d'une perception d'arbe qui se dissipe au moment même de la perception de la sensation d'arbre(Paz, *le singe grammairien*, p56)

> Mon nom, le tien, celui d'un enfant qui n'a pas encore  
> vu le jour, tous forment les syllabes du grand mot que  
> prononce très lentement l'éclat des étoiles. (Terremer, Ursula K. Le Guin)

L'invocation des mers et des vents nécessite de savoir les appeler.

>le monde spirituel n'est autre que le monde des sens, et le monde des sens n'est autre que le monde de l'esprit. Le monde est un et parfaitement total (*L'essence du Bouddhisme*, Suzuki, 1955)

>Inutile de parier sur les sons[...] on ne s'assure pas de l'existence de la mer, ni de l'existence du vent.(p.57)

---

Certaines algorithmes de DL, on citera notamment DALL-E, permettent de produire des simulacres de reproductions paysagères à partir de descriptions sommaires. Mais elles laissent inassouvie (pour le moment) la (les) sensations résonantes intrinsèques à l'évènement paysage.

Elles produisent des images mortes. Reflets de Narcisse.

*pleine page image DALL-E*

---

>Parce que le silence n'existe pas,[...] [ils sont] de l'ordre du discontinu.(p. 32)

---

>Qu'arrive-t-il en revanche, si l'on se soucie d'entendre - et non plus de reconstruire [...] ? On assume l'errance.(p. 50)

-> ça rejoint les notion de perceptions et rentre à rebours de ce dont GAVER et al. parlent. Certes on perçoit la source et *il est difficile, voire impossible* d'en faire abstraction, l'écoute réduite, soutire l'information soit disant au profit du son.
Je veux garder la douceur d'écouter les stridulations des cigales pour ce qu'elles sont, le bruissement éternel des étés.

L'absence des cigales en Berry me donne des vertiges. Le trou laissé me révèle le cri de la terre sous mes pas.

---

Si la musique de John Cage ne suscite pas d'attente autre qu'elle se termine, la matrice sonore ambiante ne s'essoufle jamais, le monde n'a pas de soupirs. Le seul silence existant est lorsque l'on ne résonne plus.

*à affixer à un paragraphe sur l'oeil et l'oreille*

l'oeil consent au chaos, y déniche des régularités, des lois, l'oreille, est contrainte de perdre la trace de ce qu'elle a retranscrit dés que le signal a été émis, le son n'existe qu'au présent.

L'enregistrement fait resurgir les morts. On n'écoute que ce qui a disparu.

Le noise se glisse "au-dessus et au-dessous"[^2] de l'entente, il prend l'interstice comme tangente. Ombre sonore.

[^2]: p.93

Le paysage est vu comme arrêté, figé par l'huile et l'encre mais il se trouve qu'il est inexorablement teinté par le temps, rien qu'une photo, de par son diaphragme et sa vitesse d'obturation fera entrer *un certain temps* dans l'appareil. La photo est une boucle musicale.


>la poésie est temps en son fond (p. 200)

L'image est polysémique, les sons ne parlent pas, l'atmosphère synthétise le sème et l'a(sème).

>chaque son, chaque bloc devient image. De son, *il vire en bruit*.(p. 207)

>Penser le Temps - c'est penser l'identité de l'aller et du retour. [...] dans le même trait *revenir*. (p.230)

Le ciel limpide parmi les entrelacs de la vigne et des roses (Rolland, J.-Chr.,Buisson ard., 1911, 1343)

[Le paysage] entrelac du fini et de l'infini.
   
   
