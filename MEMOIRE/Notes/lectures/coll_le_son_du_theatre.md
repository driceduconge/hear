# *Le son du théâtre*, coll., dir. J.-M. Larue & M-M. Mervant-Roux, ed. CNRS, 2016
## Table des articles
- [Historiographie de l'écoute, Alain Corbin](#histo)
- [Les espaces stéréophoniques du paysage sonore, Jonathan Sterne](#ster)
- [Théorie des atmosphères, L'écoute, le silence et l'attention au théâtre, George Home-Cook](#atmo)
- [L'impossible " façade " L'usage du microphone et son incidence sur la scénographie des espaces, Daniel Deshays](#facade)
- [Une géographie sonore et sensible des villes, Anne Gonon](#walkman)

##Définitions

L'auralité : 

>L'auralité nous dispense d'aborder les menus détails d'une spéficitié sensorielle si fragmentaire. Elle englobe l'anatiomie et la réalité de l'oreille, le sens de l'ouïe et l'engagemzent de l'écoute, *l'engagement du sensible et la matérialité du senti*, la vibration et la résonance, et tout ce que l'on perçoit - le son, le silence et le bruit.

Lynne Kendrick, Auralité et performance de l'audible, in *Le son du théâtre*


##Historiographie de l'écoute{#histo}
  > l'esthétisation du silence, toujours selon le code du sublime et de la cosmisation, c'est quelque chose qui traduit son appréciation romantique **(p. 24)**
 
 >l'ouie concerne une sphère
 
 il parle aussi de l'enregistrement sonore comme un plus grand changement de paradigme..
 
 plus loin :
 
 >L'intensité crée  une *coupure sonore*. C'est  à dire que *l'intensité sonore isole*, et de la société et de l'espace, on est entre-soi plus fortement.
 
## Les espaces stéréophoniques du paysage sonore{#ster}
nb : paru en anglais en 2015 ds Living stereo, histories & cultures of multichannel sound

>le paysage sonore est un concept médiatique, c'est un concept qui exige de celui qui écoute qu'il fasse l'expérience du phénomène plus large des médiations soniques depuis un point stable et étonnament fragile. **(p. 40)**

A propos de ce point :

> Les auditeurs n' "ont" pas un point d'audition, ils sont "positionnés" par la composition sonore [...] par l'emplacement des locuteurs dans un espace donné ou par *les caractéristiques d'un système sonore*. **(p. 43)**

L'audioposition sous entend alors que l'auditeur·ice est situé·e, dans un espace et il y a un *choix* dans ce qu'iel percevra ou non.

>le concept [de paysage sonore] exige de l'auditeur qu'il soit en relation avec le monde comme si ce dernier était un enregistrement ou une composition, en un mot, une oeuvre **(p. 49)**

Dans Schafer on retrouve alors une méthode très classique, il faut "apprendre" à écouter, il faut distinguer les éléments, écouter leur mélodie. On ne ressent pas purement l'ambiance de  l'espace à travers le son, mais on dissèque la rivière et la grenouille ensemble, avec les oreilles. On se rapproche alors d'une approche très iconiste du paysage sonore : on ne pourrait l'apprécier que car auparavant il y a eu de la musique inspirée des-dits paysages, ou on a décidé d'isoler par l'enregistrement certains sons.

>L'essence du paysage sonore, et à vrai dire l'essence de la stéréo *n'est pas l'espace physique ni la relation entre l'espace physique et sa représentation.* Ce qui constiture son essence c'est une audioposition stable **(p. 53)**

> Le son ne *traverse jamais l'espace vide*. C'est au contraire, la texture de l'espace qui rend le son possible.
>
>Dans la logique de Lefebvre, le "paysage sonore" ne serait plus une totalité ni un espace physique et sa représentation. **Un paysage sonore est simultanément un ensemble de pratiques sonico-sociales, les métadiscours qui les décrivent et les conditions qui rendent possible l'expérience de cet espace.** **(p. 54)**

Le paysage est alors produit par l'espace et par l'effet que ce dernier a sur les ondes, sonores ou visuelles, le son et l'image, par le regard et l'écoute *prennent de la profondeur*, on se projette dedans, on mesure sensiblement le tintement de cloche comme la distance avec le lac. Le paysage est *produit par une médiation sensible avec l'espace*. L'espace n'est pas producteur, l'espace est tel qu'il est. 

Mais alors dans un contexte informatique, l'ordinateur ne produit pas un espace mesurable esthétiquement, l'ordinateur, par l'écran *aplanit, écrase par son expérience frontale* et par conséquent ne peut être un paysage, mais des impressions de ce dernier. L'ordinateur produit de *"l'air de"* du faux-semblant : ça *m'a l'air d'être un paysage*, on me fait percevoir ce que je dois ressentir. L'ordinateur est médiateur direct, oublie la vue et l'écoute et le·a programmeur·se reconstitue à son aide les filtres de l'écoute et du regard.

>Heidegger s'inquiétait d'une "image du monde" amenée par la fonction d'arraisonnement de la technologie moderne. 
>
>Et si jouant les critiques, nous avions accidentellement utilisé le concept de "paysage sonore" pour consituer une collection d'enregistrements du monde métaphoriques, que nous contemplons vêtus de smokings métaphoriques au milieu de notre salon mental [...]? **(p. 56)**

## Théorie des atmosphères. L'écoute, le silence et l'attention au théâtre {#atmo}

>les atmosphères ne peuvent véritablement naîtres que si elles sont *remarquées* : ressentir une atmosphère c'est remarqiuer qu' "il y a un je ne sais quoi dans l'air", c'est percevoir vaguement quelque chose **(p. 201)**

>J.-P. Thibaud définit l'ambiance comme étant un support à partir duquel le monde sensible se configure au quotiidien, comme le champ à partir duquel les phénomènes émergenbt et s'individuent.

Ca me rappelle la théorie de Cox sur le bruit de fond.

>Kathleen Stewart :
>
> Une atmosphère n'est pas un contexte inerte, mais un champ de forces dans lequel on se trouve [...] un *effet vécu - une capacité d'affecter et d'être affecté*

L'atmosphère semble très parallèle au paysage, reposant sur des principes d'attention, de sensation esthétique, d'affect. On *ressent* l'atmosphère :

> être dans le son ne veut pas dire que l'on y est "immergé" directement et passivement comme dans une sphère. Il s'agit plutôt d'*un engagement corporel polymorphe et intersensoriel* avec ce qu'un environnement donné propose à l'attention. **(p.203)**

L'atmosphère, l'*ambiance* sonore, serait résultante d'un processus d'écoute, de résonance, sonnance avec le vironnant. On n'y est pas plongé, mais on s'y *projette*, s'y active. A la manière de la projection dans un paysage.

>Quoique le silence soit supposé être une absence, le retrait du bruit est remplacé par un phénomène plus fort, une focalisation de l'attention, une atmosphère, que l'on décrit à tort comme un silence. 
>
>*Alison Oddey, Re-Framing the Theatrical: Interdisciplinary Landsacpes in Performance*, Basingstoke, Palgrave MacMillan, 2007, p. 196


>Ecouter, *c'est se mettre soi-même en tension*, et être écoutant confère à la fois le sentument d'être dans l'espace et de faire résonner l'espace. [...] En faisant résonner le son, en ressentant l'atmosphère, l'écoutant fait résonner l'espace et, ce faisant, il *crée l'espace*. **(p. 209)**

L'atmosphère ce serait alors l'espace et le sujet, au même titre que le paysage. Mais ici il n'y a nulle mesure des distances ou perspective. Il y a la sensation *de quelque chose de différent*, et au final l'atmosphère découle et produit le paysage, c'est son arrière-fond, le plan sonore disponible à l'écoute. Je regarde le paysage et j'écoute l'atmosphère. Je perçois et je ressens, je crée l'évènement sensoriel en choisissant de porter attention. On génère le paysage et l'atmosphère ensemble. 

## L'impossible " façade " L'usage du microphone et son incidence sur la scénographie des espaces {#facade}

>L'amplification est en quelque sorte la liquidation du désir par la livraison d'un "*objet voix*" ficelé, éteint et refroidi, possédé. **(p. 261)**

Cela aurait-il un lien avec l'image-paysage ? on livre non pas une voix/un paysage mais sa représentation ? Il y a-t-il artialisation par le biais de la technique (hauts-parleurs + microphone) ?

> Si le premier rôle de l'amplification est d'obtenir un gain d'intensité sonore, celui-ci sera de toute façon associé à une perte de pouvoir séparateur, c'est-à-dire d'une perte de posssibilité de localiser la source [...] La baisse de qualité, autrement dit, de définition, du signal est quant à elle liée à sa simple circulation dans des circuits qui, *aussi numériques soient-ils, ne sont pas neutres pour autant* **(p. 262)**

>*La sonorisation comme double*, voilà ce qui confirme l'idée de séparation entre corps gesticulant et voix sortant du haut-parleur [...] La séparation corps-voix induit de fait une désincarnation : corps aphone gesticulant et voix objet. [...] Perte de la profondeur du corps comme perte de la profondeur acoustique du plateau. **(p. 264)**

>A cet endroit, point de possibilité de "non-émission" par fermeture de la bouuche, ici la gueule est toujours ouverte, grande ouverte. Ca dégueule du son, et quand l'articulation du discours n'est plus transmise, *un souffle continue de s'émettre, signe de l'ouverture permanente du robinet à flux*. **(p. 264)**

##Une géographie sonore et sensible des villes, Anne Gonon {#walkman}

Cet article est à propos de la pièce *WALK MAN 1* de Hervé Lelardoux, sorte de sound walk où le·a spectaeur·ice est guidé·e par la voix sur un baladeur mp3.

>En enlevant ce que semblent être les éléments constitutifs du théâtre - la présence physique de l'acteur , le plateau, un décor, la sallle..., WALK MAN 1. fait paradoxalement exister encore plus fortement le théâtre, dans une forme purement contextuelle, sensorielle et immatérielle. **(p. 384)**