# *L'être et l'écran, comment le numérique change la perception*, Stéphane Vial

## Technique et expérience du monde par la technique

>Le phénomène numérique ne fait que rendre visible, par son ampleur, un trait philosophique caractéristique de toute technique en général, resté relativement inaperçu mais essentiel : *la technique est une structure de la perception*, elle conditionne la manière dont le réel ou l'être nous apparaît. **(p.99)**

>L'expérience est le fait de la perception. Et la perception, c'est l'interaction avec le phénomène. __(p.108)__

> [...] la technique peut être définie comme une matrice ontophanique, c'est-à-dire une structure générale de la perception qui conditionne a priori la manièredont les êtres apparaissent. __(p. 111)__

Ll'ordinateur est une machine pétrie de "matrices ontophaniques", mille-feuilles de structures de translations de régimes physiques et perceptifs. L'on se déplace dans une structure croissante de l'abstraction à l'illusion de réel : l'impulsion électrique initiale qui parcourt le système nerveux et enclence le "coeur", vient ensuite une numérisation binaire de cette électricité en lui faisant parcourir des portes logiques et alors on pourra assembler tous ces chiffres pour arriver à un *signifiant* pour l'ordinateur, qui vient traduire ensuite cette information en un autre type d'impulsion électrique commandant à des périphériques d'émettre un certain *output* que l'on réceptionnera (ou non) sensoriellement.

>Si l'art invente les brouillards et les fait venir à l'existence, c'est bien que la culture - artistque, dans ce cas - a le pouvoir d'engendrer de la phénoménalité. [...] mais, répétons-le, les techniques avec lesquelles nous vivons, à une époque donnée, *conditionnent les modalités mêmes de la manifestation possible du monde* à cette même époque. __(p.118)__

## Virtuel

>La *virtus* n'est pas une illusion ou un fantasme, ou encore une simple éventualité, rejetée dans les limbes du possible [...] Le virtuel n'est [...] ni irréel ou potentiel : le virtuel est dans l'ordre du réel. 
>
 (P. Quéau, *Le Virtuel...*, p. 26)

> L'imaginaire se développe parallèlement au monde réel et ne préntend pas le rencontrer. Au contraire, *le virtuel y prépare et il est destiné à s'y actualiser*. 
>
>(J.-F Bach, Olivier Houdé, Pierre Léna et Serge Tisseron, *L'enfant et les écrans, un avis de l'académie des sciences*, Paris, Le pommier, 2013, p.20))

 Alors l'imaginaire est une autre réalité alors que le virtuel serait plutôt un "possible",  une projection temps réel du réel, dans une autre perspective, à travers une autre couche ontophanique ?
 
 >Nous tournions autour des images, maintenant nous allons tourner dans les images. [...] Les images virtuelles ne sont jamais seulement des images, juste des images, elles possèdent des dessous [...] des au-delà, elles forment des mondes.
 >>
 (P. Quéau, *Le Virtuel...*, p. 9)

Le virtuel se situe dans un entre deux, entre l'imaginaire et le réel, entre le possible et l'actuel. Le virtuel peut il s'infiltrer dans l'actuel par le biais de l'*atmosphère*, par cet instant de flottement, ce moment de l'espace où le réel semble laisser entrevoir quelque chose d'autre. L'espace alors bascule doucement de teine, effectuant un décalage d'émissions, un *déphasage incaptable*, l'insondable prendrais le pas sur le rationnel et produirait une fulgurance virtuelle, comme échappée de l'imaginaire incurvée dans le réel.

> L'information est le réel et non pas dans le réel ou devant le réel ou auprès du réel ou consécutivement au réel. "Elle est le réel", cela signifie que l'organisation de la vie dans son ensemble [...] est comme perfusée de flux informationnels [...] l'architecture et la dynamique effective.
> 
> (P. Mathias, *Qu'est ce que l'internet*, p.32)

