
allothétique et idiothétiques
Les informations allothétiques sont fournies par
l’environnement et sont de nature visuelle, auditive, olfactive et tactile principalement.
Les informations idiothétiques sont internes et proviennent du système vestibulaire
(perception du mouvement et de l’orientation par rapport à la verticale), du système
somatosensoriel (informations sensorielles du corps) et du système proprioceptif (position
réciproque des parties du corps).

>Malheureusement, l’espace est resté voyou et il est difficile d’énumérer ce qu’il 
engendre. Il est discontinu comme on est escroc **Bataille, 1930, p. 41**

>En botanique, la déhiscence désigne
l’ouverture naturelle de certains organes végétaux (comme un sporange ou une étamine),
selon des zones définies, pour libérer les fruits, les graines, les pollens ou les spores qu’ils
contiennent. De même, le sujet s’ouvre à ce qui l’entoure selon des lignes déterminées **^.86**