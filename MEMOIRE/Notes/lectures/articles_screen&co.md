# *Resolution: Digital materialities, thresholds of visibility,* Francesco Casetti and Antonio Somaini, NECSUS European Journal of Media Studies, Spring 2018



https://necsus-ejms.org/resolution-digital-materialities-thresholds-of-visibility/

>Given that the resolution of a digital image may change depending on the format it is stored in, on the transmission technology that allows it to circulate, and on the display through which it is visualised, focusing on the question of resolution and of its various degrees is a way of emphasising the specific, layered materialities of digital technologies, thereby countering a whole ideolology that tends to present digital images as dematerialised.


