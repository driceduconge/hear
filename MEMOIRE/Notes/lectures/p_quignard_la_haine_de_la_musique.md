#*La haine de la musique*, Pascal Quignard

>Sei Shônagon en l'an mil, dans le palais de l'impératrice, à Kyôto, à plusieurs reprises, dans lke journal intime qu'elle enroulait et enfouissait à l'intérieur de son oreiller en bois [...] nota les bruits qui l'émouvaient.
>Les sons qu'elle ressassa le plus, sans qu'il semble qu'elle en ait jamais pris toute la mesure, ou qu'elle en ait perçu les raisons tant la solitude et le célibat l'étouffaient, et qui à chaque fois ramenaient avec eux en elle le sentiment de la joie [...], consistaient dans le bruit des chariots de promenade sur le chemin sec, l'été, à la fin de la journée, quand l'ombre gagne tout le terrain visible de la terre.
>**à scanner, page 21**

>Des sons non visuels, qui ignorent à jamais la vue, errent en nous. Des sons anciens nous ont persécutés. *Nous ne voiyons pas encore. Nous ne respirions pas encore. Nous criions pas encore. Nous entendions.* (p.24)

>Dans les instants le plus rare, on pourrait définir la musique : *quelque chose de moins sonore que le sonore*. Quelque chose qui *lie le bruyant* (Pour le dire autrement : un bout de sonore ligoté (p.24)

Le paysage sonore, a longtemps été considéré comme une façon musicale d'écouter les sons de l'environnement, tout particulièrement car ces bruits se lient, se répondent et s'entrechoquent. Si l'on a considéré le paysage (le monde) comme musique, c'est car on a cru entendre un orchestre alors. Pourtant plus simplement, chronologiquement l'orchestre a entendu des paysages.

>L'hymne X du *Rigveda* définit les hommes comme étant ceux qui sans y prendre garde, ont pour terre l'audition. (p.39)

>Chaque son est une minuscule terreur. *Tremit*. Il vibre (p.42)

Chaque son(ne) est un cri, un cri panique depuis l'intérieur, depuis les tripes, vers le dehors, vers l'autre que soi. Le son se forme dans l'intime et se module, s'incarne en vibration pour habiter l'espace. 

On dit bien que l'on chante avec le ventre, c'est là que loge l'estomac, les poumons, le coeur.

>Toute vibration qui approche le battement du coeur et le rythme du souffle entraîne une même contraction (p.53)

>*Les mots forment chaîne dans le souffle. Les images forment rêve dans la nuit. Les sons aussi forment chaîne le long des jours.* (p. 55)

>La vieille corde asème vibre peu à peu [...] l'émotion tombe sur nous d'un seul coup. Tout bouleverse soudain à nouveau tous les rythmes du corps mais rien qui ait réellement sens n'a pu être fourni. (p. 61)
 
 L'atmosphère efface la raison, l'atmosphère, autopoïétique, génère elle même l'émotion, elle force à faire -re-surgir du sensible, une impression d'espace étendu. L'atmosphère retrace fugacement l'horizon rêvé, elle retape la scène et brise souffle (sous les murs, les mûres). L'on a l'impression d'y flotter éthérant un instant. La bourrasque passée, le sujet retombe brusquement au sol, ébété quelque instants, surchargé du ressenti expédié dans son souffle.

>**à caser important** Il y a un vieux verbe français qui dit ce tambourinement de l’obsession. Qui désigne ce groupe de sons asèmes qui toquent la pensée rationnelle à l’intérieur du crâne et qui éveillent ce faisant une mémoire non linguistique. *Tarabust* (p. 62)

>Je recherche le tarabustant sonore datant d'avant le langage. (p. 62)

A lier avec l'émotion du bruit blanc.

>Avec l'oeil seulement on "entend" les sensibles. (p. 71)
>
>Je propose d'appeler "note inouïes" ces sons écrits injouables qui font penser à ce que les gramairiens nomment les *consonnes ineffables* (le p dans sept) (p. 71)

>Ce ne fut pas la pluie qui tambourinait. 
>
>Ce fut le tambourin qui appelait la pluie.
>
>C'est Thor qui tient le marteau. (p.73)

>*Entendre, c'est être touché à distance* (p. 108)


>**S'absenter de l'environ n'est pas possible pour l'ouïe. Il n'y a pas de paysage sonore parce que le paysage suppose l'écart devant le visible.** *Il n'y a pas d'écart devant le sonore.*
>
>Le sonore est le pays qui ne se contemple pas. Le pays sans paysage. (p.110)

>L'écho est la voix de l'invisible. [...] Dans l'écho,  l'émetteur ne se rencontre pas. C'est le cache cache entre le visible et l'audible. (p.149)

*Dans le mémoire mettre la légende d'Echo et de Pan*

>La lumière a ses chants.
>C'est pour leur cri que j'aime les feux.