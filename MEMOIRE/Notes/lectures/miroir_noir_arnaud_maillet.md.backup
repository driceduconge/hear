# *Le miroir noir, enquête sur le côté obscur du reflet*, Arnaud Maillet


## Claude Glass & Instagram

Le Claude Glass/Claude Mirror qui reste en filigrane tout au long du livre comme non pas le miroir noir, mais une certaine approche du paysage. Où on applique une sorte de filtre à ce que l'on observe en regardant à travers une *glace* aux différentes teintes. On colore ainsi l'espace observé.  

>"Gilpin regrette que la vue qu'il a par la fenêtre d'un ermitage en Ecosse [...] se fasse à traversdes carreaux de fenêtres qui sont en partie composés de verres rouges et verts.[...] ces carreaux offrent un noubel et surprenant effet, transformant "l'eau en une cataracte de feu, ou en cascade de liquide vert-de-gris"[...]. *

Ces verres colorés sont certainements amusants, mais ils seraient mieux, écrit 

>Gilpin, *accrochés dans des cadres avec des poignées afin de les utiliser selon son bon plaisir, que d'être fixés ainsi en permanence, imposant au spectateur leur constante coloration.*

<- commentaire par l'auteur sur W. GILPIN, *Observations, Relative chiefly to Picturesque Beauty, Made in the year 1776, on Several Parts of Great Britain: Particularly the Highlands of Scotland.*, R. Blamire, Londres, 1789, 2 vol., vol. I, p.124 <-

Ici ce passage, lu en 2022, interpelle clairement, ces verres teintés que l'on appliquerait sur une vue paysagère (au sens que lui donne A. Cauquelin de fenêtre sur un espace). Cette histoire est semblable aux filtres que l'on a retrouvé tout d'abord sur des outils comme Photoshop, puis accessibles au grand public via Instagram, où l'on applique un filigrane numérique teinté, accentuant, au choix, ombres lumières, couleurs... Ces mêmes filtres qui se sont par la suite retrouvés intégrés aux logiciels de photographe des smartphones (ainsi que certains modèles d'appareil photo). Le paysage regardé à travers une lentille numérique, une couche de code venant se superposer sur l'image, la transformant en quelque chose de plus "vivant" et "accrocheur" que la photo si laissée brute. La Claude Glass était en quelque sorte le prototype de ce principe de créations de vues à la volée.

### Enchaînement des filtres, continuité et discontinuité

>"L'auteur [Gilpin encore] est "transporté rapidement d'une image à une autre. Une succession d'images hautement colorées glisse continuellement devant l'oeil." Il arrive même que "le coup d'oeil éphémère d'une bonne composition arrive à les unir[^3]."

On note ici que ce coup d'oeil sur la suite d'images vues (Les images sont l'objet de la vision et le "coup d'oeil" est alors le regard porté sur les vues, le *regard se porte alors sur la suite de vues pour reconstituer un paysage*.

Au contraire l'auteur écrit plus loin que Proust fait courir son narrateur "dans un train de fenêtre en fenêtre "pour rapprocher, rentoiler les fragments intermittents et opposites[^4]" d'un paysage contemplé au petit matin"(p. 137)

Maillet prend soin de préciser que chez Gilpin c'est une suite continue d'images qui re crée une autre image alors que chez Proust c'est une discontinuité d'images qui sont rassemblées mentalement pour en fabriquer un autre. L'un enchaîne les filtres pour créer une image nouvelle et l'autre recompose vue à vue un paysage (inexistant car seulement rêvé par ce que l'on a vu et ce que l'on cherche à revoir dans chaque fragment).

[^3]: W. GILPIN, *Remarks on Forest Scenery...*, p.67
[^4]: M. PROUST, *A l'ombre des jeunes filles en fleur,* in *A la recherche du temps perdu*, Robert Laffont, Paris, 1987, 3 vol., vol 1, p. 543. L'auteur suggère d'aller voir Deleuze au sujet de la vision cinématographique liée à cette desc.

## Sur le miroir convexe/concave et les déformations induites.

>"L'horoptère est la portion d'espace comprise entre une surface courbe concave proche de l'observateur et une autre surface courbe, convexe, plus éloignée. Tous les objets situés dans cet espace seront vus nets. Tous ceux situés un peu en dehors induisent une diplopie corrigée au niveau cortical, mais à l'origine de la *sensation de relief*. Je parlerais donc [...] non d'"images visuelles" ou "rétiniennes" mais d'"images mentales" qui ne sont ni planes ni courbes !" **(p.78)**

>"[...] car un tableau ne représente pas une image vue à travers un oeil : l'objet réel, tout comme l'objet peint, sont tous deux objets du regard. Un tableau n'est pas image de la vision. [...] Si la vue est toujours vue de quelque chose, elle n'est pas vue d'elle même." **(p.80)**


## Par rapport au miroir comme moyen de regarder la nature 
(en lui tournant le dos) : 

### La réalité de son reflet:

>" "il ne s'agit plus de la reproduction de la nature par un tableau," mais d'un "tableau [qui] est projeté dans la nature"." __J. BALTRUSAITIS, *Aberrations...*, p227__
>
> **(p.113)**


>"[...] la nature reflétée par le miroir convexe noir est toujours fictive et incomplète, c'est-à-dire en défaut par rapport à la nature elle-même, parce que reflétée (aspect mécanique) et réduite (aspect fragmentaire) par ce miroir. Cependant cette double imperfection du reflet de la nature tendrait alors à être [...] idéalisée par le miroir lui-même [...] le miroir tend à abstraire." **(p.128)**

> [L'art] "est fictif quant à la vérité et [...] incomplet quant à la ressemblance". 
>
>**(source inconnue, page 128)**
 
 
## L'imagination comme moyen de "voir" le monde:
 
> "Imagination est un terme subjectif; il concerne les objets non tels qu'ils sont, mais tels qu'ilsa apparaisent à l'esprit du poète.
 L'imagination est cette lentille intellectuelle [intellectual lens] à travers l'intermédiaire de laquelle l'observateur poétique voit les objets de son observattion, modifiés à la fois dans leur forme et leur couleur."
 
   Christopher WORDSWORTH, *Memoirs of Wiliam Wordsworth*, éd. Henry Reed, Ticknor, Reed and Fields, Boston 1851, 2 vol., vol II, p. 487
   
 *(traduction par l'auteur A.M., p. 117)*
   
   ## L'invention selon les néo-classiques, la machine
   
> "L'invention est "entendue comme la faculté de combiner de manière nouvelle les corps meorcelés pris de la nature individuelle, comparés, arrangés. L'invention est ce qui fait tenir ensemble les membres de la combiantoire[^2]". La supériorité du peintre sur le rendu mécanique est éclatante; elle met en valeur le style et la composition [...] et affirme par conséquent la liberté de l'artiste en tant qu'homme sur la machine." (p.130)
 

  [^2]:J.-P. CRIQUI, G.A. TIBERGHIEN, *A propos de l'art de la tache. Entretien avec Jean-Claude Lebensztejn*, article cité, p.130-131 

 
 Les peintres (impressionistes ici), au fur et à mesure de leurs utilisation d'un medium (le miroir ici mais on pourra parler en mêmes termes de la photographie) ne s'en servent alors plus comme *mode de vision* que leurs caractéristiques visuelles[^2], plutôt une approche de la sensation du regard que des aspects purement techniques de la vision.
 

 
 
 ---
## Paysage souvenir

cf. Proust un peu plus tôt j'imagine 

>"si finalement nous n'éprouvons pas "plus de plaisir, à [nous] rappeler au moyen de quelques lignes tracées légèrement, les sites que nous avons admirés, que lousqnous en avons joui en nature[^5]"".(p.138)

[^5]:W. GILPIN, *Trois essais...*,p.45


## Miroir dépréciateur

### Lien potentiel avec le jv

>"Ils ont un aspect flou qui montre une chose tout en ne la montrant point, probablement dans l'idée d'en suggérer une troisième différente."[^6]
>
*(à propos des miroirs nouirs de Richter, n°160 du répertoire de son oeuvre)*

[^6]:G. RICHTER, *Textes*, trad. C. M2tais Bürhendt, dir. X. DOuroux, Les Presses du réel, Dijon, 1995, p.258
 
 Là on a peut être un lien avec le fait que les polygones loins soient hyper simplifiés, forme abstraite qui avec la lumière en formerait une autre idk?
 