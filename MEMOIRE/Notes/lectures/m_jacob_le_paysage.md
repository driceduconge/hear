# *Le paysage*, Michael Jakob, 2008, Infolio éditions
## Sommaire
Qui suit plus ou moins les différentes parties du livre

- [A landscape is a landscape is a landscape](#alandscape)
- [Définir le paysage](#definir)
- [Paysage = Sujet + Nature](#psn)
- [Le temps du paysage](#temps)
- [Re-connaissance](#reco)

## Définitions

---

### Le concept d'image-paysage {#image-paysage}

---

C'est un concept qui caractérise 

>le paysage d'évasion, le paysage carte-postale, le paysage onirique [...] 

p. 14
un paysage par conséquent reproductible, qui va donc être diffusé à un plus grand nombre et donc influer un paradigme, des attentes paysagères. On pense au travail d'__Anne Cauquelin__. Mais aussi M.J. parle de l'expérience qu'a eu Dazai Osamu en voyant le Mt. Fuji en vrai, comme une déception face à l'image qu'il s'en était fait à force d'estampes etc. Michael Jakob se dit que peut être à force d'être transformé en image-paysage le Mt. Fuji s'apparentait plus à un rêve qu'à une figure réelle.

>Le paysage est le _résultat hautement artificiel_ d'une culture qui redéfinit perpétuellement sa relation avec la nature.

p. 31

L'image-paysage est produite par la modalité du __cadrage__, du __découpage__.
Cela convoque donc inévitablement des notions de fond perdu, d'un regard qui s'étend.

Cette abondance de représentations picturales paysagères amène alors à une représentation _mentale_ de la représentation picturale. D'image on passe à image mentale. De paysage réel fixé sur peinture ou pellicule on aboutit à un paysage fictif, idéalisé par le biais de l'image-paysage.
>Les images-paysages s'inscrivent dans un système _rhétorico-narratif_, et c'est toujours à partir du récit que le bout de nature représenté acquiert son sens et sa raison d'être.

on ne parlerais alors plus d'images-paysages mais de __paysages-récits__.

De manière sonore pourrait-on parler d'un audio-paysage ?

---

### Paysage authentique

---

>Authentique, ce serait un paysage donné à un individu [...], un bout de nature découverte et non reconnue.[...] Il impliquerait en outre la _présence synesthésique de tous les sens au lieu de la soumission béate au diktat de l'oeil et à la sémantique de la vision_.

p. 14
 
Le paysage authentique se différencie donc de [l'image-paysage](#image-paysage) de par le fait qu'il est non-reproductible, inhabituel et à durée réduite.
Plus loin pour corroborer ceci M.J. parle du paysage authentique (réel) comme "chose en soi" 

---

## A landscape is a landscape is a landscape...{#alandscape}

Artialisation :

>Le pays ne se transformerait en paysage que sous la domination de l'art
 
 Alain Roger (à lire)
 
 Le paysage serait donc selon Alain Roger (je parle sans avoir lu encore), non pas une sensation esthétique mais seulement créée par le regard du peintre par ex.
Il est plus loin corroboré par une citation du Marquis de Girardin :

>Le long des grands chemins, [...] on ne voit que du pays : mais un paysage, une scène poétique est une situation _choisie_ ou créée par le goût et le _sentiment_.

Le paysage ne serait donc pas créé par des sensations, mais par un _sentiment_ et un _goût_, et là les détenteurs du goût appartiennent à une certaine classe sociale, à un certain groupe (alors les bourgeois). Plus loin  __Denis Cosgrove__ réactualise ce postulat en précisant :

>Le paysage représente une façon de voir - une façon par laquelle certains européens ont représentés à eux-mêmes et à d'autres le monde qui les entour et leur relation à celui-ci [...]

D. Cosgrove cité en p.19, plus tard il reprochera a sa théorie de ne pas avoir pris en compte des aspects non-visuels, genre, colonial etc.

---

De par ces considérations, ainsi que celles que j'ai pu lire dans d'autres ouvrages (J. Balibar, A. Cauquelin entre autres) le paysage reste polysémique, même dans le champ restreint de la philosophie du paysage, où les débats sur ce qu'est le paysage ne sont pas clos. On préférera alors dans le mémoire parler plutôt du paysage comme d'une _sensation esthétique_.

---

## Définir le paysage{#definir}

Nous poursuivons donc sur la thématique de la définition de ce terme, pour continuer à définir le paysage, M. Jakob dit d'abord ce qu'il n'est pas :

>ni mesurable ou identifiable [...] ni le territoire, ni le pays, ni le site **(p. 29)**



le paysage serait aussi :

>L'expérience d'un morceau d'espace perçu d'un coup **(p.32)**


problème ici : ce "coup" n'est pas défini, est-ce que c'est une seconde (je pense donc à un instantané donc, un paysage silencieux comme m'en a parlé Elsa dS), une durée ultra compressée, équivalente à l'obturation d'un reflex qui fait office de rapport sensoriel avec l'environnement ? Le paysage est-ce que ce n'est pas aussi en quelque sorte passer un certain temps, un temps de _contemplation_ d'une portion d'espace à portée de senseurs ?

## Paysage = Sujet + nature {#psn}

Pour M. Jakob donc "pas de paysage sans nature" **(p. 34)**. Et il agit vraiment ici en tant qu'historien du paysage (_c'est en partie son métier_) où il reprend un peu les poncifs que nous rappellent la peinture de Caspar David Friedrich[^1], *Le Voyageur au-dessus de la mer de nuages*

  <figure><img width="350" src="../../BiblioIconoSono/Images/Caspar_David_Friedrich_-_Wanderer_above_the_sea_of_fog.jpg"></img><figcaption>Caspar David Friedrich, *Le Voyageur au-dessus de la mer de nuages*, Huile sur toile, 1818</figcaption></figure>

Là l'on retrouve donc ce qui fera une grande partie de la peinture paysagère (évoqué probablement plus tard dans cette lecture). Mais ausi on retrouve la théorie P = S+N de Jakob qui s'appliquerait ici parfaitement.

>Le sujet se découvre en tant qu'autre du monde **(p.3X)**

Mais il émet lui-même ensuite des critique quant à ça.

>Le tableau, de par sa nature même, est _non narratif_, statique, fixe : il feint un point d'arrêt, une halte que le spectateur doit lui aussi retrouver et créer [...]
>
>[cela] exige le contrôle, la maîtrise de l'espace. [Avec] un refoulement massif de l'autre logique, corporelle, celle d'un sujet qui se déplace. __(p.41)__

cela amène donc à penser à l'intérieur de ce P = S+N quelles relations convoque S+N.

### La relation Sujet-Nature
>Récupérer la nature signifie constuire une relation sur la base de la perte, de la _non appartenance même_.
>
>[La nature] devient image, [...] sutout une image d'ensemble **(p. 48)**

On a perdu prise avec la nature, si elle existe encore que par autre chose que le sublime ? Donc le paysage ferait office de pont.
#### Quelques termes
Nature
: unité illimitée

Paysage
: limitation "l'être pris dans une perspective éphémère ou durable"

Ici je trouve ça plus judicieux, de définir le paysage _par rapport au sujet en faisant l'expérience_. On note l'utilisation "d'être pris dans".

M. J. reste dans une perspective très liée au travail d'architecte paysagiste que le paysage est donc -> artialisé et permet de récupérer une relation avec la nature toujours. Pour lui le paysage est donc la réponse à un besoin, un désir de récupérer la nature.

>C'est par la voie esthétique que le sujet récupère la nature, en se l'appropriant au travers des représentations __(p. 49)

Mais ne serait-ce pas aussi une _maniarère de percevoir_ ce qui est extérieur à soi ?
## Le temps du paysage {#temps}

>Selon Yi Fu Tuan[^2], le paysage n'est en fin de compte rien que l'impulsion du temps dans l'espace.

>>Chaque tableau représentant un paysage en perspective et chaque photographie nous apprend à voir le temps 'traverser' l'espace.

Le paysage se débarasse en même temps progressivement de la figure humaine, puisqu'elle est convoquée par les temps qu'à traversé le paysage. Ces temps, synonymes de conquêtes, sentiers, champs, terraformations en tout genres ont laissé la marque de l'homme sur la nature. M. J. apelle ça du "pré-texte".

il y a là un bref encart sur le sublime, que je préfère éviter dans le mémoire, c'est déjà assez vaste ainsi.

Avec le temps donc on s'éloigne aussi peu à peu du paysage réel peint pour _inventer du paysage_. Car au final le paysage n'est qu'un assemblage d'éléments dont 

 >"l'effet de synthèse [...] d'éléments distincts est dû principalement à la technique __(p. 67)__
 
 On cherche alors à supprimer ce qui fait habituellement paysage et là Monet réalise un tour de force  : __il abandonne l'horizon au profit de la série__. Et ainsi, il abandone le point de vue fixe paysager et fait opérer, au travers de la série, un déplacement à l'intérieur du paysage réel[^3] de son jardin.
 
<figure><img width="400" src="../../BiblioIconoSono/Images/Screenshot 2022-01-26 at 17-10-30 Les Nymphéas de Claude Monet Musée de l'Orangerie.png"></img><figcaption>Quelques _Nymphéas_, Claude Monet,  1914-1926, [site du musée de l'Orangerie](https://www.musee-orangerie.fr/fr/collection/les-nympheas-de-claude-monet)</figcaption></figure>
 
 >La relation sujet-nature à l'oeuvre dans le genre paysage se révèle à cet égard comme une relation profondément __asymétrique__: c'est au point de vue humain de fournir le __cadre__, le __découpage__ et même le [placement du] point de vue **(p. 87)**
 
Et donc une dramatisation, une mise en scène et à distance feraient partie de "l'essence même du paysage".

Je suis pas très d'accord avec ça, oui il y a dramatisation dans le sens de créer de la narration à travers le paysage, il y a peut être mise en scène etc. Mais pas forcément tout le temps de la mise à distance, beaucoup de pièces tentent de confondre paysage et environnement direct, où l'on expérimente une forme de _proprioception_.

## Comme un vertige

>La rencontre entre le sujet et la natureconnaît, court-circuitée par la surabondacedes donées visuelles, un échec; le spectateur se trouve dans l'impossibilité d'organiser le regard et de se aire une image de ce qui dépasse ses capacités cognitives et imaginatives.
>**(p. 91)**
>
>Tout n'est pour le voyageur, et même pour le promeneur en quête de nature qu'un 'film' défilant de manière trop rapide devant les yeux[^4].
>**(p. 93)**

Le cadrage (des yeux ou autre appareil de fixation de l'image) est insuffisant et quand l'on se déplace on crée une succession de micro-paysages pour former une impression, la rémanence d'un autre. (cf. compte rendu de la lecture du miroir noir et de M. Desportes).

## De la re-connaissance des paysages {#reco}


> la rencontre visuelle avec la nature advient seulement là où __l'imagination__ du sujet entre en jeu, afin de __'vivifier'__ le matériau brut livré par les sens et le transformer en paysage, source de plaisir esthéthique **(p. 105)**

Le pittoresque n'est pas un paysage qui transcende (tel que le sublime) le pittoresque est un paysage _attendu, re-connu_, qui a tout de même une légère notion de surprise dans le sens où l'on retrouve des __composantes attendues__ représentative d'une typologie de paysage, mais leur agencement réel (__combinatoire__) est inattendu.

__Le pittoresque, produit par l'imaginaire est donc une source de paysages virtuellement infinie.__

Le pittoresque a donné naissance à ce qu'on a appelé plus tôt l'[image paysage](#image-paysage), que le domaine des arts appliqués a très tôt investie notamment le papier peint :

> sans devoir quitter sa propre habitation et seulement en regardant autour de soir, un homme actif qui lit les guides et récits de voyages peut croire se trouver [à ces endroits]
> 
> __Thümmler, 1802, fabricant de papiers peints de vues de Suisse__

>L'image ira jusqu'à remplacer le réel (le panorama, l'illustration + véridique que son référent) __(p. 120)__

## La différence

>Les 'paysages' de Coleridge [poète] sont ainsi toujours entre le situable et l'insituable, ou mieux : on les _entrevoit_ plutôt dans les *intervalles* de sa prose que dans le *flux* des mots.  **(p. 123)**

(en parlant de la voiture mais peut ê applicable à l'ordi)

>[...] le spectateur se retrouve désormais _dans la machine_ qui le fait voir différemment - allie la continuité du trajet à la _discontinuité_ de la perspective. **(p. 133)**

On retrouve les notions de paysage-film[^4].

>La séquence métamorphique d'images qui se transforment sans cesse _l' une dans l'autre_.

[^1]: _Caspar Friedrich and the Subject of Landscape_, L. Koerna
[^2]:  _Space and Place. The perspective of experience_, Yi Fu Tuan 
[^3]: _Du paysage représenté au paysage réel_, Justine Balibar
[^4]: Je pense à _Paysages en mouvement_, Marc Desportes


