#Entretien Elsa de Smet

[1:00]
: le paysage comme uniquement lié au visuel

[2:30]
: sur les vues d'artistes "idéales" qui ne se détachent pas de la représentation classique

[5:59]
: des espaces inconnus (dans l'espace) imaginés silencieux, non-narratifs

[9:15]
: la vue n'est pas un sens restreint, on se projette par le visuel la peinture est un prolongement de l'espace réel vers l'imaginaire

[10:20]
: artialiser, restreindre le pays qui le rend paysage

[11:50]
: le paysage pour celui qui n'a pas vu permet de s'approprier par la portion le tout/l'idée par la cognition liée à la vue **important**

[14:15]
: on n'éprouve pas les paysages extra-terrestres mais à travers les captations on *pourrait* l'éprouver

[15:00]
: la montagne la mer etc ont été "inventés"

[19:15]
: diff entre vue d'artiste et "recomposition par l'ordinateur suivant un protocole" **pas mal**

[21:10]
: l'image vraie à travers le mur algorithmique

[] 