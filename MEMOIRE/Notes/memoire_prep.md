# Paysage{s} réels et imaginaires, sonores et visuels, feedbacks



## extrait commission

En quelles qualités ces deux composantes du pay-sage,  ne  feraient  pas  que  cohabiter  mais  s’influenceraient  mutuellement,  comment  elles  pourraient  faire  corps  ?  Le  paysage, non seulement deviendrait mouvant, et le sonore pourrait  permettre  de  le  tirer  hors  de  sa  bulle  picturale.  Comment alors comprendre le paysage sonore comme un visuel  et  vice-versa,  pour  en  creuser  leurs  interactions  ?  Les  potentialités  qu’Eliane  Radigue  retrouvait  dans  ses  sons  à  travers  plusieurs  modes  d’écoutes6  ne  rejoin-draient-elles  pas  les  potentialités  des  tableaux  de  Sou-ages  en  fonction  de  l’endroit  où  ils  seraient  posées7 ?  Le sonore et le visuel, alors deux éléments composants pour-raient  être  autant  valables  l’un  et  l’autre  comme  paysage  à part entière autant qu’ils feraient paysage  réunis ? Mais alors, comment pourrait-on re-faire paysage ? Est-ce  qu’il  suffirait  bêtement  de  reprendre  des  théories  usées  et  abusées,  datant  de  la  Renaissance,  de  perspec-tives d’arrières-plans qui sont des aberrations dès que l’on aborde le paysage réel8 ? Si l’on partait du postulat comme quoi  le  paysage  ne  se  construirait  pas  par  des  principes  purement  de  perspectives  au  sens  mathématique,  calcu-lables  depuis  une  perception  fixe,  mais  par  des  déplace-ments  dans  un  site,  de  perspectives  multiples,  autant  de  micropaysages qui s’imbriquant alors imperceptiblement, l’on  retrouverait  une  narration,  qui  serait  la  clé  de  ce  qu’on appellerait un paysage ? Le paysage recréé ne serait alors  plus  un  simple  artefact,  mais  pourrait  devenir  un  agencement  d’éléments  créant  cohérence(fig  eventhori-zon). Se réduirait-il alors à une suite d’images et de sons collés, ou encore faudrait-il passer par le biais de la nar-ration environnementale9 pour permettre de faire passer dans un paysage virtuel fabriqué la praxis présente habi-tuellement  dans  le  paysage  réel  ?  On  pourrait  alors  croi-ser  les  mediums,  les  manières  de  narrer  un  paysage,  peut-être le fragmenter pour mieux le coller, ou bien permettre à l’autre de se le raconter soi-même, en lui laissant mettre  bout  à  bout  des  mots,  des  images  et  des  sons  ,  de  les laisser faire écho ? On pourrait ainsi fluctuer entre les plans, prendre à corps le paysage dans sa multiplicité. Ma démarche  se  constituera  ainsi  d’allers-retours,  d’échos  et  de larsens, qui feront le principe même de la construction du  mémoire  en  tant  qu’objet,  regroupant  des  références  audios, visuelles et textuelles.

# Méthodologie

  un site stylo pour la rédaction + tard ?


# Principes dans le mémoire

### 

description des sons et des images qui vient par dessus le son, l'image

# Général

## Structures etc

### Grand Axe

> Percevoir et faire percevoir

> Synesthésie et narration à travers les croisements des paysages sonores et visuels

> Les interactions entre paysages sonores et visuels à l'ère numérique

> De la captation et de l'artificialisation des paysages ( à l'ère numérique )

### Grand axe desc

  Tout au long du mémoire, il s'agira de faire entrer en résonance les paysages visuels et sonores et d'étudier comment ces derniers, au fil de leur évolution, ont fini par se retrouver fusionnés par des dispositifs analogiques ainsi que numériques. On observera quels effets cette juxtaposition a produit sur la manière de les aborder. Il sera en effet question non pas de traiter le son et l'image séparément, mais de tenter de voir où sont les zones sensibles, synesthétiques où les deux types de paysages se retrouvent et d'en déduire des causes socio-techniques, des résultantes narratives. On envisagera aussi les relations qui se tissent alors avec le paysage réel, qui est par définition multi-sensoriel et donc insaisissable dans son entièreté. L'on opérera un glissement des perceptions paysagères dûes aux nouvelles techniques vers les nouvelles possibilités d'artifialimagination qu'elles créent. Tout cela dans un constant larsen entre réel et imaginaire, son et lumière.
  

### Axes

> intro avec un balayage de ce que sont des paysages ? et qu'on va essayer de les re-définir
>           - on mettra en place la dichotomie paysage réel/ paysage représenté qui va courir tout du long du mémoire 

         <intro>le paysage (représenté) est avant tout une affaire de cadrage.</intro>

> potentialité(es) narratives des paysages sonores et visuels
>                   
> quel est leur rapport avec l'environnement réel

> leur rapport avec la technique (numérique) qui les génère

> NOTION DE DERIVE -->> répétition inlassable de presque la même image, avec de subtiles différences qu'est ce que ça crée? une série? un déplacement?

> Un paysage est-il un espace sonore/visuel régi par un set de règles quand à sa construction ainsi qu'à sa perception

> > partir sur une généralisation du paysage en grand I a. visu b. sonore puis redéfinir ce que c'est pour des dispositifs numériques en II a. visu b. sonore et faire un III interactions etc ? 

### axes II

##suite

### Questions ?

> quelles définitions du paysage utilise-t-on comme point d'essor (acheter biblio)

> paysages sonores et visuels, issus de la praxis

> modularité des paysages

> représentation multiple, création d'ambiances
>   (donc le jeu vidéo, qui combine visu et sono afin de former des paysages crédibles, et les influences par action (praxis) du joueur)

> sons, images & temps 

/!\ Penser dichotomie son/musique /!\

### refs vrac

> sandstorm daniel linssen
> le sommet des dieux incroyable immense ref son image aussi philo? => lire le mangasse
> John Taggart, Rothko's Chapel

### idées

> les guides de voyages, les photos dedans? ça peut être intéressant de voir comment le paysage y est traité => mm le projet plastique ça peut être un guide de voyage rêvé?????????????? !!!!!!!!! BOF
> les feux d'artifices chinois??? paysage????  BOF

### Refs formelles

un glossaire ? pour le côté non-linéaire

On the road against, approche geographiste de la ségrégation sur les routes américaines, Pauline Desombre

dans un mémoire avec une structure chelou (ex: poeme) faut y aller à fond sinon ça marche pas
no tiédeur no!
att c risqué

!!!!!!!! des didascalies !!!!!!!!!!!!!!!
pour lancer les sons etc
et en fait dans ces didascalies on retrouverait des indications du type  "arrêter la lecture" ou encore "aller à la timestamp 12:03" etc et ce serait contextualisé dans le propos !!!
genre le texte s'appuierait en partie sur la manipulation du son?

les images ?
est ce qu'elles viennent se loger dans le flux du texte (en grand)
en page à côté ?
ou en mini livret parallèle par ex. ?

# 

#Notes

### point de vue et d'écoute

  pdv fixe => pdv mouvant
  environnement stable et instable

  Tout paysage part d'une position dans un espace, une mise en situation de soi par rapport à l'environnement. Le point depuis lequel le paysage va être perçu va varier selon plusieurs facteurs: l'angle et la hauteur de la tête, ainsi que les acuités sensorielles. Si le premier est assez évident, (on ne verra pas la même chose que l'on soit debout, assis ou couché par exemple), le second n'est pas forcément intuitif. Un.e malentendant.e ne percevra pas toutes les fréquences ou encore certains sons distants et par conséquent se consacrera peut être sur des sons plus proches de son spectre d'audition, percevant alors avec plus de finesse un son que quelqu'un avec une ouïe parfaite pourrait négliger. Il en va ainsi pour la vue, certains jeux d'ombres, de couleurs etc ne seront alors pas perçus de la même façon par l'un.e ou l'autre.

  Les mêmes considérations que l'on a dans une situation de paysage réel s'appliquent alors quand on le capte ou l'artificialise. C'est là qu'on cherchera à trouver de la justesse dans le calibrage, la composition des paysages, de quelles intentions on va charger les niveaux de gris, de lumière et de volume. La même chose que pour un paysage réel, à quel niveau se situe le regard, le micro, la mise au point, le focus.

Les instruments de captations vont aussi faire varier de par leur composante technologiques la fabrication du paysage.
-> spectre de freq, grain, objectif...
->
  

### rapport paysager

d

bonne ressource: 
    http://www.arch.uth.gr/urbanlandscapesinvideogames/index/soundclouds

  Les jeux vidéos peuvent-ils se doter d'un espace sonore paysager ?
      => probablement
  il faut déjà dire que dans le jeu vidéo il y a plusieurs types de sons:
      - les musiques
      
        elles sont là tout du long du jeu, comme au cours d'un film, afin d'en ponctuer le parcours. Elles servent en premier lieu à donner le ton d'une scène, d'un espace. Plus anciennement dans par exemple Super Mario Bros. ou Tetris on retrouve une musique qui est purement là pour donner le ton, le rythme de jeu demandé à l'instant. (la musique qui accélère dans Mario dans les châteaux de Bowser ou qui accélère avec le rythme du jeu dans tetris), ici elle ne donne que très peu la "couleur" d'une ambiance particulière. Pourtant, dans les château de Bowser on retrouve quand même quelque chose de plus inquiétant, stressant: directement lié à ce que le joueur est /sensé/ éprouver lors de ce niveau; c'est une fin de monde, un moment particulièrement difficile avec un boss au bout du couloir, il est facile de perdre des vies et donc de toucher au game over. La musique s'adresse alors principalement au joueur, accentuant sa situation de joueur. Il n'y a pas de /rapport paysager/ à ces sons, la notion d'extérieur est écrasée par la situation mentale du joueur. On retrouve encore ces musiques /d'intention/ dans des jeux de nos jours, où l'emphase est plus mise sur un gameplay et une notion de pression vidéoludique que sur une immersion dans un monde.

        Dans d'autres cas on va justement retrouver une approche plus /dynamique/ de la musique: elle va servir à guider le joueur au travers de situations, lui faire ressentir plus fortement le monde en certaines situations. On va vouloir l'immerger dans le monde du jeu de manière ponctuelle, intense, tout en créant des situations sonores extra-diégétiques (la diff. théorique est pas encore claire ici). On retrouve ici l'exemple de Skyrim où, peu avant une situation de combat, lorsqu'on "capte l'aggro" [ càd qu'un PNJ nous prend pour cible et passe en mode offensif, après qu'on l'aie attaqué ou simplement car le PNJ nous est hostile par prédéfinition ] la musique va soudainement changer pour indiquer, à grand renfort de percussions et de violon(celles etc)s. Lorsque cette musique joue on est instantanément au courant de la situation dans laquelle se trouve notre personnage, c'est l'équivalent d'un tocsin. Dans le même jeu on retrouvera dès lors que l'on poussera un cri de dragon, une musique chorale aux influences nordiques/chrétiennes européennes qui scandent le statut du personnage "Dovahkinn" se jouera en premier plan, renforçant le statut sacré (pour les autres personnages du jeu) de l'action du joueur (alors que c'est seulement lui qui "perçoit" cette musique). La musique est alors liée directement au comportement, les actions, la praxis du personnage joueur.

        Plus souvent, on va retrouver des musiques que je qualifierai plutôt /d'ambiantes/, en effet on retrouvera souvent des musiques qui vont décrire la "couleur" générale de l'espace dans lequel se trouve le joueur, on retrouvera souvent des clichés tels que la musique calme et introspective au bord d'un lac ou d'une cascade, nerveuse dans une ville, inquiétante dans une cave... mais bien sûr cela changera en fonction du contexte, et ces musiques souvent répètent et amplifient la direction donnée par le visuel, sans pour autant passer dans le premier plan de la description de la vue donnée. On ne retrouvera pas dans ces dernières des sons typiques des vironnants mais plutôt des interprétations conceptuelles de ceux-ci. Dans ces mêmes musiques ambiantes on retrouvera aussi dans énormément de jeux des musiques qui ne chargent pas l'environnement d'une charge émotionnelle, mais plutôt qui vient charger le joueur d'une autre couche d'affect. Ces musiques-ci contribuent plutôt à la construction d'une ambiance globale et peuvent être parfaitement indépendantes de l'endroit où elles sont jouées. Elles sont souvent coupées par les musiques contextuelles et opèrent avec ces dernières un fade-out très court, coupure quasi-net, avec un fade-in beaucoup plus lent, le temps que la contextuelle retombe.
        
        Bien sûr rien de tout ça n'est exhaustif.
        
      - les sons non joueur
      
      - les sons joueur

---

#Vieux trucs

### tentative introduction I

    
    Brouillard épais, murmures indéchiffrables, peut être aussi la mer qui lèche doucement la plage sous l'aube. On est proche de deux voitures stationnées, Twingos.
*
On devinerait ///

### milieu intro vrac

La technique a transformé nos perceptions du paysage, nos manières de l'approcher, de le ressentir, le pratiquer, ainsi que le rapport profond que l'on entretient avec ces vues reconstituées.

*
(bof)
La vue des Alpes sur une boîte d'allumettes nous évoque-t-elle encore autre chose que purement ce qu'elle est: une image d'un paysage ? Il faudra trouver les sens de ces paysages produits en masse, répétés sans différence qui nous ont désensibilisés, anesthésiés. Il faudra récupérer ces morceaux choisis et recomposés afin de refaire percevoir le réel qu'il y a derrière, ou non. (bof)

### fin d'intro potentielle

Au fil de ce mémoire on s'échinera à trouver des connections, tendre les fils ténus entre son et image afin de progressivement, faire émerger à travers des propositions théoriques une nouvelle approche des paysages, plus en phase avec notre approche perceptuelle multi-écranique et surround. Il en résulterat des pistes sans fin ainsi que des impasses assûrément, mais il y aura eu le mérite d'essayer de se remettre en phase avec le sensible, ne serait-ce qu'un instant.

# 

---

# Structure II

voir fragments d'un discours amoureux, 
dans chaque catégorie des petites entrées de quelques pages qui scindent encore la catégorie générale.

aussi Myriam Suchet
au final pas vraiment 
mais je pense qu'il y a une vraie notion de dichotomie et de lien à la fois à creuser entre l'écrit, le son et l'image.

quand on parle de son une certaine typo puis une autre quand on parle d'image ?

### avant-propos?

  *struct du mem*
  

### Intro

- définition gén. du paysage, champs d'attaques ici etc.

- définitions du paysage par des praticien.ne.s (on intègre un cours portrait d'elleux dans la mep)
    
    
  
  ### Capter | (re)Transcrire

> Outils et techniques de captation, outils et techniques de représentation.
>   (première confrontation réel/capté car prisme des outils)
>   de ce que les choix (point de vue et d'écoute, techniques etc) vont créer dans le paysage.

  [cette partie sert de mise en place d'outils théoriques]
  + [exploration des zones de tension paysagère]
  
  Toshiya Tsunoda, Francisco Lopèz

### Mouvements_Déplacements

> on développerait de ce que le déplacement par rapport au réel et au représenté induit pour le visuel et pour le sonore, (puis pour les deux). (space within space) 
>  [Effet narratifs] 

### Audio+Video

> Où on aborderait les espaces où visuel et sonore se retrouvent ensemble, se juxtaposent, se superposent... on parlera pas mal des ordis et de jv...

### Ouverture

>  potentiellement sur l'espace entre la vue et l'ouïe... très flou jsp

## méthode old

chaque partie retrouvera, encarté au travers de la "rédaction" des extraits d'oeuvres sonores, des archives... et aussi des dialogues avec des praticien.nes qui permettent d'ouvrir sur des potentialités, des pistes de mise en pratique.

---

# Défs vrac

 **Analogique** :

 Qui représente une information par un rapport proportionnel et continu entre l'information initiale et sa représentation (opposé à numérique).

 **Numérique** :

 Se dit de la représentation de données, de grandeurs physiques sous forme de nombres (opposé à analogique), ainsi que des procédés utilisant ce mode de représentation.

---

# Structure III 12-Dec-2021

 Traiter du paysage, avec comme point d'appuis :

1. Les relations, interactions vue et ouïe
2. Les implications des outils et techniques au XXIe siècle
3. Le rapport intrinsèque du réel jusqu'à l'artificiel

Le mémoire ne sera pas : 

- une analyse enchaînée d'études de cas
- une réponse à la question du paysage
- une approche ultra-précise d'un pan particulier de l'étude paysagère
- un historique des représentations paysagères

Le mémoire sera(it) :

- un éventail de pistes possibles pour aborder le paysage au XXIe siècle
- partisan du flou, de l'instabilité de la définition de paysage
- une expérience sensible de la réflexion paysagère

*!!! L'intro se situe bien au début !!!
!!! les autres "parties" vont probablement se chevaucher à des moments donnés... surtout la 3 avec la 1 et la 2... !!!*

## "Intro" généralités

### paysage réel / paysage représenté

 un poncif des ouvrages paysagers est de travailler sur cette distinction, il est donc utile de re-définir ce qui pour nous relève du paysage et ce qui relève du réel et du représenté.

### Paysage visuel & sonore !

remettre en cause les théories qui n'utilisent comme point de départ que la vue, ou qui minimisent l'ouïe car "moins performante". Alors que la vue et l'ouïe sont des sens permettant des mesures esthétiques d'un espace et sont donc complémentaires dans la projection dans un paysage.

- Sound shaped vision, visually shaped sound
- aller-retour entre vue et ouïe

### paysage hors-nature

 où il sera nécessaire de définir le paysage comme autre chose qu'un espace naturel,
 mais plutôt comme un rapport esthétique (et  pratique pour le paysage réel) à tout espace "ouvert" et dans lequel on peut discriminer une variétés d'éléments participatifs. 

## Capter, ReTranscrire

### Capter, sentir

 On parlera ici des différents prismes sensoriels et techniques qui constituent d'une part l'expérience paysagère et d'une autre la possibilité d'enregistrement et de reproduction de ce dernier.

- Lo-Fi & Hi-Fi
  
  > Sentir
  > Schaffer le lo-fi et hi-fi dans le paysage sonore (et appliqué au visuel
  > Capter
  > puis technique lo-fi et hi-fi des techniques de captation et de restitution 

- Points de Perception, points de fuite
  
  > Sentir
  > globalement conséquence des points de vue et découte
  > glissement vers Capter
  > conséquence du calibrage de la sensibilité des outils (ex: dB, Ouverture...)

### ReTranscrire, artificialiser

- Quels sont les méthodes permettant de "faire paysage"
- Comment on remet en scène un paysage capté ?
- Comment on fabrique un paysage ? 
- Comment joue-t-on sur le percept, crée-t-on un espace dans lequel des *possibles* sont projetables ?

## Praxis

### Déplacer

- Le déplacement comme transfiguration du paysage
  pas mal de Balibar

- Le déplacement comme créateur de narration
  pas mal de Desportes

### Media praxiques

praxis : Activité codifiée, manière générique de penser la transformation de l'environnement.
medium : Support ; intermédiaire.

- La praxis comme influence larsénique sur le paysage

réel

représenté

- Le jeu vidéo comme expérience paysagère

## Du paysage analogique au paysage numérique

Une sorte de partie fleuve, qui coule entre Capter, retranscrire, artificialiser et Praxis...

### paysage analogique et numérique (tech)

- expression directe, avec un filtre -> analogique
- expression directe, avec bcp de filtres (2 min?) -> numérique

### l'ordinateur, machine à paysages

 je vois bien là une sorte de grande conclusion... feu d'artifice final

 où l'on fait fusionner tellement le son et l'image par le biais numérique, calculatoire qu'on en arrive
 à un "toucher" numérique, qui rend le paysage complètement palpable(esthétiquement parlant)  par l'illusion provoquée par l'enchaînement des données et des cycles processeurs...

**RETAPER LE PLAN** en prenant en compte Landscape as a Souvenir et mise à distance par le paysage, en recentrant ça sur l'outil numérique.

# RETAPAGE PLAN ENCORE

En fait on reste globalement sur la même chose, on reste axés sur la **perception paysagère** donc en se concentrant sur les deux modes d'appréhension du paysage (et un troisième en filigrane qui est lié aux deux autres) : *regarder* [et non pas voir] & *écouter* [et non pas entendre] (puis en même temps le mouvement, guidé par le regard et l'écoute, le mouvement *à l'intérieur même de ces perceptions*).

Donc il faudra présenter ce que c'est un paysage à l'aune du regard, de l'écoute. Et en même temps on place les enjeux du numérique quant à la fusion de ces deux prismes de percept, tout en traçant le larsen réel/reproduit.

Déjà il y a la question de la percept par l'humain du "réel", sans outils. Puis à travers des outils (captation/retransmission du réel). 
Puis enfin du changement de paradigme que provoque la création de paysage directement par l'outil (sans "source" réelle). 

## Questions à Yvan

un peu en train de me perdre ??

est ce que j'y gagnerais en clarté à découper comme ça :

- Regard & écoute (humain->réel)
- Captation Retransmission (humain->outil->réel)
- Artificialisation (humain->artificiel(machine))

ou plutôt des micros essais par concepts ? j'ai peur de perdre en clarté dans le fil du mémoire ? sur la modalité de la haine de la musique...

- introduction au paysage
- le paysage, silencieux (le temps)
- le sujet diapason ? / correspondances termes son/image
- paysage & atmosphère
- filtre (claude glass, instagram, lo/hi-pass)
- captation / retransmission (où l'outil transforme le réel)
- point(s) de perception(s) (de l'unique au multiple)
- ?les coquillages et les pierres (le paysage comme projection de l'esprit)
- déplacement dans le paysage (du réel au jv)
- le feedback son / image
- le 

retourner aux sources

# RDV YVAN 14.12

Pauline Oliveros , Deep Listening

poésure et peintrie

Roger Caillois "pierres"
Attila Faravelli
Dignes Les Bains - Paysage (voir si yvan a le contact)

# FLASH

Les pierres de Roger Caillois -> lire un paysage là où il ne serait pas
équivalent sonore -> les coquillages... voir si qqn travaille sur les coquillages


# Réflexion 02.02.22

Aborder le paysage comme une définition polysémique ok, ça on y est. Mais il faut se centrer sur la définition esthétique de paysage, que l'on reliera tout d'abord, comme le propose Rosario Assunto, au paysage réel. Cet espace ouvert et limité, infini et fini. Cette définition a la particularité de pouvoir référer à la fois aux particularités sonores et visuelles du paysage, ce qui nous permettra d'avoir une *base* de travail commune aux deux champs. Il sera alors plus simplement possible de les lier, d'effectuer des aller-retours entre les deux. 

On ne délaissera pas pour autant le paysage représenté, notamment car je ne crois pas en une "pureté" des sensations et impressions paysagères. On constatera que l'*aisthesis* paysagère se crée d'une part par une suite de percepts sensoriels émis et reçus (senseurs/actuateurs) mais aussi par des régimes d'attentions du regard et de l'écoute liés à un arrière-plan culturel, technique et social. On pourra alors continuer à développer les notions de sensations paysagères au travers du pan de la technique et d'essayer de comprendre comment l'on retrouve l'espace dans des représentations qui, physiquement, le suppriment. On étudiera un *transfert de sensations, d'impressions*, qui se créent de plusieurs manières virtuelles : simulation de profondeur et de mouvement notamment. On essaiera tout de même de rester le plus éloigné possible d'un régime perspectiviste du point de vue et d'écoute (ce dernier terme est d'ailleurs étrangement peu sémiologiquement lié au premier) en restant dans le regard et l'écoute qui amènent à un positionnement du sujet. On utilisera notamment le terme d"*audioposition* et de *mesure visuelle*.

Lors de la confrontation avec le numérique est ce que le paysage devient *ambiance* ?

def cnrtl
: C.− Emploi techn.
1. CIN., RADIO et TÉLÉV.
a) Atmosphère de la réalisation ou de la projection d'un film. (Giraud 1956). Ambiance sonore. ,Ensemble des bruitages créant l'illusion de la réalité. (Voyenne 1967) :

En Anglais : 
: (figuratively) The apparent mood felt in an environment.

# RDV LOÏC 4.02.22
Mémoire étudiantes :

- Anna Guignard (+ le proj plasti?)
- Julie Bassinot

Artistes


- Masaki Fujihata - Simultaneous Echoes (son et image !)
- Caroline Delieutraz - Deux Visions (notion de position ?)
- Grégoire Lauvin - Split Soundscape - Mesa Landscape 
	mail : greglauvin@gregoirelauvin.net
- N. Bailleul - Film dans jv (à contacter)
- David Claerbout - photo vidéo 

Locus sonus -> echoscape

#other artists

https://richardvijgen.nl/#architectureofradio

#Pour rdv

si je fais un truc décousu qui brosse de manière légère plusieurs aspects de l'écoute, du regard et du numérique au travers du prisme paysager est ce que c'est ok ?

Ou vaut-il mieux que je fasse un truc plus carré sur un sujet très précis (rappel la soutenance de mémoire que j'ai vu)

## Avant-propos ((again))


## Mix de notes extraites de cr

### Desportes 1
>La technique a joué un rôle de premier plan en créant de _multiples médiations_ qui s'interposent entre nous même et notre cadre de vie. **(p. 8)**
>
>les  _"paysages de la technique"_ [...] qui désignent [...] les regards induits par ces infrastructures sur le cadre qui les environne. **(p. 8)**

Ici on peut se dire en parallèle que les _environnements numériques_ sont porteurs eux-mêmes à la fois de regard (et d'écoute) sur un environnement (_médiation technique_) et à la fois producteurs d'environnement sur lesquels se portent ces percepts  ( _autopoïèse_[^1] ).

### Desportes 2
>Ce paysage [...] c'est une _succession de vues_, s'enchaînant selon des angles variés pour peu que la route serpente. D'où ces effets de découverte, de masque, d'aperçu, dus à un _horizon qui s'ouvre_, à un relief que l'on contourne, à des obstacles dépassés. [...] Le paysage automobile naît d'une _conjonction entre le site traversé et la conduite adoptée_. **(p. 240)**

Cela peut ramener à la narrativité d'un espace numérique : ce dernier est en général perçu à travers un écran et/ou des enceintes et l'on interagit avec au moyen de périphériques qui répondent à des mouvements _signifiants_, la balade de la souris, le mouvement circulaire d'un potentiomètre... On est à distance de la réalité de l'espace numérique mais on en perçoit une fraction (la fenêtre, le son transformé) à travers l'outil médiateur. 
L'on se crée une narration par le biais de _l'interaction_ et des effets résultants sur la partie perceptible. (par exemple nivellement d'un filtre passe-bas, déplacement dans une vue 3D...). L'on reste immobile mais l'on opère des formes de _déplacements_.

[^1]: L'autopoïèse (du grec auto soi-même, et poièsis production, création) est la propriété d'un système de se produire lui-même, en permanence et en interaction avec son environnement, et ainsi de maintenir son organisation (structure) malgré son changement de composants (matériaux). [Wikipedia](https://www.wikiwand.com/fr/Autopo%C3%AF%C3%A8se#/)


### Jean Luc Nancy 1

Voir & Regarder | Entendre & Ecouter

Le son est plus lent que la lumière, la lumière est _instantanée_, le son _va-et-vient_, il remplit l'espace que la lumière accable.

> le sentir (_l'aisthesis_) est toujours un ressentir, c'est à dire un _se-sentir-sentir_ : ou bien si l'on préfère, _le sentir est sujet_, ou il ne sent pas. **(p.23)**

>[...] le visuel serait tendanciellement mimétique, et le sonore tendanciellement méthexique[^8] **(p.27)**

[^8]: En cosmogonie, le terme de métaxie concerne toute chose intermédiaire variant comme moyen terme entre des extrêmes invariables. On aperçoit par cela qu'une indéfinité de réalités intermédiaires, relatives entre elles, participent des extrêmes opposées en tant que constitutions mixtes. *Exemple*: apprendre est métaxie entre l'ignorance et le savoir.

>Mais l'enjeu d'un travail sur les sens et les qualités sensibles est nécessairement celui d'un empirisme par lequel on tente une _conversion de l'expérience en condition à-priori de possibilité_... **(p. 28)**

### Jean Luc Nancy 2

Ecouter, ce n'est pas seulement entendre ce qu'il y a dans l'espace, c'est _entendre l'espace_, c'est tout à la fois prendre partie à un récit que d'en être le·a spectateur·ice. Ecouter, c'est autant _faire_ que _entendre faire_, passif et actif.

>[...] c'est être _en même temps_ au dehors et au dedans, être ouvert _du_ dehors et _du_ dedans, de l'un à l'autre et donc de l'un en l'autre. **(p.33)**

>La présence visuelle est _déjà là_ disponible avant que je la voie, la présence sonore _arrive_ : elle comporte une _attaque_ **(p.34)**

Il omet peut être la notion de déplacement, où tout autant qu'acoustiquement que visuellement, des choses apparaissent et disparaissent, _viennent vers nous_.

>Là où la présence visible ou tactile se tient dans un "en même temps" _immobile_, la présence sonore est un "en même temps" _essentiellement mobile_, vibrant de l'_aller-retour entre la source et l'oreille_, à travers l'espace ouvert. **(p. 36)**

### Jean Luc Nancy 3

>Là où la présence visible ou tactile se tient dans un "en même temps" _immobile_, la présence sonore est un "en même temps" _essentiellement mobile_, vibrant de l'_aller-retour entre la source et l'oreille_, à travers l'espace ouvert. **(p. 36)**

Le son crée le temps, rend le visuel _tactile_[^2], car la vue prend alors vie, on s'éloigne du paysage silencieux - un silence de mort - pour insuffler le mouvement depuis un point de perception fixe. Si l'espace visuel immobile est considéré commez _zone morte_ alors la simple ouverture à l'écoute induit des échelles, des _déplacements_, des _variations_, fade-in & out d'une voiture, bourdon de la rivière, évènement d'un bang quelques rues plus loin...

[^2]: Edith Wyschogrod, _Doing before hearing : on the primacy of touch_, dans _Textes pour Emmanuel Levinas_, Paris, Jean-Michel Place, 1980

>Le "silence" en effet doit ici s'_entendre_ non pas comme une privation mais comme une _disposition de résonance_ **(p. 44)**

Si le son agit comme jalon temporel on reprend les mots de Pierre Schaeffer : 

>La seule introduction possible du langage dans la musique est celle des conjonctions [mais, où, est, donc, or, ni, car]

Si le son est conjonction, le visuel est les noms et le sujet est lui-même, performant les verbes. Il y a donc bien une sorte de _dynamique_ son et image, composer un paysage en omettant le son reviendrait à lui enlever des _connecteurs_, ce qui ne va pas forcément ruiner la phrase "J'aime les chaussures elles sont belles" mais lui enlever une composante fine de cette dernière "J'aime les chaussures, or elles sont belles", "car elles sont belles". Je m'avancerai et dirait que le sonore est de l'ordre de la _ponctuation et de la diacritique_, le son souligne, sur-signifie, transforme, retarde, rapproche, _respire_.

>Comme ce n'est pas par un acte distinct de l'esprit que nous composons l'idée du timbre, il n'est peut-être pas juste de l'appeler perception ; en percevant le timbre, nous ne mesurons rien. [...] L'intensité et le timbre sont des sensations immédiates, dans lesquelles nous ne pouvons remarquer de complexité qu'en nous servant de l'analyse extérieur.[^3]

[^3]: Lagneau, _Célèbres leçons et fragments_, Paris, PUF, 1964, p. 200

### Arnaud Maillet 1

>"[...] car un tableau ne représente pas une image vue à travers un oeil : l'objet réel, tout comme l'objet peint, sont tous deux objets du regard. Un tableau n'est pas image de la vision. [...] Si la vue est toujours vue de quelque chose, elle n'est pas vue d'elle même." **(p.80)**

### Arnaud Maillet 2

>"[...] la nature reflétée par le miroir convexe noir est toujours fictive et incomplète, c'est-à-dire en défaut par rapport à la nature elle-même, parce que reflétée (aspect mécanique) et réduite (aspect fragmentaire) par ce miroir. Cependant cette double imperfection du reflet de la nature tendrait alors à être [...] idéalisée par le miroir lui-même [...] le miroir tend à abstraire." **(p.128)**

> [L'art] "est fictif quant à la vérité et [...] incomplet quant à la ressemblance". 
>
>**(source inconnue, page 128)**

### Arnaud Maillet 3

e Claude Glass/Claude Mirror qui reste en filigrane tout au long du livre comme non pas le miroir noir, mais une certaine approche du paysage. Où on applique une sorte de filtre à ce que l'on observe en regardant à travers une *glace* aux différentes teintes. On colore ainsi l'espace observé.  

>"Gilpin regrette que la vue qu'il a par la fenêtre d'un ermitage en Ecosse [...] se fasse à traversdes carreaux de fenêtres qui sont en partie composés de verres rouges et verts.[...] ces carreaux offrent un noubel et surprenant effet, transformant "l'eau en une cataracte de feu, ou en cascade de liquide vert-de-gris"[...]. *
	
Ces verres colorés sont certainements amusants, mais ils seraient mieux, écrit 

>Gilpin, *accrochés dans des cadres avec des poignées afin de les utiliser selon son bon plaisir, que d'être fixés ainsi en permanence, imposant au spectateur leur constante coloration.*


# rdv 4 05

le paysage imaginaire -> mater Cage et aussi Satie (musique non-narrative)
julius - sound cooking

aller voir palais de tokyo mimosa

roger caillois

glose sur john cage non finito

les rapports non logiques par ""sympathie" (ex. paillasson somnambule bulletin)

paréidolie

bruit blanc

poésie spatialiste pierre garnier
di campo

poésie concréte ubuweb

pattern poetry

# pres memmoire

regarder godard

écouter truc jslb

## 21 SEPTEMBRE 22

PENSEURS MEDIEVAUX DE LATMOSPHERE ?

LIVRES NUAGES

LA MONTE YOUNG DREAM HOUSE

JUNG -> incertitude (dans les analyses de rêve)

von fransz conte de fées

 Hugo Ball 
 
 henri chopin

dream machine brion

invocations pour partie III

glossolalies

françoise bonardel - philosophie vision

bachelard