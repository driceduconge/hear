# VONSOLE ESOLANG


## How it works

### MATRIX MANIPULATING

Two matrices, multiples of 8.
One poiting to a document.
One pointing to the password.*
to access a document you need its adress + password.
 
 `[[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], 
   [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]`
  
  
 ###  INSTRUCTIONS
  USING BASE 16.
  
  eval CTRL+ENTREE
  
  - ROW   is   rw[numberInHex] 
  - COLMN is   cl[numberInHex]
  - SHFT1 is   (opt 3 5 7)* ** ***-> (l/d) <-*** ** * (r/u)
  - SHFT2 is   (opt 4 6 8)* ** ***->> (left/down) <<-* ** *** (right/up) 
  - ONE   is   :
  - ZERO  is   .
  - COPY  is   =
  
 example of seq:
 .................
 >  rw[a]**->
 >  cl[2]***->>
 >  rw[a] = rw[3]
 >  cl[c] .
 =================
 
 symboles = "->" | "->>" | "<-" | "<<-" | "." | ":" | "=" ;
 operands = "/row[\d+]|col[\d+]/g" ;
 
  since matrixes work on a binary logic ; maybe translate them
  -> it means something, it is an adress as well as a graphical logic
  
  1001 1101 ==  157 <=> 9D
   base 2       b10     16
   
  ### docs
  -> we could do things such as give the address of a doc:
  9D|32|FE|0A ...
  
  -> rather give the @dd as smth more cryptic but usable
     : [0,1,0,0,0,1,0,0]
       [1,0,1,0,0,0,0,1]
       [1,0,0,0,1,0,0,0]
       [0,0,1,1,1,1,1,1]
       [1,1,0,0,1,0,1,1]
       
     : 0100 0100
       1010 0001
       1000 1000
       0011 1111
       1100 1011
       
     : 4    4
       a    1
       8    8 
       3    f
       c    b
       
     :@44.a1.88.3f.cb
 
  ### password
  
  -> the password would be  for certain objects.
  
  
  
  
# coding this

1 make all of the design
2 set up the grid system + manipulation + correct or not! 
3 make a parse/eval for language
4 link grid + language
5 file system
6 narration (command then grid -> then filedoc etc.)

## grid system

make a grid_valid class (@DDRESS):
-> turns @DDRESS in a matrix (list[ list[] ])
fait

make a grid_matrix:
-> grid with each cell two states (0 or 1) displayed and stored in real time
fait 

-> code each manipulation as a func. 
                 |grid_shift1 (rwOrcl, number, degré)
                 |grid_shift2 (rwOrcl, number, degré)
                 |grid_one    (rwOrcl, number)
                 |grid_zero   (rwOrcl, number)
                 |grid_copy   (source, dest, [opt]) 

fait !

-> compare each time grid with every @ADDR (every frame ??)
fait

--> si j'ai le temps animation
trop chiant, à voir à la fin final     q

## parse eval
on est en full js => parse done manque plus que la pré eval et l'eval
penser à faire les erreurs out of range et invalid chaining.
fait

## link grid + language
fait
eval_output : func and params


##file system + design

refaire le design,
tout mettre dans une fenêtre (voir capture).
donc pas à avoir les ipcRenderer etc.
mais garder les fichiers .js séparés
https://stackoverflow.com/questions/41255861/how-to-pass-variable-from-one-javascript-to-another-javascript-file
faiitttt

RUNIC BASED DESIGN

exploration 

mix entre journal de bord & desc. des runes à la caillois
-on mentionne casu où elle a été aperçue (dans le flux du texte ou non) on leur donne un petit nom etc.

un peu un journal intime d'un illuminé complet qui a l'impression que des motifs lui parlent. Le mec est complètement obnubilé par ça et a une appétence pour l'architecture et les réseaux.

aesthetic//noise on everything


aesthetic//dither on the runes
https://www.shadertoy.com/view/tscBz7



#Graphic Design

---
  ## Soft
   
  ### Documents Vis
   
  What do they consist of ?
 Pictures and audios ?
 analyses of a territory with weird properties. (maybe throwback to spec.orch.)
 
 ### Password Vis
 
 No  != Maybe on the led screen print out the HEX of the matrix.

-> maybe a separate app working with a transfer protocol.

--- 
 
 ## Guidebook ?

### General overview
 
 The guidebook is full of notes of development, also a lot about the travel.
 Maybe use an already existing book (Aleph) ?
 use already existing photos.
 
layout: 
- L'atelier du Pic, Abraham Poincheval, DG Mathias Schweizer

un seul exemplaire ?

since we are gonna make things overlap maybe add a sense of finding stuff inside ?

### Façonnage

reliure spirale ou archive ou classeur.
Papier couché

---

# Installation

### Materials

- One computer 
- (Two screens linked)  *later*
- One book
- Two lamps pointing at the book
