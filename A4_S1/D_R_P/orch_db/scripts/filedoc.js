"use strict";
function readTextFile(file)
{
  // Create an XMLHttpRequest object
  const xhttp = new XMLHttpRequest();
  // Define a callback function
xhttp.onload = function() {
  // Here you can use the Data
  var displayer = document.getElementById('displayer');
  displayer.innerHTML = this.responseText;
}

// Send a request
xhttp.open("GET", file);
xhttp.send();
}

function text_click(id){
  var d0c = document.getElementById(id);
  if(d0c.getAttribute('found')=="true"){
  var path = d0c.getAttribute('metadata');
  readTextFile(path);
  }
  else{
    var displayer = document.getElementById('displayer');
    displayer.innerHTML = "[LOCKED]<br>[LOCKED]<br>[LOCKED]<br>[LOCKED]<br>[LOCKED]<br>"
  }
}

function open_image(file)
{
  // Create an XMLHttpRequest object
  const xhttp = new XMLHttpRequest();
  // Define a callback function
xhttp.onload = function() {
  // Here you can use the Data
  var displayer = document.getElementById('displayer');
  displayer.innerHTML = this.responseText;
}

// Send a request
xhttp.open("GET", file);
xhttp.send();
}
