"use strict";

const ipcRenderer = require('electron').ipcRenderer

const MUSHU_SYMBOL = Symbol('MUSHU_SYMBOL');
const MUSHU_MOD = Symbol('MUSHU_MOD');
const MUSHU_OPERAND = Symbol('MUSHU_OPERAND');
const COL = Symbol('COL');
const ROW = Symbol('ROW');

class Mushu{
  constructor(){
    this.type = null; //MUHU_ MOD | SYMBOL | OPERAND
    this.symbol = null; //    "->" | "->>" | "<-" | "<<-" | "." | ":" | "="
    this.operand = null; //   "/row[\d+]|col[\d+]/g"
    this.modifiers = null; // "*" |"***" |"**"
  }
}

class Operands {
  constructor(){
   this.r_c = null ; // en fonction de si c row ou col
   this.num = null;
 }
}
//------------------------------PARSING---------------------------------

function op_parse(str){
  var row_col = str.replace(/row|col/g,' $& ').replace(/\[|\]/g,'').replace(/\d+/g,'').trim();
  var num = str.replace(/row|col/g, '').replace(/\[|\]/g,'').replace(/\d+/g,'$&').trim();
  var oper = new Operands();
  switch(row_col){
    case "row":
      oper.r_c = "ROW"; break;
    case "col":
      oper.r_c = "COL"; break;
  }
  oper.num = num;
  return oper;
}

function mushu_sym(str){
  var mush_s = new Mushu();
  mush_s.symbol = str;
  mush_s.type = MUSHU_SYMBOL;
  return mush_s;
}

function mushu_op(str){
  var mush_op = new Mushu();
  mush_op.operand = op_parse(str);
  mush_op.type = MUSHU_OPERAND;
  return mush_op;
}

function mushu_mod(str){
  var mush_mod = new Mushu();
  mush_mod.modifiers = str;
  mush_mod.type = MUSHU_MOD;
  return mush_mod;
}


function parser(str){ //breaks down string then outputs a list (of lists in case there are multiple lines) of Mushus, otherwise sends error in list
  console.log("og string");
  console.log(str);
  var lines = str.split('\n');
  console.log("test lines");
  console.log(lines);
  var out_parse = new Array();
  for(var i = 0; i<lines.length;i++){
    console.log("lines length = "+lines.length);
    console.log(lines[i]);
    var l_copy = lines;
    var arr = l_copy[i].replace(/row\[\d+\]|col\[\d+\]/g,'$& ').replace(/<<-|->>|->|<-|\.|:|=/g,'$& ').replace(/\*\*\*|\*\*|\*/g, '$& ').split(' ');

    for(let correc=0;correc < arr.length; correc++){ if(arr[correc]=="" || arr[correc]=="\n"){ arr.splice(correc,1); } }
    console.log("test arr");
    console.log(arr);
    var mush_lst = new Array();
    for(var j = 0; j<arr.length;j++){
      console.log(/row\[\d+\]|col\[\d+\]/g.test(arr[j]));
        if(/row\[\d+\]|col\[\d+\]/g.test(arr[j])) { mush_lst.push(mushu_op(arr[j])); }
        else if(/<<-|->>|->|<-|\.|:|=/g.test(arr[j])) { mush_lst.push(mushu_sym(arr[j])); }
        else if(/\*\*\*|\*\*|\*/g.test(arr[j])) { mush_lst.push(mushu_mod(arr[j])); }
        else{
          mush_lst = ["SYNTAX ERROR ON \"" + arr[j] + "\"!<br>AWAITING FOR INPUT..."];
          var output = document.getElementById("out");
          output.innerHTML = mush_lst[0];
          return null;}
      }
      out_parse.push(mush_lst);
      console.log("Done "+i+1+" times");
    }
  return out_parse;
}

//-------------------------------------------------- PRE_EVALUATING ----------------

function mod_eval(mod){
  switch (mod) {
    case "*":
      return 1;
      break;
    case "**":
      return 3;
    case "***":
      return 5;
    default:
      return 1;
  }
}

function op_eval(sym, r_c, num1, mod_or_c_r2 = "1", num2=null){
  console.log(sym);
  console.log(r_c);
  console.log(mod_eval(mod_or_c_r2));
  switch(sym){
    case "=":
      return "visible = grid_cpy("+r_c+","+num1+","+num2+",visible);";
      break;
    case "->":
      return "visible = grid_shift("+r_c+","+num1+","+(mod_eval(mod_or_c_r2)+0)+",visible);";
      break;
    case "->>":
      return "visible = grid_shift("+r_c+","+num1+","+(mod_eval(mod_or_c_r2)+1)+",visible);";
      break;
    case "<-":
      return "visible = grid_shift_rev("+r_c+","+num1+","+(mod_eval(mod_or_c_r2)+0)+",visible);";
      break;
    case "<<-":
      return "visible = grid_shift_rev("+r_c+","+num1+","+(mod_eval(mod_or_c_r2)+1)+",visible);";
      break;
    case ":":
      return "visible = grid_one("+r_c+","+num1+",visible);";
      break;
    case ".":
      return "visible = grid_zero("+r_c+","+num1+",visible);";
      break;
  }
}

function pre_eval(m_lst){//pass down a list (of lists) of Mushus, outputs a list of functions to send to matrix.js 4 eval
  var pass_lst = new Array();
  for(var i = 0; i < m_lst.length; i++){
      var expr = m_lst[i];
      //firstly check if expr is in right order : <OP> (optional : <MOD>) <SYM> (optional: <OP>)
      if(expr[0].type == MUSHU_OPERAND){
        switch(expr[1].type){
          case MUSHU_SYMBOL:
            if(expr[1].symbol != "=" && expr.length > 2){
              pass_lst = []; var output = document.getElementById("out");
              output.innerHTML = "ERROR ON LINE " + i+1 + ".<br>" + "TOO MANY PARAMETERS.";
              return null;
            }
            if(expr[1].symbol == "=" && (expr[2].type!=MUSHU_OPERAND || expr[2]==null)) {
              pass_lst = [];
              var output = document.getElementById("out");
              output.innerHTML = "ERROR ON LINE " + i+1 + ".<br>" + "MISSING OPERAND AFTER\"" + expr[1] + "\".";
              return null;
            }
            break;
          case MUSHU_MOD:
            if(expr.length==2 || expr[2].type != MUSHU_SYMBOL){
              pass_lst = [];
              var output = document.getElementById("out");
              output.innerHTML = "ERROR ON LINE " + i+1 + ".<br>" + "INVALID GRAMMAR AFTER \"" + expr[1] + "\".";
              return null;
            }
            break;
          default:
            pass_lst = [];
            var output = document.getElementById("out");
            output.innerHTML = "ERROR ON LINE " + i + "..<br>" + "INVALID GRAMMAR on \"" + expr[1] + "\".";
            return null;
        }
      }
      else{var output = document.getElementById("out"); output.innerHTML = "ERROR ON LINE " + i + ".<br>" + "FIRST WORD NOT OPERAND."; return null; }
      //if evthing ok then loop through it
      switch (expr[1].type) {
        case MUSHU_SYMBOL:
            console.log(expr[1].symbol);
            console.log(expr[0].operand);
            if(expr[1].symbol == "="){ pass_lst.push(op_eval(expr[1].symbol, expr[0].operand.r_c, expr[0].operand.num, expr[2].operand.r_c, expr[2].operand.num)); }
            else{ pass_lst.push(op_eval(expr[1].symbol, expr[0].operand.r_c, expr[0].operand.num));}
          break;
        case MUSHU_MOD:
            pass_lst.push(op_eval(expr[2].symbol, expr[0].operand.r_c, expr[0].operand.num, expr[1].modifiers));
            break;
        default:
            pass_lst = [];
            var output = document.getElementById("out");
            output.innerHTML = "UNCAUGHT ERROR ON LINE" +i+".";
            return null;
      }
  }
  return pass_lst;
}

//-------------------------------------------------SENDING_INPUT -------------------

function send_input(){
  let u_input = document.getElementById('comline');
  console.log(u_input.value);
  let copy_inp = u_input.value;
  u_input.value = "";
  //remember if nothing is passed by parser it's an error just don't send
  //same goes for pre_eval !
  var parsed = parser(copy_inp.toLowerCase());
  if(parsed == null){ console.log("err"); return ; }
  console.log("break point");
  console.log(parsed);
  var to_pass = pre_eval(parsed);
  if(to_pass == null){ return ; }
  console.log(to_pass);
  ipcRenderer.send('commands', to_pass);

}

// ************************************************** MAIN **************************

window.addEventListener("keydown", function(event) {
  if (event.defaultPrevented) {
    return; // Do nothing if event already handled
  }
  if(event.key == "Enter" && event.ctrlKey){ send_input(); }
});

/*
var ButtonSendName = document.getElementById('sendName');
ButtonSendName.addEventListener('click', (event) => {
  ipcRenderer.send('nameMsg', name.value);
})*/

ipcRenderer.on('nameReply', (event, arg) => {
  console.log(arg) // why/what is not right..
});
