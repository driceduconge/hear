"use strict";
//const { ipcRenderer } = require('electron')
/*
ipcRenderer.on('forMatrix', function (event, arg){
  console.log(arg);
  list_to_eval = arg;
});
console.log(">>Matrix<<");
*/

//------------SETUP-ADRESS-SYSTEM-----------------
var adressesArray = new Array();

class Adresse{
  constructor(indic, idx){
    this.id = idx;
    this.addr = indic; //addresse que l'on va convertir en matrice
    this.fileName = null; //nom du fichier lié (plus tard) pour passer a FileDoc
    this.matrix = new Array();
    this.check = false;
  }
}

class Cell{
  constructor(){
    this.id = null;
    this.state = 0;
  }
}

function simplify_addr(a){ //parametre une classe Adresse. retourne une classe.
  var n = "";
  for(var i = 0; i<a.addr.length; i++){
    if(a.addr.charAt(i)!='.'){
      n += a.addr.charAt(i);
    }
  }
  a.addr = n;
  return a;
}
function hex_to_bin(hex){//convertir hex en binaire
    return ("00000000" + (parseInt(hex, 16)).toString(2)).substr(-8);
}

function hex_to_list(m){
  var byt = hex_to_bin(m);
  var list = new Array();
  for(var i = 0; i<byt.length; i++){
    list[i] = parseInt(byt.charAt(i));
  }
  return list;
}

function grid_define(a){//passe Adresse, renvoie Adresse avec matrix màj.
  var grid =  a.matrix;
  var s = a.addr;
  var count = 0;
  for(var i = 0; i<s.length; i+=2){
    grid[count] = hex_to_list(s.substring(i,i+2));
  	count++;
  }
  a.matrix = grid;
  return a;
}

function make_Adresse(addr,i){
  var x = new Adresse(addr,i);
  x = simplify_addr(x);
  x = grid_define(x);
  return x;
}



function matrix_2_hex(matrix){
  var string = "";
  var zero = true;
  for(i=0; i<14;i++){
    var byte = "";
    for(j=0;j<8;j++){
      byte+= matrix[i][j];
      if(matrix[i][j]=="1"){zero = false;}
    }
    if(i==13){string += parseInt(byte,2).toString(16);}
    else{string += parseInt(byte,2).toString(16) + ":";}
  }
  if(zero){ return "";}
  return string;
}

function matrix_2_cells(matrix, cellarr){//transforme les cells en fonction de la matrice
  var idx = 0;
  for(i=0; i<14;i++){
    for(j=0;j<8;j++){
      if(matrix[i][j]==0){ cellarr[idx].state = 0; }
      else{ cellarr[idx].state = 1; }

      idx++;
    }
  }
  return cellarr;
}

function cells_update(cells) { //update la vue des cellules en fonctions de leur state Class
    for(i=0; i<112;i++){
      var cell = document.getElementById(cells[i].id);
      if(cells[i].state==true){
        cell.className = "cell on";
      }
      else{ cell.className = "cell"}
    }
  //  console.log(a);
  //return a;
}

function compare_cells_add(main, file){//pass two Adresses (the grid and an Address)
  file.check = true;
  for(var i = 0; i<14; i++){
    for(var j = 0; j<8; j++){//compare each
      if(main.matrix[i][j] != file.matrix[i][j]){ file.check = false; }
    }
  }
  if(file.check==true){
    var texte = document.getElementById(file.id);
    var rune = document.getElementById(file.id+"r");
    console.log(file.id);
    texte.setAttribute("found", "true");
    rune.setAttribute("found", "true");
    var notA = document.getElementsByClassName("cell")
    var cells = document.getElementsByClassName("on");
    for(var i = 0; i<notA.length;i++){
      notA.item(i).style.animation="othercells 0.45s linear";
    }
    for(var i = 0; i<cells.length;i++){
      cells.item(i).style.animation="correct 0.45s linear";
    }
  }
  console.log(file.check);
  return file.check;
}

//-----------------------------FONCTIONS-DE-MODIFICATIONS------------------------------------------------

//const ROW = Symbol('ROW');
//const COL = Symbol('COL');

function grid_zero(row_or_col, number, main){//deux paramètres + la matrice
  if(row_or_col == ROW){
    for(var i = 0; i<8; i++){
      main.matrix[number-1][i] = 0;
    }}
  if(row_or_col == COL){
    for(var i = 0; i<14; i++){
      main.matrix[i][number-1] = 0;
    }
  }
  return main;
}

function grid_one(row_or_col, number, main){//deux paramètres + la matrice
  if(row_or_col == ROW){
    for(var i = 0; i<8; i++){
      main.matrix[number-1][i] = 1;
    }}
  if(row_or_col == COL){
    for(var i = 0; i<14; i++){
      main.matrix[i][number-1] = 1;
    }
  }
  return main;
}

function grid_cpy(row_or_col, source, dest, main){
  if(row_or_col == ROW){
    for(i=0;i<8;i++){
    main.matrix[dest-1][i] = main.matrix[source-1][i];
  }}
  if(row_or_col == COL){
    for(i=0;i<14;i++){
      main.matrix[i][dest-1] = main.matrix[i][source-1];
    }
  }
  return main;
}

function grid_shift(row_or_col, number, deg/*int*/, main){//deg will be obtained by adding *1 **3 ***5 to either 0 -> or 1 ->>
  if(row_or_col == ROW){
    var row_lst = main.matrix[number-1].slice(8-deg);
    main.matrix[number-1].splice(8-deg);
    for(var i=row_lst.length;i>0;i--){
      main.matrix[number-1].unshift(row_lst[i-1]);
      }
    }
  if(row_or_col == COL){
    var temp_arr = new Array();
    for(var i=0;i<14;i++){
      temp_arr[i] = main.matrix[i][number-1]; // put every cell in a new list
    }
    //apply mods to this temp list
    var col_lstemp = temp_arr.slice(14-deg);
    temp_arr.splice(14-deg);
    for(var i = col_lstemp.length; i>0;i--){
    temp_arr.unshift(col_lstemp[i-1]);
    }
    //restore it in array
    for(i=0;i<14;i++){
      main.matrix[i][number-1] = temp_arr[i];
    }
  }
  return main
}

// when <- or <<-
function grid_shift_rev(row_or_col, number, deg/*int*/, main){//deg will be obtained by adding *1 **3 ***5 to either 0 -> or 1 ->>
  if(row_or_col == ROW){
    var row_lst = main.matrix[number-1].slice(0,deg);
    main.matrix[number-1].splice(0,deg);
    for(var i =0; i<row_lst.length;i++){
    main.matrix[number-1].push(row_lst[i]);}
    }
  if(row_or_col == COL){
    var temp_arr = new Array();
    for(i=0;i<14;i++){
      temp_arr[i] = main.matrix[i][number-1]; // put every cell in a new list
    }
    //apply mods to this temp list
    var col_lstemp = temp_arr.slice(0,deg);
    temp_arr.splice(0,deg);
    for(i=0;i<col_lstemp.length;i++){
    temp_arr.push(col_lstemp[i]);}
    //restore it in array
    for(i=0;i<14;i++){
      main.matrix[i][number-1] = temp_arr[i];
    }
  }
  return main
}
//-------------------------------------EVAL function

function each_frame(main, cellarray, addrArray){//la grid du moment et l'array de grille
  //evaluate_async(chaining_funcs);
  var serial = document.getElementById("serial");
  var hex = matrix_2_hex(main.matrix);
  matrix_2_cells(main.matrix,cellarray);
  cells_update(cellarray);
  serial.innerHTML = hex;
  //pour l'instant on fait pas de loop à travers toutes les addresses car on en a pas
  for(var i = 0; i<addrArray.length;i++){
    compare_cells_add(main, addrArray[i]);
  }
}


//***************************************MAIN****************************************************************

var cellarray = new Array();
var idx = 0;

//--------------------------------------------------créer dynamiquement les divs

var parent = document.getElementById('grid');

for(var j=0;j<14;j++){
  for(var i=0;i<8;i++){
    //créons une div et une instance cellule qui correspond
    var cell = document.createElement('div');
    var c = new Cell();
    cell.style.width = "34px";
    cell.style.height= "34px";
    cell.style.position = "absolute";
    cell.className = "cell";
    var l = 34+(i*48.5);
    var t = 238+(j*45.5);
    if((i==0 && j==0)!=true){
      idx +=1;
    }
    cell.style.top = t+"px";
    cell.style.left = l+"px";
    cell.id = "cell_"+idx;
    c.id = cell.id;
    c.state = 0;
    cellarray[idx] = c;
    document.body.appendChild(cell);
  }
}

console.log("first roll");
console.log(cellarray);

//on crée la variable principale qui servira à stocker la matrice VISIBLE
var visible = make_Adresse("00.00.00.00.00.00.00.00.00.00.00.00.00.00","default");
console.log(visible.matrix);
var new_cellarray = matrix_2_cells(visible.matrix, cellarray);
cells_update(new_cellarray);

//TESTING
var test = new Array();
test[0] = make_Adresse("ff.81.81.81.81.81.81.81.81.81.81.81.81.ff","1");
test[1] = make_Adresse("28.a9.d4.6a.a9.d4.a9.d4.a9.6a.a9.a9.a9.28","2")
test[2] = make_Adresse("20.bf.54.6a.41.34.61.35.01.2a.41.a4.bf.48","3");
test[3] = make_Adresse("aa.55.aa.55.aa.55.aa.55.aa.55.aa.55.aa.55","4");



console.log("test id")
console.log(test.id);
//var chaining_funcs = ["grid_zero(ROW,2,visible);", "grid_one(COL, 2, visible);", "grid_shift(ROW,1,1,visible);", "grid_shift(COL,4,1,visible);", "grid_cpy(ROW,5,6,visible);"];
/*visible = grid_zero(ROW,2,visible);
visible = grid_one(COL, 2, visible);
visible = grid_shift(ROW,1,1,visible);
visible = grid_shift(COL,4,1,visible);
visible = grid_cpy(ROW,5,6,visible);*/
//console.log(chaining_funcs);
console.log(visible);

//eval("visible = grid_zero(COL,3,visible);");
ipcRenderer.on('forMatrix', function (event, arg){
  console.log(visible);
  console.log(arg[0]);
  for(var i = 0; i<arg.length;i++){
  eval(arg[i]);  }
  each_frame(visible, new_cellarray, test)
  //return arg;

});

/*
for(i=0;i<ex.length;i++){
  console.log(ex[i]);
  console.log(ex[i][0]);
  eval(ex[i][0]);
}*/

i=0;
//END OF TESTING
window.onload = function() {
      setInterval(each_frame(visible, new_cellarray, test), 100);
 }
