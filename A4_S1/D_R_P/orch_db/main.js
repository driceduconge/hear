"use strict";


const { app, BrowserWindow } = require('electron')
const {ipcMain} = require('electron');

var main;
var matrix;
var filedoc;
function create_main () {
  main = new BrowserWindow({width: 1156,height: 889,   autoHideMenuBar: true,
    webPreferences: {
               nodeIntegration: true,
               contextIsolation: false,
           },
        resizable: false,
  })
  main.loadURL(`file://${__dirname}/main.html`)
  main.webContents.openDevTools()
  main.on('closed', function () {
     main = null
  })
  return
}
app.on('ready', () => {
  main = create_main();
    ipcMain.on('commands', (event, arg) => {
  //console.log("name inside main process is: ", arg);
   // this comes form within window 1 -> and into the mainProcess
  event.sender.send('forMatrix', arg); // sends back/replies to window 1 - "event" is a reference to this chanel.
  //main.webContents.send( 'forMatrix', arg ); // sends the stuff from commander to matrix.
});
});
