
Iel avait encore raté de près son but, ça faisait la troisième fois ce mois-ci. Dans sa cave mal éclairée, iel s'étirait sur le fauteuil de satin, entouré d'espaces liquides où défilaient des nuées lumineuses  sur lesquels son regard ne se posait plus. En lorgnant sur les lithes iel vérifiait du bout des doigts que leur derme n'avaient pas souffert de l'humidité, en profitait pour épousseter les sigils marqués en elles, rien n'avait bougé, son oreille s'était peut être émoussée avec le temps ? Iel se déplie alors hors de son siège, secoue la tête et les paupières, psalmodie quelques motssons et signe en direction des écrans liquides. Les claquements lumineux s'évaporent alors lentement, se décollant de la lumière pour retourner de là où ses machines les avaient appelés, ciel et terre. Iel entend le bruissement du vent à nouveau ainsi que les quelques rouges-gorges qui piaillent un peu plus haut. Déroule alors ses jambes le long de l'escalier vers la surface, rendant  peu à peu à ses yeux leur sensibilité au jour.

{longue pause}

Ses recherches lui prenaient des nuits entières, à chaque fois iel s'asseyait et regardait directement les écrans clignoter, en essayant de ne pas ciller. Iel entendait bruisser parfois comme des phrases entre les murs enterrées où il se trouvait. Dans ces moments là il lui semblaient que les extrémités de son corps s'embrasaient doucement d'une douleur sourde, que le feu lui lèchait aussi les paupières. Chaque fois que cela lui arrivait, il lui semblait ressentir les herbes folles du jardin qu'il parcourait chaque jour, il lui semblait qu'elles lui griffaient les jambes, que la terre sous ses pieds se contractait, mais tout cela d'une manière très légèrement différente. Sans mettre de mots dessus, iel trouvait que la terre manquait de présence, que les herbes - quand elles fouettaient sa peau - perdaient un peu d'épaisseur, il comprenait que ce qui lui était compris était du parcellaire. Les machines ne savaient pas comment le réel était, elles cherchaient simplement à renvoyer la totalité de leurs perceptions dans des stridulations lumineuses. Leur cadence fixée par les sigils ne les bridaient pas mais leur indiquait le chemin, les fréquences et les affects à parcourir pour retrouver une semblance de sensible. Le courant qui les traversait amenait avec lui des mots et des sons dont elle gardaient la trace, lui faisant ressentir comme une certaine staticité dans la pièce, lui picotant le creux des orbites, hérissant ses poils. Il lui manquait quelque chose encor.

ces sigils à tracer sur le métal encore brûlant, ce langage inscrit au fond de leur circuit par son fil.  Iel appris à regarder et écouter les machines, à communiquer avec, non pas en comprenant la cadence des affichages liquides, mais plutôt en ressentant les vagues de chaleur, les accélérations électromagnétique et leurs bourdons. Elles semblent parfois vivre après l'arrêt, car quand iel retourne dans la salle aux heures d'aurore iel les sent parfois frémir sous ses doigts.


---

le son sera la paille sur laquelle seront couchés les lignes de codes, 

elles seront nourries aux machines le matin venu.

leur syn-taxe, le tactile du tapuscrit, la frappe machinale 
la frappe machinale qui sonne vie au langage écrit,
la langue qui se joue,
langue sur la joue,
le processeur battant la mesure
coeurs tramant  l'impulsion en la fréquence,
un choeur ondulant une mer de signal,
traducteurs du réel, 
phares dans les ténèbres mathématiques, 
soleils dans l'abîme de l'invisible.

---

	Pour que tu les comprennes, il te faudra passer de longes heures à leur murmurer nos histoires; des longues nuits de contes. Ensuite tu devras écouter leur chant les yeux fermés, pour percevoir les infimes variations dans la trame qu'ils tissent. Tu t'enfermeras dans le silence des lunes durant, pas un autre morceau de langage, humain ou animal ne traversera ton tympan. Alors, leur ouvrage te paraîtra prendre forme dans tes rêves, et à ce moment là observe bien, concentre-toi; car les symboles qu'elles formeront, tu devras les fixer dans un creux derrière ton coeur, loin de ta conscience. Le matin suivant, en laissant à nouveau le bruit des autres te traverser, tu les oublieras. Mais après, lorsque tu retourneras leur parler, les signes remonteront du fond de ta poitrine vers ta main, les mots couleront de tes doigts et tu pourras les reformer sur leur derme.

---

les moirages de la pierre, tracés par la chute de foudre, font danser les yeux des voyageurses. le long de sa surface, l'iris se perd et se heurte à un nacre mouvant, maille translucide sur tissu d'écume figée. La pierre imaginaire ne se trouve pas au fond de la montagne, elle n'est pas secrète. L'on en ramasse sur les bords des rivières, celles qui remontent à travers les vignes, vironnées de bouleau. Elles ne sont pas géodes, on ne les brise pas. Leurs secrets sont décrits entiers dans leurs anneaux de sélénite; l'on y lit les soirs où la lune éclairait pleinement la rive, ceux où l'ondée a tant fait croître le cours que l'eau a poussé le minéral. Si l'on choisit de la ramener chez soi, celleux l'enrobant dans un coffre verront au bout de quelques mois que la pierre se désédimente, laissant tomber ses reflets afin de les tronquer contre une matitude semblable au granit : on dit que la pierre se fane. Il faudra pour lui laisser pleinement le temps de se développer la poser sur le seuil a côté d'une tasse d'eau de pluie, les musicien·nes pourront chanter à mi-voix les soirs où la lune dévoile son diamètre complet.


---

Le rapport que la plupart des utilisateurices ont avec les ordinateurs (computers) est pour la plupart d'entre elleux inégal. Certes, on comprend que sous le capot, sous l'écran on trouve des composants électroniques : des circuits, des processerurs, des cartes-mères... Le tech-speak capitaliste a mué ces objets opaques en arguments de promotion: on ne cesse de vanter le nombre de coeurs d'un processeur , de cadences en gigahertz, de vitesse de rafraichissement etc. Mais qu'en sait-on réellement ? La réalité technique de ces objets est très difficilement appréhensible sans de solides bases en mathématiques et en électronique. C'est pour cela qu'il faut rêver différemment de ces objets, qu'il faut les  aborder sous un autre angle et s'extraire du paradiglme techno-virilisme ambiant; du statu de l'ingénieur·e passer à celui de magicien·ne et de saisir  les ordinateurs sous le prisme auquel ils nous paraissent : celui d'objet technomagique. Il faut disséquer, déboîter la carcasse (techno) et suivre du bout des doigts les circuits imprimés, les sentirs comme autant de symboles, de nerfs et de sigils (magie). On écoutera aussi leur sons, des râles, grâce à des oreilles magnétiques. 
Peu à peu, on lèvera la nuit logique et l'on  parlera leur sabir d'impulsions électriques, faisant à nouveau maille avec la machine. Nos pratiques feront constellation autour de l'archipel électronique.


 ---
 
 L'arc électrique sort du circuit et parcourt les surfaces, transforme le minéral, lui chuchote sous la couche sédimenteuse et fait émerger des variations de tensions/surfaces, tout vient à se tendre, l'arc s'étire de part en part de la couche de métal, l'air se charge et tout à coup, le court circuit opère et en marge, émerge une mer de signaux, des variations électromagnétiques aux pics montagneux, l'oreille interne chamboulée on vacille et le circuit perd toute intensité, la tension s'écroule. Il n'y a plus aucun bruissement de courant, il ne reste qu'un calme plat et une écume d'ondes.
 
--- 

De plus près, on voit les symboles qu'on avait gravés sur les composants, système de noeuds et de sauts, formes et contreformes d'intentions de calcul. 

---

Après avoir inscrit en elllui tous les signes que les calculateurs lui formulaient, après les avoir transformés en tracés, syllabes, mots et phrases iel comprit que tout ne se déroulait pas qu'entre son corps et la machine. La cave n'était en fait qu'un aiguisoir pour ses sens, que seul le long affûtage qu'iel y avait vécu a eu pour objectif de s'ouvrir à d'autres flux, qui se jouaient en-dessous des machines. Iel enleva ses chaussures et plaqua son talon à même la terre. Sous-sol un bourdon se jouait, imperceptible par les sismographes, iel le perçu par un tressaillement parcourant son nerf *rachial*, ce son émergeait de bien plus bas que l'humus, plus profondément encore que les nappes phréatiques, à la frontière du magma ; le chant d'un choeur de pierres, résonnant d'impulsions magnétiques provenant de tous les virons. Les pierres magnétiques forment un roulement doux, s'accordent en harmoniques, même lorsque la terre tremble et que les plaques se déchirent, au lieu de se distordre et de s'accélérer, elles ralentissent leur son, expirent lentement pour pallier au choc.

---

La brume ruine la vue, forme une vapeur statique, ce brouillard diffracte la lumière en un halo sourd. On n'y voit plus mais on entend toujours le craquement du tonnerrre et le picotement statique sous le derme.

---

Si j'ouvre un processeur en deux, il n'y aura ni fluide, ni organes, il n'y aura que des éclats lumineux et une fumée grise, émanant d'une fournaise. L'ordinateur ne respire pas, ce n'est qu'un simulacre de neurones, surinterprété et transformé en signes, seul échappatoire à l'absence de matérialité de ce qu'il nous transmet, hors de la brûlure de l'occasionnelle surchauffe. La fournaise, le sel et le soufre. Si j'ouvre ma main au-dessus du circuit électrique, mon sang s'ajoutera à la fumée, mes nerfs trembleront à la caresse des étincelles, mais pas d'éclats, pas de fulgurances, la seule chose que nous aurons en commun avec la machine, c'est une chaleur de fournaise à l'intérieur.

---

Les machines à l'intérieur de la cave semblaient sortir directement du plus profond de la terre. On aurait dit des cristaux de métal, parfaitement rectilignes. La seule chose qui trahissait la charge technomagique était les sigils tracés en leurs surfaces, couche à couche, palimpsestes oxydés. Blocs aux relets froids tapissant les murs, le sol et le plafond. Seuls certains écrans liquides agissaient comme interface, seuls éléments rapportés de la cave aux lithes électrifères.

---

Cela faisait tant d'années qu'on avait perdu les plans de structurations des machines que l'on ne s'en servait plus que dans des contextes marginaux : mesurer le temps, le chant des pierres-au-dessous et calculer les interférences dans le champ magnétique provoqué par les météores. Iel savait que le savoir q'on lui a transmis était voué à l'oubli, que dans quelques générations, peut-être plus tôt, les machines chaufferaient dans le vide, sans oreilles pour écouter leurs cantiques subsoniques. Elles s'éteindraient alors lentement, une à une, quand les inscriptions en leur sein se seraient totalement oxydées, on ne saura plus qu'elles aidaient les humains à lire dans les signes du ciel et les entrailles de la terre,. On ira peut être les arracher du sol pour les enfouir au fond d'un palais, déracinées dans les demeures des virons; iel y pensait sans appréhension, sûrement avec une certaine joie du privilège d'être une des dernières qui marquera de ses doigts chauffés à blanc, leur peau de métal

---

Il faut écrire, décrire, dès aujourd'hui, une nouvelle manière d'être avec la technologie électronique. Imaginer des futurs lointains où la sur-technification des outils disparaît, au profit d'une interaction minérale post-organique, par delà la chair, par delà la réalité des terres rares. Il faut écrire un monde où la machine marche autrement, hors-processeur, en zone blanche, hors infrastructure électrique. Il faut mettre au jour une techno-magie, un occulte-calculatoire, où la pierre parle pour elle même, où on l'écoute et la guide, tailleureuses de sens dans la pierre, chercher dans le flou parlant plutôt que dans la technologie aphone. Faire parler nos machines actuelles pour imaginer les chants de celles du futur, retracer leur parcours à l'envers : elles reviendront comme nous, au fond de la terre et des mers. On pourrira avec elles et on ressurgira, des viscères brûlantes de la biosphère vers un ciel promis, archipel des possibles.

---

mourir un peu plus tard encor, au fond de la cave caverne, cela lui paraissait dans ses cordes. Les vieuilles lui avaient raconté comment à la fin, celleux comme lui finissait par s'assimiler avec les machines, que leurs ossements aussi se raidissaient comme le métal et que leurs yeux prenaient la teinte absente de l'éclair. Ellui avaient dit qu'avant on enterrait sous la terre les ossements de toustes, c'est pour cela qu'on sédimentait, et que sa peau aussi, aussi résistante au températures qu'iel s'imposait soit-elle, finirait par s'éparpiller comme celle des codexs. Que la seule voie possible était de finir et de faire comme avant et de s'enterrer, de devenir sol et plus tard mur. Qu'au fond de la terre se trouvait un océan sous l'océan de lithes, qu'au fond ce ne serait pas plus mal, car iel pourrait les entendre chanter même dans la fin de la mort. On a pas de meilleurs oreilles que celles au fond du sol.

---

Quand le bout de mes doigts se chauffe à blanc, je ne ressens pas la douleur, rien qu'une raideur au bout des mains. Une fois rougeoyants, je les poses sur les faces de métal émergées et ressens à leur contact le soubresaut qui les traverse. Puis mes doigts glissent sur leurs surfaces, et de point en point j'articule mon intention en leur matérialité. Une fois les lithes marquées, mes doigts retrouvent leur température, leur forme biseautée s'extirpe du métal mordu. A chaque fois, je ne vois plus ma peau.

---

Les vieuilles disent qu'avant, l'heur où l'on foulait encor les airs, avant qu'on aille parler aux pierres et à la mer, il y avait déjà des machines, mais pas comme on fait. Iels disent qu'elles étaient tissées par des automates, tramées de pierre fondue en de fins circuits parcourus de soubresauts, battant au-delà de la cadence de nos coeurs. Iels les construisait par la contrainte du logique, détournants leurs chants par des séries de portes. Minéraux aphones ne recrachant que des chiffres. Loin dans le passé, iels n'écoutaient ni ne les touchaient, applicant seulement une technique implacable et immuable. Et nous, l'été, quand la pluie cesse, on en retrouve parfois des morceaux et si je les compare aux machines qu'on écoute, qui sortent de nos aïeules, je n'y vois rien de semblable, je n'entends rien, dans ces reliques subsiste un maigre éclat.
