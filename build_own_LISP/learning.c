#include <stdio.h>
#include <stdlib.h>

/*If we are compiling on Windows compile these functions */
/* these (#) are preproc. instructions */

#ifdef _WIN32
#include <string.h>

/*fake readline function*/
static char buffer[2048];

char* readline(char* prompt){
  fputs(prompt, stdout);
  fgets(buffer, 2048, stdin);
  char* cpy = malloc(strlen(buffer)+1);
  strcpy(cpy,buffer);
  cpy[strlien(cpy)-1] = '\0';
  return cpy;
}

/*fake add hist function*/
void add_history(char* unused){}

/*otherwise include the editline headers */
#else
#include <editline/readline.h>
#include <editline/history.h>
#endif
/* Declare a buffer for user input of size 2048*/
//static char input[2048]; not anymore?

int main(int argc, char** argv){

  /* Print version and exit info*/
  puts("DLISP Version 0.0.0.1");
  puts("Press CTRL+C to Exit\n");

  /*while*/
  while(1){
/*output prompt*/
  char* input = readline("dlisp> ");
  add_history(input);

  /*Echo input back to user*/
  printf("hey, (%s)\n", input);
  free(input);
  }

  return 0;
}
