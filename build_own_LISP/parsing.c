#include <stdio.h>
#include <stdlib.h>
#include "mpc.h"
/*If we are compiling on Windows compile these functions */
/* these (#) are preproc. instructions */

#ifdef _WIN32
#include <string.h>

/*fake readline function*/
static char buffer[2048];

char* readline(char* prompt){
  fputs(prompt, stdout);
  fgets(buffer, 2048, stdin);
  char* cpy = malloc(strlen(buffer)+1);
  strcpy(cpy,buffer);
  cpy[strlien(cpy)-1] = '\0';
  return cpy;
}

/*fake add hist function*/
void add_history(char* unused){}

/*otherwise include the editline headers */
#else
#include <editline/readline.h>
#include <editline/history.h>
#endif
/* Declare a buffer for user input of size 2048*/
//static char input[2048]; not anymore?

//------SET-UP---------------------------------------

enum{LERR_DIV_ZERO, LERR_BAD_OP, LERR_BAD_NUM};

struct lval;
struct lenv;
typedef struct lval lval;
typedef struct lenv lenv;

/* Declare new LispValue "lval" struct */

enum{LVAL_NUM, LVAL_QEXPR, LVAL_ERR, LVAL_SYM, LVAL_SEXPR, LVAL_FUN}; //Création de constantes
typedef lval*(*lbuiltin)(lenv*, lval*); //to get an lval* we dereference lbuiltin and call it with a lenv* and a lval* it returns a lval*

struct lval{
  int type;
  double num;
  /* Error and Symbol types have string data*/
  char* err;
  char* sym;
  lbuiltin fun;
  /*Count and pointer to a list of lval */
  int count;
  lval** cell;
};

lval* lval_fun(lbuiltin func){
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_FUN;
  v->fun = func;
  return v;
}

lval* lval_num(double x){
  lval* v = malloc(sizeof(lval)); // crée une struct lval
  v->type = LVAL_NUM; //la flèche assigne une valeur à travers le pointeur de la struct
  v->num = x;//assigne valeurs
  return v;
}

lval* lval_err(char* m){
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_ERR;
  v->err = malloc(strlen(m)+1);
  strcpy(v->err,m);
  return v;
}

lval* lval_sym(char* s){
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_SYM;
  v->sym = malloc(strlen(s)+1);
  strcpy(v->sym,s);
  return v;
}

lval* lval_sexpr(void){
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_SEXPR;
  v->count = 0;
  v->cell = NULL;
  return v;
}

lval* lval_qexpr(void){
  lval* v = malloc(sizeof(lval));
  v->type = LVAL_QEXPR;
  v->count = 0;
  v->cell = NULL;
  return v;
}

void lval_del(lval* v){
  switch(v->type){
    // do nothing for numbers
    case LVAL_NUM: break;
    // do nothing for functions
    case LVAL_FUN: break;
    // for err or sym free the string
    case LVAL_ERR: free(v->err); break;
    case LVAL_SYM: free(v->sym); break;

    //if sexpr then delete all elements inside
    case LVAL_QEXPR:
    case LVAL_SEXPR:
      for (int i=0; i< v->count; i++){
        lval_del(v->cell[i]);
      }
      free(v->cell);
    break;
  }
  free(v);
}

lval* lval_copy(lval* v){
  lval* x = malloc(sizeof(lval));
  x->type = v->type;
  switch(v->type){
    //copy func and numbers directly
    case LVAL_FUN: x->fun = v->fun; break;
    case LVAL_NUM: x->num = v->num; break;

    //copy strings using malloc and strcpy
    case LVAL_ERR:
      x->err = malloc(strlen(v->err)+1);
      strcpy(x->err, v->err); break;
    case LVAL_SYM:
      x->sym = malloc(strlen(v->sym)+1);
      strcpy(x->sym, v->sym); break;

      //copy lists by copying each sub expressions
      case LVAL_SEXPR:
      case LVAL_QEXPR:
        x->count = v->count;
        x->cell = malloc(sizeof(lval*)*x->count);
        for(int i = 0; i<x->cell;i++){
          x->cell[i] = lval_copy(v->cell[i]);
        }
        break;
  }
  return x;
}

//------ENVIRONMENT---------------------------------------

struct lenv{
  int count;
  char** syms;
  lval** vals;
};

lenv* lenv_new(void) {
  lenv* e = malloc(sizeof(lenv));
  e->count = 0;
  e->syms = NULL;
  e->vals = NULL;
  return e;
}

void lenv_del(lenv* e){
  for(int i=0; i < e->count; i++){
    free(e->syms[i]);
    lval_del(e->vals[i]);
  }
  free(e->syms);
  free(e->vals);
  free(e);
}

//------READING-------------------------------------------

lval* lval_read_num(mpc_ast_t* t){

  errno = 0;
  double x = strtod(t->contents,NULL);
  return errno != ERANGE ?
    lval_num(x) : lval_err("invalid number");
}

lval* lval_add(lval* v, lval* x){
  v->count++;
  v->cell = realloc(v->cell, sizeof(lval*) * v->count);
  v->cell[v->count-1] = x;
  return v;
}

lval* lval_read(mpc_ast_t* t){
  // If Symbol or Number return conversion to that type

  if(strstr(t->tag,"number")){ return lval_read_num(t);}
  if(strstr(t->tag,"symbol")){ return lval_sym(t->contents);}

  // If root (>) or sexpr then create empty list
  lval* x = NULL;
  if(strcmp(t->tag,">")==0)  { x = lval_sexpr();}
  if(strstr(t->tag,"sexpr")) { x = lval_sexpr();}
  if(strstr(t->tag,"qexpr")) { x = lval_qexpr();}

  // Fill this list w/ any valid expressions contained w/in
  for(int i = 0; i < t->children_num; i++){
    // le continue permet de "sauter" les parties inutiles "(" ")" "regex"
    if(strcmp(t->children[i]->contents, "(")==0) { continue; }
    if(strcmp(t->children[i]->contents, ")")==0) { continue; }
    if(strcmp(t->children[i]->contents, "{")==0) { continue; }
    if(strcmp(t->children[i]->contents, "}")==0) { continue; }
    if(strcmp(t->children[i]->tag,  "regex")==0) { continue; }
    x = lval_add(x, lval_read(t->children[i])); // récursivité tant qu'on a pas un num/sym
  }
  return x;
}

//------EVALUATING---------------------------------------
lval* lval_eval(lval* v);

lval* lval_pop(lval* v, int i){//pops a single element at index and shifts the rest of the list
  //find item at i
  lval* x = v->cell[i];
  //shift memory after the at i over the top ?
  memmove(&v->cell[i], &v->cell[i+1], sizeof(lval*)*(v->count-i-1)); //memmove(destination, source, size)
  //decrease the cunt of items in the list
  v->count--;
  //realloc the memory used
  v->cell = realloc(v->cell,(sizeof(lval*)*v->count)); //donc un bloc memoire en moins
  return x; //renvoie l'element qu'on a pop-out
}

lval* lval_take(lval* v, int i){
  lval* x = lval_pop(v,i);
  lval_del(v);
  return x;
}

lval* builtin_op(lval* a, char* op){//calculatingggggg
  //ensure all args are numbers
  for(int i = 0; i < a->count;i++){
    if(a->cell[i]->type != LVAL_NUM){
      lval_del(a); //frees memory
      return lval_err("Cannot operate on NaN !"); //returns a lval ERR
    }
  }
  //pop first el
  lval* x = lval_pop(a,0);
  //if no args and symb = substract then perform unary negation
  if((strcmp(op,"-")==0) && a->count == 0){
    x->num = -x->num;
  }
  //while there are still elements remaining
  while(a->count > 0){
    //pop next element
    lval* y = lval_pop(a,0);
    if(strcmp(op,"+")==0){ x->num += y->num;}
    if(strcmp(op,"-")==0){ x->num -= y->num;}
    if(strcmp(op,"*")==0){ x->num *= y->num;}
    if(strcmp(op,"/")==0){
      if(y->num == 0){lval_del(x); lval_del(y); x = lval_err("Division by Zero!");
        break;
      }
      x->num /= y->num;
    }
    if(strcmp(op,"%")==0){
      if(y->num == 0){lval_del(x); lval_del(y); x = lval_err("Modulo by Zero!");
        break;
      }
      long x_n = (long)x->num;
      long y_n = (long)y->num;
      x_n %= y_n;
      x->num = (double)x_n;
    }

    lval_del(y);
  }
  lval_del(a);
  return x;
}

//************************************ FOR Q EXPRESSIONS

#define LASSERT(args,cond,err) \
  if (!(cond)) { lval_del(args); return lval_err(err); }

lval* builtin_head(lval* a){
  //check error conditions
  LASSERT(a, a->count == 1, "'head' passed too many args!");
  LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "'head' passed incorrect type!");
  LASSERT(a, a->cell[0]->count != 0, "'head' passed {}!");

  //otherwise take() first arg
  lval* v = lval_take(a,0);

  //del all elements that aren't head
  while(v->count > 1){ lval_del(lval_pop(v,1)); }

  return v;
}

lval* builtin_tail(lval* a){

  LASSERT(a, a->count == 1, "'tail' passed too many args!");
  LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "'tail' passed incorrect type!");
  LASSERT(a, a->cell[0]->count != 0, "'tail' passed {}!");

  //check error conditions

  //take first arg
  lval* v = lval_take(a,0);

  //del first el and return
  lval_del(lval_pop(v,0));
  return v;
}

lval* builtin_list(lval* a){
  a->type = LVAL_QEXPR;
  return a;
}

lval* builtin_eval(lval* a){
  LASSERT(a, a->count == 1, "'eval' passed too many args!");
  LASSERT(a, a->cell[0]->type == LVAL_QEXPR, "'eval' passed incorrect type!");

  lval* x = lval_take(a,0);
  x->type = LVAL_SEXPR;
  return lval_eval(x);
}

lval* lval_join(lval* x, lval* y){
  //for each cell in y add it to x
  while(y->count){ x = lval_add(x, lval_pop(y,0)); }
  //del empty y and ret x
  lval_del(y);
  return x;
}

lval*  builtin_join(lval* a){
  for(int i = 0; i < a->count; i++){
    LASSERT(a,a->cell[i]->type == LVAL_QEXPR, "'join' passed incorrect type!");
  }

  lval* x = lval_pop(a,0);
  while(a->count){
    x = lval_join(x, lval_pop(a,0));
  }
  lval_del(a);
  return x;
}

// CALLING BUILTIN

lval* builtin(lval* a, char* func){
  if(strcmp("list",func)==0){ return builtin_list(a);}
  if(strcmp("head",func)==0){ return builtin_head(a);}
  if(strcmp("tail",func)==0){ return builtin_tail(a);}
  if(strcmp("join",func)==0){ return builtin_join(a);}
  if(strcmp("eval",func)==0){ return builtin_eval(a);}
  if(strcmp("+-/*%",func)==0){ return builtin_op(a, func);}
  lval_del(a);
  return lval_err("Unknown Function!");
}

//*************************** FOR S Expressions


lval* lval_eval_sexpr(lval* v){
  // eval children
  for(int i = 0; i < v->count; i++){
    v->cell[i] = lval_eval(v->cell[i]);
  }
  //error checking
  for(int i = 0; i < v->count; i++){
    if(v->cell[i]->type == LVAL_ERR) { return lval_take(v, i);}
  }
  //empty exp ?
  if(v->count == 0) { return v; }
  //single expr ?
  if(v->count == 1) { return lval_take(v,0); }
  // ensure first element is symbol
  lval* f = lval_pop(v,0);
  if(f->type != LVAL_SYM){
    lval_del(f); lval_del(v);
    return lval_err("S-expression does not start with symbol!");
  }
  //call builtin_op with operator
  lval* result = builtin(v, f->sym);
  lval_del(f);
  return result;
}

lval* lval_eval(lval* v){
  //eval S-Expressions
  if(v->type == LVAL_SEXPR){ return lval_eval_sexpr(v);}
  //others remains the same
  return v;
}


//------PRINTING-----------------------------------------
void lval_print(lval* v); // on pré-déclare lval_print pour que lval_expr_print ne bugue pas
/* S EXPRESSIONS loops over all sub-expressions */
void lval_expr_print(lval* v, char open, char close){
  putchar(open);
  for (int i=0; i < v->count; i++){
    // print value inside
    lval_print(v->cell[i]);
    // don't print trailing space if last eleme
    if(i != (v->count-1)){ putchar(' ');}
  }
  putchar(close);
}

/* function that prints lval */
void lval_print(lval* v){
  switch(v->type){
    case LVAL_NUM: printf("%lf", v->num); break; //in case it's a number print it
    case LVAL_ERR: printf("Error : %s", v->err); break;
    case LVAL_SYM: printf("%s", v->sym); break;
    case LVAL_FUN: printf("<function>"); break;
    case LVAL_SEXPR: lval_expr_print(v, '(',')'); break;
    case LVAL_QEXPR: lval_expr_print(v, '{','}'); break;
  }
}
/* same but with a line feed*/
void lval_println(lval* v){ lval_print(v);putchar('\n'); }





int main(int argc, char** argv){

//------CREATE PARSER---------------------------------------
  /* Create some Parsers */
  mpc_parser_t* Number = mpc_new("number");
  mpc_parser_t* Symbol = mpc_new("symbol");
  mpc_parser_t* Sexpr = mpc_new("sexpr");
  mpc_parser_t* Qexpr = mpc_new("qexpr");
  mpc_parser_t* Expr = mpc_new("expr");
  mpc_parser_t* Lispy = mpc_new("lispy");

  /* Define them with the following language */
  mpca_lang(MPCA_LANG_DEFAULT,
      "                                                 \
      number   : /-?[0-9]+\\.[0-9]+|-?[0-9]+/ ;         \
      symbol   : /[a-zA-Z0-9_%+\\-*\\/\\\\=<>!&]+/;     \
      sexpr    : '(' <expr>* ')' ;                      \
      qexpr    : '{' <expr>* '}' ;                      \
      expr     : <number> | <symbol> | <sexpr> | <qexpr> ;   \
      lispy    : /^/ <expr>* /$/ ;                      \
      ",
      Number, Symbol, Sexpr, Qexpr, Expr, Lispy);

//------IN CONSOLE---------------------------------------
  /* Print version and exit info*/
  puts("DLISP Version 0.0.0.1");
  puts("Press CTRL+C to Exit\n");

  /*while*/
  while(1){
/*output prompt*/
  char* input = readline("dlisp> ");
  add_history(input);

  /*Attempt to Parse the User Input*/
  mpc_result_t r;
  if(mpc_parse("<stdin>", input, Lispy, &r)){
    /* On Success Print the AST */
  //  lval result = eval(r.output);
    lval* x = lval_eval(lval_read(r.output));
    lval_println(x); //li est utilisé comme %d mais pr un long
    lval_del(x);
  } else{
    /* Otherwise Print the Error */
    mpc_err_print(r.error);
    mpc_err_delete(r.error);
  }
  free(input);
  }
  /* Undefine and Delete our Parsers */
  mpc_cleanup(6, Number, Symbol, Sexpr, Qexpr, Expr, Lispy);
  return 0;
}
